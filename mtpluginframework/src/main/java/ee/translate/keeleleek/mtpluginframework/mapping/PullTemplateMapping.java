package ee.translate.keeleleek.mtpluginframework.mapping;

import java.util.ArrayList;

import ee.translate.keeleleek.mtpluginframework.chopup.TemplateElement;

/**
 * Mapping for templates.
 */
public class PullTemplateMapping implements PullMapping {

	private String name;
	private ArrayList<TemplateElement> elements = new ArrayList<>();
	
	
	// INIT
	public PullTemplateMapping(String name) {
		this.name = name;
	}

	public void addTemplate(TemplateElement element) {
		elements.add(element);
	}

	
	// VALUES
	public String getName() {
		return name;
	}

	public int getElementCount() {
		return elements.size();
	}

	
	// IMPLEMENT (PULL MAPPING)
	public String getGroup() {
		return "Template";
	}
	
	public String getSource() {
		return name;
	}
	
	public String getDestination() {
		if (elements.size() == 0) return null;
		return elements.get(0).getName();
	}

	public void setDestination(String destination) {
		for (TemplateElement element : elements) {
			element.setName(destination);
		}
	}
	
	@Override
	public boolean isTouched() {
		if (elements.size() == 0) return false;
		return elements.get(0).isNameTouched();
	}
	
	
}
