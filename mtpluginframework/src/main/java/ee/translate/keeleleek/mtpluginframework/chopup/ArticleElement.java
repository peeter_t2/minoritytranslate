package ee.translate.keeleleek.mtpluginframework.chopup;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public abstract class ArticleElement {

	public static char BGN = '\u25C0';
	public static char SEP = '\u25FC';
	public static char END = '\u25B6';
	
	private final Type type;
	
	protected HashMap<Integer, ArticleElement> elements = new HashMap<>();
	
	int idCounter = 0;
	private StringBuilder buffer = new StringBuilder();
	
	
	// INIT
	public ArticleElement(Type type)
	 {
		this.type = type;
	 }

	public abstract void init(String text);
	
	
	// CONSTRUCT
	public Type type()
	 {
		return type;
	 }
	
	public void append(char c)
	 {
		buffer.append(c);
	 }
	
	public void append(ArticleElement element)
	 {
		buffer.append(BGN);
		buffer.append(idCounter);
		elements.put(idCounter, element);
		buffer.append(END);
		idCounter++;
	 }
	
	protected void dump()
	 {
		Set<Entry<Integer, ArticleElement>> entries = elements.entrySet();
		for (Entry<Integer, ArticleElement> entry : entries) {
			entry.getValue().dump();
		}
		
		String text = buffer.toString();
		buffer = new StringBuilder();
		init(text);
	 }
	
	
	// COLLECT
	public void collectWikilinks(Collection<WikilinkElement> collection) {
		if (this instanceof WikilinkElement) collection.add((WikilinkElement) this);
		Collection<ArticleElement> values = elements.values();
		for (ArticleElement element : values) {
			element.collectWikilinks(collection);
		}
	}

	public void collectTemplates(Collection<TemplateElement> collection) {
		if (this instanceof TemplateElement) collection.add((TemplateElement) this);
		Collection<ArticleElement> values = elements.values();
		for (ArticleElement element : values) {
			element.collectTemplates(collection);
		}
	}

	public void collectTextBlocks(Collection<TextBlockElement> collection) {
		if (this instanceof TextBlockElement) collection.add((TextBlockElement) this);
		Collection<ArticleElement> values = elements.values();
		for (ArticleElement element : values) {
			element.collectTextBlocks(collection);
		}
	}

	public void collectContentTextBlocks(Collection<TextBlockElement> collection) {
		if (this instanceof TextBlockElement) {
			TextBlockElement textBlock = (TextBlockElement) this;
			if (textBlock.hasContent()) collection.add(textBlock);
		}
		Collection<ArticleElement> values = elements.values();
		for (ArticleElement element : values) {
			element.collectContentTextBlocks(collection);
		}
	}
	

	// CONVERT
	protected abstract String asCode();
	
	public String toCode()
	 {
		String result = asCode();
		Set<Entry<Integer, ArticleElement>> entries = elements.entrySet();
		for (Entry<Integer, ArticleElement> entry : entries) {
			//System.out.println("" + BGN + entry.getKey() + END + "->" + entry.getValue().toCode());
			result = result.replace("" + BGN + entry.getKey() + END, entry.getValue().toCode());
		}
		return result;
	 }
	
	
	// HELPERS
	@Override
	public String toString() {
		return toString(null, 0);
	}
	
	private String toString(Integer id, int level) {
		String padding = new String(new char[level]).replace('\0', ' ');
		String sid = id == null ? "" : id + ": ";
		StringBuilder result = new StringBuilder(padding + sid + " " + type() + " " + asCode().replace("\n", "\n" + padding));
		Set<Entry<Integer, ArticleElement>> entries = elements.entrySet();
		for (Entry<Integer, ArticleElement> entry : entries) {
			result.append('\n' + entry.getValue().toString(entry.getKey(), level + 1));
		}
		return result.toString();
	}


	// TYPES
	public enum Type {
		TEXT_BLOCK,
		ROOT,
		WIKILINK,
		TEMPLATE,
		TAG_START,
		TAG_CONTENT,
		TAG_END
	}
	
	
}
