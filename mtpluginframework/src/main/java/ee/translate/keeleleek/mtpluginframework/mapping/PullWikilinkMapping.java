package ee.translate.keeleleek.mtpluginframework.mapping;

import java.util.ArrayList;

import ee.translate.keeleleek.mtpluginframework.chopup.WikilinkElement;

/**
 * Mapping for wikilinks.
 */
public class PullWikilinkMapping implements PullMapping {

	private String namespace;
	private String link;
	private ArrayList<WikilinkElement> elements = new ArrayList<>();
	
	
	// INIT
	public PullWikilinkMapping(String namespace, String link) {
		this.namespace = namespace;
		this.link = link;
	}

	public void addWikilink(WikilinkElement element) {
		elements.add(element);
	}

	
	// VALUES
	public String getNamespace() {
		return namespace;
	}
	
	public String getLink() {
		return link;
	}
	
	public String getGroup() {
		return namespace;
	}

	public int getElementCount() {
		return elements.size();
	}

	
	// IMPLEMENT (PULL MAPPING)
	public String getSource() {
		return link;
	}
	
	public String getDestination() {
		if (elements.size() == 0) return null;
		return elements.get(0).getLink();
	}

	public void setDestination(String destination) {
		for (WikilinkElement element : elements) {
			element.setLink(destination);
		}
	}
	
	@Override
	public boolean isTouched() {
		if (elements.size() == 0) return false;
		return elements.get(0).isLinkTouched();
	}
	
	
}
