package ee.translate.keeleleek.mtapplication.view.swing.swingview;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtapplication.view.swing.dialogs.ManualDialogMediatorSwing;

public class SwingManualWindowViewComponent extends JDialog {

	private static final long serialVersionUID = 7228903177092166304L;
	
	public ManualDialogMediatorSwing mediator;
	
	public JEditorPane wikitextEditorPane;
	public JEditorPane programEditorPane;
	private JScrollPane programScrollPane;
	private JScrollPane wikitextScrollPane;

	/**
	 * Create the dialog.
	 */
	public SwingManualWindowViewComponent(JFrame parent) {
		super(parent);
		setPreferredSize(new Dimension(700, 600));
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		{
			JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
			getContentPane().add(tabbedPane);
			
			programScrollPane = new JScrollPane();
			tabbedPane.addTab(Messages.getString("manual.program"), null, programScrollPane, null);
			
			wikitextEditorPane = new JEditorPane();
			programScrollPane.setViewportView(wikitextEditorPane);
			wikitextEditorPane.setContentType("text/html");
			
			wikitextScrollPane = new JScrollPane();
			tabbedPane.addTab(Messages.getString("manual.wikitext"), null, wikitextScrollPane, null);
			
			programEditorPane = new JEditorPane();
			wikitextScrollPane.setViewportView(programEditorPane);
			programEditorPane.setContentType("text/html");
		}
	}

}
