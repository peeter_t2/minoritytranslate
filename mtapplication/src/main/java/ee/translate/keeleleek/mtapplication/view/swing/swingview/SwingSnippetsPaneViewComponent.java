package ee.translate.keeleleek.mtapplication.view.swing.swingview;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.SpinnerNumberModel;

import ee.translate.keeleleek.mtapplication.view.elements.SnippetsPaneMediator;

public class SwingSnippetsPaneViewComponent extends JPanel {

	private static final long serialVersionUID = 7115905443530405160L;


	public SnippetsPaneMediator mediator;
	public JSpinner keySelection;
	public JLabel keyCodeLabel;
	public JTextArea snippetEdit;
	
	/**
	 * Create the panel.
	 */
	public SwingSnippetsPaneViewComponent() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		snippetEdit = new JTextArea();
		GridBagConstraints gbc_snippetEdit = new GridBagConstraints();
		gbc_snippetEdit.gridwidth = 2;
		gbc_snippetEdit.insets = new Insets(0, 0, 5, 0);
		gbc_snippetEdit.fill = GridBagConstraints.BOTH;
		gbc_snippetEdit.gridx = 0;
		gbc_snippetEdit.gridy = 0;
		add(snippetEdit, gbc_snippetEdit);
		
		keySelection = new JSpinner();
		keySelection.setModel(new SpinnerNumberModel(1, 1, 9, 1));
		GridBagConstraints gbc_keySelection = new GridBagConstraints();
		gbc_keySelection.fill = GridBagConstraints.HORIZONTAL;
		gbc_keySelection.insets = new Insets(0, 0, 0, 5);
		gbc_keySelection.gridx = 0;
		gbc_keySelection.gridy = 1;
		add(keySelection, gbc_keySelection);
		
		keyCodeLabel = new JLabel("");
		GridBagConstraints gbc_keyCodeLabel = new GridBagConstraints();
		gbc_keyCodeLabel.gridx = 1;
		gbc_keyCodeLabel.gridy = 1;
		add(keyCodeLabel, gbc_keyCodeLabel);

	}

}
