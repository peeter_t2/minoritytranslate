package ee.translate.keeleleek.mtapplication.view.swing.swingview;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtapplication.view.swing.dialogs.LoginDialogMediatorSwing;

public class SwingLoginWindowViewComponent extends JDialog {

	private static final long serialVersionUID = 4532704653705269836L;
	
	public LoginDialogMediatorSwing mediator;
	
	public final JPanel contentPanel = new JPanel();
	public JPasswordField passwordTextField;
	public JTextField usernameTextField;
	public JButton okButton;
	public JCheckBox stayLoggedCheckBox;

	/**
	 * Create the dialog.
	 */
	public SwingLoginWindowViewComponent(JFrame parent) {
		super(parent);
		setPreferredSize(new Dimension(250, 115));
		setBounds(100, 100, 250, 140);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JLabel lblUsername = new JLabel(Messages.getString("login.username")); //$NON-NLS-1$
			GridBagConstraints gbc_lblUsername = new GridBagConstraints();
			gbc_lblUsername.anchor = GridBagConstraints.EAST;
			gbc_lblUsername.insets = new Insets(0, 0, 5, 5);
			gbc_lblUsername.gridx = 0;
			gbc_lblUsername.gridy = 0;
			contentPanel.add(lblUsername, gbc_lblUsername);
		}
		{
			usernameTextField = new JTextField();
			usernameTextField.setMinimumSize(new Dimension(300, 19));
			GridBagConstraints gbc_usernameTextField = new GridBagConstraints();
			gbc_usernameTextField.insets = new Insets(0, 0, 5, 0);
			gbc_usernameTextField.fill = GridBagConstraints.HORIZONTAL;
			gbc_usernameTextField.gridx = 1;
			gbc_usernameTextField.gridy = 0;
			contentPanel.add(usernameTextField, gbc_usernameTextField);
			usernameTextField.setColumns(10);
		}
		{
			JLabel lblPassword = new JLabel(Messages.getString("login.password")); //$NON-NLS-1$
			GridBagConstraints gbc_lblPassword = new GridBagConstraints();
			gbc_lblPassword.anchor = GridBagConstraints.EAST;
			gbc_lblPassword.insets = new Insets(0, 0, 5, 5);
			gbc_lblPassword.gridx = 0;
			gbc_lblPassword.gridy = 1;
			contentPanel.add(lblPassword, gbc_lblPassword);
		}
		{
			passwordTextField = new JPasswordField();
			GridBagConstraints gbc_passwordTextField = new GridBagConstraints();
			gbc_passwordTextField.insets = new Insets(0, 0, 5, 0);
			gbc_passwordTextField.fill = GridBagConstraints.HORIZONTAL;
			gbc_passwordTextField.gridx = 1;
			gbc_passwordTextField.gridy = 1;
			contentPanel.add(passwordTextField, gbc_passwordTextField);
		}
		{
			stayLoggedCheckBox = new JCheckBox(Messages.getString("login.stay.logged.in")); //$NON-NLS-1$
			GridBagConstraints gbc_stayLoggedCheckBox = new GridBagConstraints();
			gbc_stayLoggedCheckBox.anchor = GridBagConstraints.WEST;
			gbc_stayLoggedCheckBox.gridwidth = 2;
			gbc_stayLoggedCheckBox.insets = new Insets(0, 0, 0, 5);
			gbc_stayLoggedCheckBox.gridx = 0;
			gbc_stayLoggedCheckBox.gridy = 2;
			contentPanel.add(stayLoggedCheckBox, gbc_stayLoggedCheckBox);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton(Messages.getString("button.cancel")); //$NON-NLS-1$
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				okButton = new JButton(Messages.getString("button.ok")); //$NON-NLS-1$
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
	}

}
