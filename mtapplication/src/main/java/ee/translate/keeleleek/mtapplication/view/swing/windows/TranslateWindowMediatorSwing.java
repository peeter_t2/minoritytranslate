package ee.translate.keeleleek.mtapplication.view.swing.windows;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JTabbedPane;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.view.elements.SnippetsPaneMediator;
import ee.translate.keeleleek.mtapplication.view.elements.TranslateTabMediator;
import ee.translate.keeleleek.mtapplication.view.swing.MediatorLoaderSwing;
import ee.translate.keeleleek.mtapplication.view.swing.elements.ArticlesMediatorSwing;
import ee.translate.keeleleek.mtapplication.view.swing.elements.NotesMediatorSwing;
import ee.translate.keeleleek.mtapplication.view.swing.elements.TranslateTabMediatorSwing.SwingTabObject;
import ee.translate.keeleleek.mtapplication.view.swing.swingview.SwingTranslateWindowViewComponent;
import ee.translate.keeleleek.mtapplication.view.windows.TranslateWindowMediator;

public class TranslateWindowMediatorSwing extends TranslateWindowMediator {

	protected ArrayList<TranslateTabMediator> upperTabs = new ArrayList<>();
	protected ArrayList<TranslateTabMediator> lowerTabs = new ArrayList<>();
	
	
	// INIT
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
		SnippetsPaneMediator snippetsMediator = MediatorLoaderSwing.loadSnippetsPaneMediator();
		getViewComponent().snippetsPage.add((Component) snippetsMediator.getViewComponent());
		getFacade().registerMediator(snippetsMediator);
	 }
    
	@Override
    protected String getPullPluginName() {
    	// TODO Auto-generated method stub
    	return null;
    }
	
	@Override
	public String findSelectedSrcLangCode() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	protected void showCheckingTitleBusy(boolean busy) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Mediator createArticlesMediator() {
		return new ArticlesMediatorSwing(getViewComponent().articleList);
	}

	@Override
	public Mediator createNotesMediator() {
		return new NotesMediatorSwing(getViewComponent().notesEdit);
	}

	@Override
	public SwingTranslateWindowViewComponent getViewComponent() {
		return (SwingTranslateWindowViewComponent) super.getViewComponent();
	}


	
	// INHERIT (CONTROLS)
	protected void setNavigationDisable(boolean disabled)
	 {
		getViewComponent().btnNext.setEnabled(!disabled);
		getViewComponent().nextArticleMenuItem.setEnabled(!disabled);
		
		getViewComponent().btnPrevious.setEnabled(!disabled);
		getViewComponent().previousArticleMenuItem.setEnabled(!disabled);
	 };
	
	
	@Override
	protected boolean isUpload() {
		return getViewComponent().uploadButton.isSelected();
	}
	
	@Override
	protected void setUpload(boolean selected) {
		getViewComponent().uploadButton.setSelected(selected);
	}

	@Override
	protected void setUploadDisable(boolean disabled)
	 {
		getViewComponent().uploadButton.setEnabled(!disabled);
		getViewComponent().uploadMenuItem.setEnabled(!disabled);
	 }
	
	
	@Override
	protected void setPullDisable(boolean disabled) {
		getViewComponent().pullButton.setEnabled(!disabled);
	}

	@Override
	protected void setAlignDisable(boolean disabled) {
		// TODO
	}
	@Override
	protected void showSpellCheckerBusy(boolean busy) {
		// TODO Auto-generated method stub
		
	}
	
	// INHERIT (MENU CONTROLS)
	@Override
	protected void setMenuUploadEnabled(boolean enabled) {
		getViewComponent().uploadEnabledMenuItem.setSelected(enabled);
	}
	
	@Override
	protected void disableMenuExactToggle(boolean disable) {
		getViewComponent().toggleExactMenuItem.setEnabled(!disable);
	}
	
	
	// INHERIT (EDITORS)
	public void openEditor(Editor editor)
	 {
		// TODO
	 };
	 
	 @Override
	public Editor getEditor() {
		return Editor.TRANSLATE;
	}
	
	
	// INHERIT (TABS)
	@Override
	protected TranslateTabMediator createTab(TabsBox box)
	 {
		// create mediator
		TranslateTabMediator tabMediator = MediatorLoaderSwing.loadTranslateTabMediator();
		ArrayList<TranslateTabMediator> tabs = getTabs(box);
		tabs.add(tabMediator);
		
		// add to view
		SwingTabObject tabObject = new SwingTabObject();
		tabObject.i = getTabCount(box) - 1;
		tabObject.pane = getTabPane(box);
		tabMediator.setTabObject(tabObject);
		getTabPane(box).addTab("", (Component) tabMediator.getViewComponent());
		
		return tabMediator;
	 }
	
	@Override
	protected TranslateTabMediator destroyTab(TabsBox box)
	 {
		// destroy mediator
		TranslateTabMediator tabMediator = getTabs(box).remove(getTabs(box).size() - 1);
		
		// remove from view
		SwingTabObject tabObject = (SwingTabObject) tabMediator.getTabObject();
		getTabPane(box).removeTabAt(tabObject.i);
		
		return tabMediator;
	 }
	
	@Override
	protected TranslateTabMediator getTab(TabsBox box, int i) {
		return getTabs(box).get(i);
	}
	
	@Override
	protected TranslateTabMediator getSelectedTab(TabsBox box)
	 {
		int i = getTabPane(box).getSelectedIndex();
		List<TranslateTabMediator> tabs = getTabs(box);
		if (i < 0 || i >= tabs.size()) return null;
		return tabs.get(i);
	 }
	
	@Override
	protected int getTabCount(TabsBox box) {
		return getTabs(box).size();
	}
	
	@Override
	protected ArrayList<TranslateTabMediator> getTabs(TabsBox box)
	 {
		if (box == TabsBox.UPPER) return upperTabs;
		else if (box == TabsBox.LOWER) return lowerTabs;
		else return new ArrayList<>();
	 }
	
	
	// INHERIT (DIVIDERS)
	@Override
	public int getAddonsDivider() {
		return getViewComponent().addonsSplitPane.getDividerLocation();
	}
	
	@Override
	public void setAddonsDivider(int position) {
		getViewComponent().addonsSplitPane.setDividerLocation(position);
	}
	
	@Override
	public double getTranslateDivider() {
		System.out.println((double)getViewComponent().translateSplitPane.getDividerLocation() / getViewComponent().translateSplitPane.getHeight());
		return (double)getViewComponent().translateSplitPane.getDividerLocation() / getViewComponent().translateSplitPane.getHeight();
	}
	
	@Override
	public void setTranslateDivider(double position) {
		getViewComponent().translateSplitPane.setDividerLocation(position);
	}
	
	
	// INHERIT (STATUSBAR)
	@Override
	protected void showWikis(String[] langCodes)
	 {
		getViewComponent().wikisPanel.removeAll();
		
		for (final String langCode : langCodes) {
			JLabel label = new JLabel("<HTML><FONT color=\"#000099\"><U>" + langCode + "</U></FONT></HTML>");
			label.setCursor(new Cursor(Cursor.HAND_CURSOR));
			label.addMouseListener(new MouseListener() {
				
				@Override
				public void mouseReleased(MouseEvent e) { }
				
				@Override
				public void mousePressed(MouseEvent e)
				 {
					onWikiClicked(langCode);
				 }
				
				@Override
				public void mouseExited(MouseEvent e) { }
				
				@Override
				public void mouseEntered(MouseEvent e) { }
				
				@Override
				public void mouseClicked(MouseEvent e) { }
			});
			getViewComponent().wikisPanel.add(label);
		}
	 }
	
	@Override
	protected void showTag(String tag) {
		getViewComponent().tagLabel.setText(tag);
	}
	
	@Override
	protected void showCount(String count) {
		getViewComponent().countLabel.setText(count);
	}
	
	
	@Override
	protected void setLoginIndicator(java.awt.Color colour, String tooltip) {
		getViewComponent().loginStatusLabel.setText(tooltip);
	}
	

	protected void setProgressVisible(boolean visible) {
//		getViewComponent().progressBar.setVisible(visible);
	}
	
	protected void setProgress(double value) {
//		getViewComponent().progressBar.setValue((int) (value * PROGRESS_MULTIPLIER));
	}
	
	
	@Override
	protected void showQueuerBusy(boolean busy) {
		getViewComponent().queingPanel.setVisible(busy);
	}
	
	@Override
	protected void showDownloaderBusy(boolean busy) {
		getViewComponent().downloadingPanel.setVisible(busy);
	}
	
	@Override
	protected void showPreviewerBusy(boolean busy) {
		getViewComponent().previewingPanel.setVisible(busy);
	}
	
	@Override
	protected void showUploaderBusy(boolean busy) {
		getViewComponent().uploadingPanel.setVisible(busy);
	}
	
	@Override
	protected void showSessionSaveBusy(boolean busy) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	protected void showPullingBusy(boolean busy) {
		// TODO Auto-generated method stub
		
	}
	

	// HELPERS
	private JTabbedPane getTabPane(TabsBox box)
	 {
		if (box == TabsBox.UPPER) return getViewComponent().upperTabPane;
		else if (box == TabsBox.LOWER) return getViewComponent().lowerTabPane;
		else return null;
	 }
	
	
}
