package ee.translate.keeleleek.mtapplication.view.javafx.fxemelents;

import ee.translate.keeleleek.mtapplication.model.preferences.Lookup;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class LookupFX {

	public final StringProperty name;
	public final StringProperty url;
	public final StringProperty parameters;
	public final StringProperty srcElement;
	public final StringProperty opening;
	public final StringProperty dstElement;
	public final StringProperty closing;
	
	
	public LookupFX(Lookup parameters)
	 {
		this.name = new SimpleStringProperty(parameters.getName());
		this.url = new SimpleStringProperty(parameters.getURL());
		this.parameters = new SimpleStringProperty(parameters.getParameters());
		this.srcElement = new SimpleStringProperty(parameters.getSrcElement());
		this.opening = new SimpleStringProperty(parameters.getOpening());
		this.dstElement = new SimpleStringProperty(parameters.getDstElement());
		this.closing = new SimpleStringProperty(parameters.getClosing());
	 }

	public Lookup toLookup()
	 {
		return new Lookup(name.getValueSafe(), url.getValueSafe(), parameters.getValueSafe(), srcElement.getValueSafe(), opening.getValueSafe(), dstElement.getValueSafe(), closing.getValueSafe());
	 }
	
	
}
