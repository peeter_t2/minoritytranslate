package ee.translate.keeleleek.mtapplication.model.preferences;

public class EntryFilter {

	private String srcLangRegex = ".*";
	private String dstLangRegex = ".*";
	
	public EntryFilter()
	 {
		this(null, null);
	 }

	public EntryFilter(String srcLang, String dstLang)
	 {
		if (srcLang == null || srcLang.isEmpty()) srcLang = ".*";
		if (dstLang == null || dstLang.isEmpty()) dstLang = ".*";
		
		this.srcLangRegex = srcLang;
		this.dstLangRegex = dstLang;
	 }

	public EntryFilter(EntryFilter other)
	 {
		this.srcLangRegex = other.srcLangRegex;
		this.dstLangRegex = other.dstLangRegex;
	 }
	
	
	public boolean isAccept(String srcLangCode, String dstLangCode)
	 {
		return srcLangCode.matches(this.srcLangRegex) && dstLangCode.matches(this.dstLangRegex);
	 }
	
	
	public String getSrcLangRegex() {
		return srcLangRegex;
	}
	public String getDstLangRegex() {
		return dstLangRegex;
	}

	
	// HELPERS
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dstLangRegex == null) ? 0 : dstLangRegex.hashCode());
		result = prime * result + ((srcLangRegex == null) ? 0 : srcLangRegex.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		EntryFilter other = (EntryFilter) obj;
		if (dstLangRegex == null) {
			if (other.dstLangRegex != null) return false;
		} else if (!dstLangRegex.equals(other.dstLangRegex)) return false;
		if (srcLangRegex == null) {
			if (other.srcLangRegex != null) return false;
		} else if (!srcLangRegex.equals(other.srcLangRegex)) return false;
		return true;
	}
	
	
}
