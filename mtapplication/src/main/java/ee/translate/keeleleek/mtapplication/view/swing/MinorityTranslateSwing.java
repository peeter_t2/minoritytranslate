package ee.translate.keeleleek.mtapplication.view.swing;

import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import org.slf4j.LoggerFactory;

public class MinorityTranslateSwing extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public static void main(String[] args) throws IOException
	 {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	    }
	    catch (Exception e) {
	    	LoggerFactory.getLogger(MinorityTranslateSwing.class).warn("Failed to set system look and feel", e);
	    }
		
		JOptionPane.showMessageDialog(null, "MinorityTranslate requires Java 8u40 with JavaFX to run", "Newer Java required", JOptionPane.ERROR_MESSAGE);
	 }

}
