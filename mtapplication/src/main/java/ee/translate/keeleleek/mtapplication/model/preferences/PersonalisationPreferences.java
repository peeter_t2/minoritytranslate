package ee.translate.keeleleek.mtapplication.model.preferences;



public class PersonalisationPreferences {

	private int addonsDivider;
	private double translateDivider;
	
	// INIT
	public PersonalisationPreferences()
	 {
		this.addonsDivider = 200;
		this.translateDivider = 0.5;
	 }

	public PersonalisationPreferences(PersonalisationPreferences preferences)
	 {
		this.translateDivider = preferences.translateDivider;
		this.addonsDivider = preferences.addonsDivider;
	 }
	

	// PREFERENCES
	public int getAddonsDivider() {
		return addonsDivider;
	}
	
	public void setAddonsDivider(int addonsDivider) {
		this.addonsDivider = addonsDivider;
	}
	
	public double getTranslateDivider() {
		return translateDivider;
	}
	
	public void setTranslateDivider(double translateDivider) {
		this.translateDivider = translateDivider;
	}

	
	// UTILITY
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(addonsDivider);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(translateDivider);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		PersonalisationPreferences other = (PersonalisationPreferences) obj;
		if (Double.doubleToLongBits(addonsDivider) != Double.doubleToLongBits(other.addonsDivider)) return false;
		if (Double.doubleToLongBits(translateDivider) != Double.doubleToLongBits(other.translateDivider)) return false;
		return true;
	}
	
	
}
