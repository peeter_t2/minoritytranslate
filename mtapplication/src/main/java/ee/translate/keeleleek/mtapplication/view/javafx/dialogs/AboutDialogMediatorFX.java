package ee.translate.keeleleek.mtapplication.view.javafx.dialogs;

import ee.translate.keeleleek.mtapplication.model.media.LogosProxy;
import ee.translate.keeleleek.mtapplication.view.dialogs.AboutDialogMediator;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


public class AboutDialogMediatorFX extends AboutDialogMediator {

	@FXML
	private Label versionLabel;
	
	@FXML
	private ImageView iconImageView;
	@FXML
	private ImageView wmfImageView;
	@FXML
	private ImageView keeleleekImageView;
	
	@FXML
	private Hyperlink licenceHyperling;
	
	
	// IMPLEMENTATION:
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
		LogosProxy logosProxi = (LogosProxy) getFacade().retrieveProxy(LogosProxy.NAME);
		
		iconImageView.setImage((Image) logosProxi.retrieveApplicationLogo());
		
		wmfImageView.setImage((Image) logosProxi.retrieveWMFLogo());
		wmfImageView.setCursor(Cursor.HAND);
		
		keeleleekImageView.setImage((Image) logosProxi.retrieveKeeleleekLogo());
		keeleleekImageView.setCursor(Cursor.HAND);
	 }

	@Override
	protected void setVersion(String version, String date) {
		versionLabel.setText(version + " (" + date + ")");
	}
	
	// BUTTONS:
	@FXML
	@Override
	public void onLicenceHyperlinkClick() {
		super.onLicenceHyperlinkClick();
	}

	@FXML
	@Override
	public void onWMFImageViewClick() {
		super.onWMFImageViewClick();
	}

	@FXML
	@Override
	public void onKeeleleekImageViewClick() {
		super.onKeeleleekImageViewClick();
	}
	
}
