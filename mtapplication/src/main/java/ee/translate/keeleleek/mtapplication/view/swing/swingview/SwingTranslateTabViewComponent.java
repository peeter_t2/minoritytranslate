package ee.translate.keeleleek.mtapplication.view.swing.swingview;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.ScrollPaneConstants;

import ee.translate.keeleleek.mtapplication.view.elements.TranslateTabMediator;
import ee.translate.keeleleek.mtapplication.view.elements.TranslateTabMediator.TabMode;
import ee.translate.keeleleek.mtapplication.view.elements.TranslateTabMediator.TabView;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;

public class SwingTranslateTabViewComponent extends JPanel {

	private static final long serialVersionUID = 3937043716749903843L;
	
	public TranslateTabMediator mediator;
	public JTextField titleEdit;
	public JPanel middleBox;
	public JToggleButton previewButton;
	private JScrollPane editBox;
	public JEditorPane textEdit;
	private JScrollPane previewBox;
	public JEditorPane previewView;
	public JLabel statusLabel;
	private JPanel editBar;
	public JCheckBox exactCheckbox;
	private JPanel panel;
	public JPanel bar;
	private JPanel filterBar;
	private JPanel panel_1;
	public JCheckBox filterTemplatesCheckbox;
	public JCheckBox filterFilesCheckbox;
	public JCheckBox filterIntroductionCheckbox;
	public JCheckBox filterReferencesCheckbox;
	
	public SwingTranslateTabViewComponent() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 1.0, 0.0};
		gridBagLayout.rowWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		titleEdit = new JTextField();
		titleEdit.setFont(new Font("Dialog", Font.BOLD, 15));
		GridBagConstraints gbc_titleEdit = new GridBagConstraints();
		gbc_titleEdit.weightx = 2.5;
		gbc_titleEdit.insets = new Insets(0, 0, 5, 5);
		gbc_titleEdit.fill = GridBagConstraints.BOTH;
		gbc_titleEdit.gridx = 0;
		gbc_titleEdit.gridy = 0;
		add(titleEdit, gbc_titleEdit);
		titleEdit.setColumns(10);
		
		JPanel statusPanel = new JPanel();
		FlowLayout fl_statusPanel = (FlowLayout) statusPanel.getLayout();
		fl_statusPanel.setVgap(0);
		fl_statusPanel.setHgap(0);
		GridBagConstraints gbc_statusPanel = new GridBagConstraints();
		gbc_statusPanel.anchor = GridBagConstraints.WEST;
		gbc_statusPanel.weightx = 1.0;
		gbc_statusPanel.insets = new Insets(0, 0, 5, 5);
		gbc_statusPanel.gridx = 1;
		gbc_statusPanel.gridy = 0;
		add(statusPanel, gbc_statusPanel);
		
		statusLabel = new JLabel("<status>");
		statusLabel.setPreferredSize(new Dimension(64, 15));
		statusLabel.setMinimumSize(new Dimension(64, 15));
		statusLabel.setMaximumSize(new Dimension(64, 15));
		statusPanel.add(statusLabel);
		
		previewButton = new JToggleButton(Messages.getString("toolbar.preview.button"));
		previewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (previewButton.isSelected()) mediator.onChangeView(TabView.PREVIEW);
				else mediator.onChangeView(TabView.EDIT);
			}
		});
		GridBagConstraints gbc_previewButton = new GridBagConstraints();
		gbc_previewButton.anchor = GridBagConstraints.EAST;
		gbc_previewButton.weightx = 1.0;
		gbc_previewButton.insets = new Insets(0, 0, 5, 0);
		gbc_previewButton.gridx = 2;
		gbc_previewButton.gridy = 0;
		add(previewButton, gbc_previewButton);
		
		middleBox = new JPanel();
		GridBagConstraints gbc_middleBox = new GridBagConstraints();
		gbc_middleBox.insets = new Insets(0, 0, 5, 0);
		gbc_middleBox.gridwidth = 3;
		gbc_middleBox.fill = GridBagConstraints.BOTH;
		gbc_middleBox.gridx = 0;
		gbc_middleBox.gridy = 1;
		add(middleBox, gbc_middleBox);
		middleBox.setLayout(new CardLayout(0, 0));
		
		editBox = new JScrollPane();
		editBox.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		middleBox.add(editBox, TabView.EDIT.toString());
		
		textEdit = new JEditorPane();
		textEdit.setFont(new Font("Dialog", Font.PLAIN, 13));
		editBox.setViewportView(textEdit);
		
		previewBox = new JScrollPane();
		previewBox.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		middleBox.add(previewBox, TabView.PREVIEW.toString());
		
		previewView = new JEditorPane();
		previewView.setEditable(false);
		previewView.setContentType("text/html");
		previewBox.setViewportView(previewView);
		
		bar = new JPanel();
		GridBagConstraints gbc_bar = new GridBagConstraints();
		gbc_bar.gridwidth = 3;
		gbc_bar.insets = new Insets(0, 0, 0, 5);
		gbc_bar.fill = GridBagConstraints.HORIZONTAL;
		gbc_bar.gridx = 0;
		gbc_bar.gridy = 2;
		add(bar, gbc_bar);
		bar.setLayout(new CardLayout(0, 0));
		
		editBar = new JPanel();
		bar.add(editBar, TabMode.NORMAL.toString());
		GridBagLayout gbl_editBar = new GridBagLayout();
		gbl_editBar.columnWidths = new int[]{142, 145, 0};
		gbl_editBar.rowHeights = new int[]{23, 0};
		gbl_editBar.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gbl_editBar.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		editBar.setLayout(gbl_editBar);
		
		exactCheckbox = new JCheckBox(Messages.getString("toolbar.exact.checkbox")); //$NON-NLS-1$
		exactCheckbox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onChangeExact(exactCheckbox.isSelected());
			}
		});
		
		panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel.insets = new Insets(0, 0, 0, 5);
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		editBar.add(panel, gbc_panel);
		GridBagConstraints gbc_exactCheckbox = new GridBagConstraints();
		gbc_exactCheckbox.anchor = GridBagConstraints.NORTHWEST;
		gbc_exactCheckbox.gridx = 1;
		gbc_exactCheckbox.gridy = 0;
		editBar.add(exactCheckbox, gbc_exactCheckbox);
		
		filterBar = new JPanel();
		bar.add(filterBar, TabMode.FILTERED.toString());
		GridBagLayout gbl_filterBar = new GridBagLayout();
		gbl_filterBar.columnWidths = new int[]{71, 12, 140, 146, 0, 0, 0};
		gbl_filterBar.rowHeights = new int[]{23, 0};
		gbl_filterBar.columnWeights = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_filterBar.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		filterBar.setLayout(gbl_filterBar);
		
		panel_1 = new JPanel();
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel_1.insets = new Insets(0, 0, 0, 5);
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 0;
		filterBar.add(panel_1, gbc_panel_1);
		
		filterTemplatesCheckbox = new JCheckBox(Messages.getString("statusbar.filters.templates")); //$NON-NLS-1$
		filterTemplatesCheckbox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onChangeTemplatesFilter(filterTemplatesCheckbox.isSelected());
			}
		});
		GridBagConstraints gbc_filterTemplatesCheckbox = new GridBagConstraints();
		gbc_filterTemplatesCheckbox.anchor = GridBagConstraints.NORTHWEST;
		gbc_filterTemplatesCheckbox.insets = new Insets(0, 0, 0, 5);
		gbc_filterTemplatesCheckbox.gridx = 1;
		gbc_filterTemplatesCheckbox.gridy = 0;
		filterBar.add(filterTemplatesCheckbox, gbc_filterTemplatesCheckbox);
		
		filterFilesCheckbox = new JCheckBox(Messages.getString("statusbar.filters.files")); //$NON-NLS-1$
		filterFilesCheckbox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onChangeFilesFilter(filterFilesCheckbox.isSelected());
			}
		});
		GridBagConstraints gbc_filterFilesCheckbox = new GridBagConstraints();
		gbc_filterFilesCheckbox.anchor = GridBagConstraints.NORTH;
		gbc_filterFilesCheckbox.insets = new Insets(0, 0, 0, 5);
		gbc_filterFilesCheckbox.gridx = 2;
		gbc_filterFilesCheckbox.gridy = 0;
		filterBar.add(filterFilesCheckbox, gbc_filterFilesCheckbox);
		
		filterIntroductionCheckbox = new JCheckBox(Messages.getString("statusbar.filters.introduction")); //$NON-NLS-1$
		filterIntroductionCheckbox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onChangeIntroductionFilter(filterIntroductionCheckbox.isSelected());
			}
		});
		GridBagConstraints gbc_filterIntroductionCheckbox = new GridBagConstraints();
		gbc_filterIntroductionCheckbox.insets = new Insets(0, 0, 0, 5);
		gbc_filterIntroductionCheckbox.anchor = GridBagConstraints.NORTHWEST;
		gbc_filterIntroductionCheckbox.gridwidth = 2;
		gbc_filterIntroductionCheckbox.gridx = 3;
		gbc_filterIntroductionCheckbox.gridy = 0;
		filterBar.add(filterIntroductionCheckbox, gbc_filterIntroductionCheckbox);
		
		filterReferencesCheckbox = new JCheckBox(Messages.getString("statusbar.filters.references")); //$NON-NLS-1$
		filterReferencesCheckbox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onChangeReferencesFilter(filterReferencesCheckbox.isSelected());
			}
		});
		GridBagConstraints gbc_filterReferencesCheckbox = new GridBagConstraints();
		gbc_filterReferencesCheckbox.anchor = GridBagConstraints.NORTHWEST;
		gbc_filterReferencesCheckbox.gridx = 5;
		gbc_filterReferencesCheckbox.gridy = 0;
		filterBar.add(filterReferencesCheckbox, gbc_filterReferencesCheckbox);

	}

}
