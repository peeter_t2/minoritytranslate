package ee.translate.keeleleek.mtapplication.model.application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

import org.puremvc.java.multicore.patterns.proxy.Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;

import ee.translate.keeleleek.mtapplication.MinorityTranslate;
import ee.translate.keeleleek.mtapplication.common.FileUtil;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;

public class ApplicationProxy extends Proxy implements Runnable {

	public final static String NAME = "{2DF4C5FA-C50F-4F67-90DD-02DB32089F06}";
	
	private static Logger LOGGER = LoggerFactory.getLogger(ApplicationProxy.class);
	
	public final static Path FILE_PATH = Paths.get(MinorityTranslate.ROOT_PATH + "/message-versions.json");
	public final static String JAR_PATH = "/message-versions.json";
	public final static String VERSIONS_URL = "http://translate.keeleleek.ee/download/versions/messages/message-versions.json";
	public final static String MESSAGES_URL = "http://translate.keeleleek.ee/download/versions/messages/messages_#.properties";
	
	private MessageVersions msgVersions;
	
	
	// INIT
	public ApplicationProxy() {
		super(NAME, "");
	}

	@Override
	public void onRegister()
	 {
		new Thread(this).start();
	 }
	
	@Override
	public void onRemove()
	 {
	 }
	
	
	// UPDATING
	public void run()
	 {
		work();
	 };
	
	private void work()
	 {
		// read versions
		String json;
		try {
			if (FILE_PATH.toFile().isFile()) {
				json = FileUtil.read(FILE_PATH);
			} else {
				json = FileUtil.readFromJar(JAR_PATH);
			}
		} catch (IOException e) {
			LOGGER.error("Failed to read versions", e);
			return;
		}
		
		GsonBuilder builder = new GsonBuilder().setPrettyPrinting();
		Gson gson = builder.create();
		try {
			msgVersions = gson.fromJson(json, MessageVersions.class);
		} catch (JsonParseException e) {
			LOGGER.error("Failed to parse versions", e);
			return;
		}
		
		// download latest
		try {
			json = downloadingContent(new URL(VERSIONS_URL));
		} catch (UnknownHostException e) {
			LOGGER.info("Skipping downloading latest version info");
			return; // no internet or website is down
		} catch (Exception e) {
			LOGGER.error("Failed to download latest version info!", e);
			return;
		}
		
		MessageVersions latestVersions;
		try {
			latestVersions = gson.fromJson(json, MessageVersions.class);
		} catch (JsonParseException e) {
			LOGGER.error("Failed to parse latest versions", e);
			return;
		}
		
		// compare language versions
		boolean msgsChanged = false;
		
		Set<String> langCodes = msgVersions.getLangCodes();
		langCodes.addAll(latestVersions.getLangCodes());
		
		for (String langCode : langCodes) {
			String versionCurrent = msgVersions.getLangVersion(langCode);
			String versionLatest = latestVersions.getLangVersion(langCode);
			
			if (versionLatest == null) continue;
			
			if (versionCurrent == null || !versionLatest.equals(versionCurrent)) {
				if (downloadMessages(langCode)) {
					msgVersions.setLangVersion(langCode, versionLatest);
					msgsChanged = true;
				}
			}
			
		}
		
		// write versions
		if (msgsChanged) {
			
			json = gson.toJson(this.msgVersions);
			
			try {
				FileUtil.write(FILE_PATH, json);
			} catch (IOException e) {
				LOGGER.error("Failed to write versions", e);
			}
			
		}
		
		// init messages
		if (msgsChanged) Messages.init();
	 }
	
	private static boolean downloadMessages(String langCode)
	 {
		try {
			String messages = downloadingContent(new URL(MESSAGES_URL.replace("#", langCode)));
			FileUtil.write(Paths.get(MinorityTranslate.ROOT_PATH + "/" + Messages.I18N_NAME + "/" + Messages.BUNDLE_NAME + "_" + langCode + ".properties"), messages);
		} catch (Exception e) {
			LOGGER.error("Failed to download messages!", e);
			return false;
		}
		
		return true;
	 }
	
	
	// HELPERS
	private static String downloadingContent(URL url) throws IOException
	 {
		LOGGER.info("Downloading " + url);
		
		BufferedReader in = null;
		try {
			
			in = new BufferedReader(new InputStreamReader(url.openStream()));
			StringBuilder strbldr = new StringBuilder();
			String line;
			while ((line = in.readLine()) != null) {
				if (strbldr.length() > 0) strbldr.append('\n');
				strbldr.append(line);
			}
			return strbldr.toString();
			
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
	
	
}
