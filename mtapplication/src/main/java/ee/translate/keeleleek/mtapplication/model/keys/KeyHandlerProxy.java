package ee.translate.keeleleek.mtapplication.model.keys;

import org.puremvc.java.multicore.patterns.proxy.Proxy;

import ee.translate.keeleleek.mtapplication.Notifications;

public class KeyHandlerProxy extends Proxy {

	public final static String NAME = "{5FC8E560-D453-4495-A8A6-B969F76482E8}";

	public enum KeyShortcut
	 {
		SNIPPET_1, SNIPPET_2, SNIPPET_3, SNIPPET_4, SNIPPET_5, SNIPPET_6, SNIPPET_7, SNIPPET_8, SNIPPET_9,
	 }
	
	
	// INIT
	public KeyHandlerProxy() {
		super(NAME, null);
	}

	
	public void handle(KeyShortcut shortcut)
	 {
		int index;
		
		switch (shortcut) {
		case SNIPPET_1:
		case SNIPPET_2:
		case SNIPPET_3:
		case SNIPPET_4:
		case SNIPPET_5:
		case SNIPPET_6:
		case SNIPPET_7:
		case SNIPPET_8:
		case SNIPPET_9:
			index = getSnippetIndexFor(shortcut);
			if (index != -1) sendNotification(Notifications.PROCESSING_PROCESS_PASTE, index);
			break;

		default:
			break;
		}
	 }
	
	public int getSnippetIndexFor(KeyShortcut shortcut)
	 {
		switch (shortcut) {
    		case SNIPPET_1: return 0;
    		case SNIPPET_2: return 1;
    		case SNIPPET_3: return 2;
    		case SNIPPET_4: return 3;
    		case SNIPPET_5: return 4;
    		case SNIPPET_6: return 5;
    		case SNIPPET_7: return 6;
    		case SNIPPET_8: return 7;
    		case SNIPPET_9: return 8;
    		default: return -1;
		}
	 }
	
	
}
