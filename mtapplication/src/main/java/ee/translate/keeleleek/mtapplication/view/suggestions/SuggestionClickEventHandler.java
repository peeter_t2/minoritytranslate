package ee.translate.keeleleek.mtapplication.view.suggestions;

import javafx.event.ActionEvent;

public class SuggestionClickEventHandler implements javafx.event.EventHandler<ActionEvent> {

	
	private final String suggestion;
	private final Suggestable suggestable;

	
	public SuggestionClickEventHandler(String suggestion, Suggestable suggestable) {
		super();
		this.suggestion = suggestion;
		this.suggestable = suggestable;
	}


	/**
	 * Gets the suggestion.
	 * 
	 * @return the suggestion
	 */
	public String getSuggestion() {
		return suggestion;
	}


	@Override
	public void handle(ActionEvent event) {
		suggestable.update(suggestion);
	}

}
