package ee.translate.keeleleek.mtapplication.view.javafx.fxemelents;

import ee.translate.keeleleek.mtpluginframework.mapping.PullMapping;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

public class PullMappingFX {

	private final static String TOUCHED = "\u2714";
	private final static String NOT_TOUCHED = "\u2718";
	
	private PullMapping mapping;

	private final SimpleBooleanProperty touched;
	private final SimpleStringProperty group;
	private final SimpleStringProperty source;
	private final SimpleStringProperty destination;
	
	public PullMappingFX(PullMapping mapping)
	 {
		this.mapping = mapping;
		
		this.touched = new SimpleBooleanProperty(mapping.isTouched());
		this.group = new SimpleStringProperty(mapping.getGroup());
		this.source = new SimpleStringProperty(mapping.getSource());
		this.destination = new SimpleStringProperty(mapping.getDestination());
	 }

	public String getTouched() {
        if (touched.get()) return TOUCHED;
		return NOT_TOUCHED;
    }
	
	public String getGroup() {
		return group.get();
	}
	
	public SimpleStringProperty getGroupProperty() {
        return group;
    }

	public String getSource() {
		return source.get();
	}
	
	public void setSource(String value) {
		source.set(value);
	}

	public SimpleStringProperty getSourceProperty() {
        return source;
    }
	
	public String getDestination() {
		return destination.get();
	}

	public SimpleStringProperty getDestinationProperty() {
        return destination;
    }
	
	public void setDestination(String value) {
		destination.set(value);
	}

	public void apply() {
		mapping.setDestination(getDestination());
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mapping == null) ? 0 : mapping.hashCode());
		result = prime * result + ((destination == null) ? 0 : destination.hashCode());
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PullMappingFX other = (PullMappingFX) obj;
		if (mapping == null) {
			if (other.mapping != null)
				return false;
		} else if (!mapping.equals(other.mapping))
			return false;
		if (destination == null) {
			if (other.destination != null)
				return false;
		} else if (!destination.equals(other.destination))
			return false;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		return true;
	}

}
