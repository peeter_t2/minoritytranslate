package ee.translate.keeleleek.mtapplication.model.media;

import org.puremvc.java.multicore.patterns.proxy.Proxy;

import javafx.scene.image.Image;

public class KeeleleekProxy extends Proxy {

	public final static String NAME = "{2D428284-FCEF-4286-834A-C14DA8CCC0AB}";
	
	
	// INITIATION:
	public KeeleleekProxy() {
		super(NAME, null);
	}

	/**
	 * Gets logo.
	 * 
	 * @return logo
	 */
	public Image retrieveLogo() {
		if (getData() == null) setData(new Image("keeleleek.png"));
		return (Image) getData();
	}
	
}
