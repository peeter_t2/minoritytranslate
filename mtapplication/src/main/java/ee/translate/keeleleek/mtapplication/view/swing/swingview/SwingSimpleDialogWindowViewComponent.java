package ee.translate.keeleleek.mtapplication.view.swing.swingview;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import ee.translate.keeleleek.mtapplication.view.dialogs.SimpleDialogMediator;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;

public class SwingSimpleDialogWindowViewComponent extends JPanel {

	private static final long serialVersionUID = 5045426811713862922L;

	public SimpleDialogMediator mediator;
	
	public JTextField titleEdit;

	public JPanel rootComponent;
	
	public JLabel headingLabel;
	public JLabel titleLabel;
	public JComboBox<String> langCombo;
	public JButton okButton;

	
	/**
	 * Create the panel.
	 */
	public SwingSimpleDialogWindowViewComponent() {
		setPreferredSize(new Dimension(280, 120));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{280, 0};
		gridBagLayout.rowHeights = new int[]{110, 0};
		gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		rootComponent = new JPanel();
		GridBagConstraints gbc_rootComponent = new GridBagConstraints();
		gbc_rootComponent.insets = new Insets(7, 7, 7, 7);
		gbc_rootComponent.fill = GridBagConstraints.BOTH;
		gbc_rootComponent.gridx = 0;
		gbc_rootComponent.gridy = 0;
		add(rootComponent, gbc_rootComponent);
		GridBagLayout gbl_rootComponent = new GridBagLayout();
		gbl_rootComponent.columnWidths = new int[]{62, 87, 0};
		gbl_rootComponent.rowHeights = new int[]{15, 5, 19, 0, 5, 0};
		gbl_rootComponent.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_rootComponent.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		rootComponent.setLayout(gbl_rootComponent);
		
		headingLabel = new JLabel("%Heading");
		GridBagConstraints gbc_headingLabel = new GridBagConstraints();
		gbc_headingLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_headingLabel.insets = new Insets(0, 0, 5, 0);
		gbc_headingLabel.gridwidth = 2;
		gbc_headingLabel.gridx = 0;
		gbc_headingLabel.gridy = 0;
		rootComponent.add(headingLabel, gbc_headingLabel);
		
		titleLabel = new JLabel("%Title");
		titleLabel.setFont(new Font("Dialog", Font.PLAIN, 12));
		GridBagConstraints gbc_textLabel = new GridBagConstraints();
		gbc_textLabel.anchor = GridBagConstraints.WEST;
		gbc_textLabel.insets = new Insets(0, 0, 5, 5);
		gbc_textLabel.gridx = 0;
		gbc_textLabel.gridy = 1;
		rootComponent.add(titleLabel, gbc_textLabel);
		
		titleEdit = new JTextField();
		GridBagConstraints gbc_textTextField = new GridBagConstraints();
		gbc_textTextField.anchor = GridBagConstraints.NORTH;
		gbc_textTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textTextField.insets = new Insets(0, 0, 5, 0);
		gbc_textTextField.gridx = 1;
		gbc_textTextField.gridy = 1;
		rootComponent.add(titleEdit, gbc_textTextField);
		titleEdit.setColumns(10);
		
		JLabel lblLanguage = new JLabel(Messages.getString("sdialog.language")); //$NON-NLS-1$
		lblLanguage.setFont(new Font("Dialog", Font.PLAIN, 12));
		GridBagConstraints gbc_lblLanguage = new GridBagConstraints();
		gbc_lblLanguage.anchor = GridBagConstraints.WEST;
		gbc_lblLanguage.insets = new Insets(0, 0, 5, 5);
		gbc_lblLanguage.gridx = 0;
		gbc_lblLanguage.gridy = 2;
		rootComponent.add(lblLanguage, gbc_lblLanguage);
		
		langCombo = new JComboBox<String>();
		GridBagConstraints gbc_langCombo = new GridBagConstraints();
		gbc_langCombo.insets = new Insets(0, 0, 5, 0);
		gbc_langCombo.fill = GridBagConstraints.HORIZONTAL;
		gbc_langCombo.gridx = 1;
		gbc_langCombo.gridy = 2;
		rootComponent.add(langCombo, gbc_langCombo);
		
		JPanel padding = new JPanel();
		GridBagConstraints gbc_padding = new GridBagConstraints();
		gbc_padding.anchor = GridBagConstraints.EAST;
		gbc_padding.gridwidth = 2;
		gbc_padding.gridx = 0;
		gbc_padding.gridy = 4;
		rootComponent.add(padding, gbc_padding);
		
		JButton btnCancel = new JButton(Messages.getString("button.cancel")); //$NON-NLS-1$
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.decline();
			}
		});
		padding.add(btnCancel);
		
		okButton = new JButton(Messages.getString("button.ok")); //$NON-NLS-1$
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mediator.confirm();
			}
		});
		padding.add(okButton);

	}

}
