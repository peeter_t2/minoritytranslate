package ee.translate.keeleleek.mtapplication.view.swing.dialogs;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.preferences.CollectEntry;
import ee.translate.keeleleek.mtapplication.model.preferences.ContentAssistPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.CorpusPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.InsertsPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.LanguagesPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.LookupsPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.PreferencesProxy.Display;
import ee.translate.keeleleek.mtapplication.model.preferences.PreferencesProxy.Proficiency;
import ee.translate.keeleleek.mtapplication.model.preferences.ProcessingPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.ProgramPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.ReplaceEntry;
import ee.translate.keeleleek.mtapplication.model.preferences.SymbolsPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.TemplateMappingPreferences;
import ee.translate.keeleleek.mtapplication.view.dialogs.PreferencesDialogMediator;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtapplication.view.suggestions.Suggestable;
import ee.translate.keeleleek.mtapplication.view.swing.swingview.SwingPreferencesWindowViewComponent;


public class PreferencesDialogMediatorSwing extends PreferencesDialogMediator {
	
	public enum PreferencesPage { NONE, PROGRAM, LANGUAGES, CORPUS, PROCESSING, COLLECT, REPLACE }
	
	private DefaultMutableTreeNode rootNode;
	private DefaultMutableTreeNode programNode;
	private DefaultMutableTreeNode languagesNode;
	private DefaultMutableTreeNode corpusNode;
	private DefaultMutableTreeNode processingNode;
	private DefaultMutableTreeNode collectNode;
	private DefaultMutableTreeNode replaceNode;
	
	private Suggestable toLanguageSuggestable = new Suggestable() {

		private JPopupMenu lastMenu;
		
		@Override
		public void update(String suggested) {
			manual = true;
			JTextField textField = getViewComponent().langsAddField;
			textField.setText(suggested);
			textField.setCaretPosition(suggested.length());
			manual = false;
		}
		
		@Override
		public void suggest(String[] results)
		 {
			JPopupMenu currentMenu = new JPopupMenu();
			JTextField textField = getViewComponent().langsAddField;
			
			for (int i = 0; i < results.length; i++) {
				JMenuItem item = new JMenuItem(results[i]);
				item.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						update(e.getActionCommand());
					}
					
				});
				currentMenu.add(item);
			}
			
			if (results.length > 0){
				currentMenu.show(textField, -1, textField.getHeight());
				textField.requestFocus();
				lastMenu = currentMenu;
			} else {
				if (lastMenu != null) {
					lastMenu.setVisible(false);
					lastMenu = null;
				}
			}
		 }
		
		@Override
		public String getSearchTerm() {
			return getViewComponent().langsAddField.getText();
		}
		
	};
@Override
protected TemplateMappingPreferences closeTemplateMapping() {
	// TODO Auto-generated method stub
	return null;
}

@Override
protected void openTemplateMapping(TemplateMappingPreferences preferences) {
	// TODO Auto-generated method stub
	
}
	private boolean feedback = false;
	
	
	// INIT
	public PreferencesDialogMediatorSwing() {
		super();
	}

	@Override
	public void onRegister()
	 {
		super.onRegister();
		
		// tree
		Icon empty = new Icon()
		 {
			@Override
			public void paintIcon(Component c, Graphics g, int x, int y) { }
			
			@Override
			public int getIconWidth() { return 0; }
			
			@Override
			public int getIconHeight() { return 0; }
		 };
		DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer) getViewComponent().preferencesTree.getCellRenderer();
		renderer.setLeafIcon(empty);
		renderer.setClosedIcon(empty);
		renderer.setOpenIcon(empty);
		
		DefaultTreeModel treeModel = (DefaultTreeModel) getViewComponent().preferencesTree.getModel();
		rootNode = (DefaultMutableTreeNode) treeModel.getRoot();
		getViewComponent().preferencesTree.addTreeSelectionListener(new TreeSelectionListener() {
			@Override
			public void valueChanged(TreeSelectionEvent e) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) getViewComponent().preferencesTree.getLastSelectedPathComponent();
				show(node);
			}
		});
		
		// program
		programNode = new DefaultMutableTreeNode(Messages.getString("preferences.program"));
		getViewComponent().preferencesTree.expandPath(new TreePath(programNode.getPath()));
		treeModel.insertNodeInto(programNode, rootNode, rootNode.getChildCount());

		DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>();	
		getViewComponent().guiLangCombo.setModel(model);
		
		getViewComponent().guiLangCombo.removeAllItems();
		for (int i = 0; i < guiLangNames.length; i++) {
			getViewComponent().guiLangCombo.addItem(guiLangNames[i]);
		}
		
		// languages
		languagesNode = new DefaultMutableTreeNode(Messages.getString("preferences.program.languages"));
		treeModel.insertNodeInto(languagesNode, programNode, programNode.getChildCount());

		getViewComponent().langsAddField.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				if (!manual) sendNotification(Notifications.SUGGEST_LANGUAGE, toLanguageSuggestable);
			}
			
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				if (!manual) sendNotification(Notifications.SUGGEST_LANGUAGE, toLanguageSuggestable);
			}
			
			@Override
			public void changedUpdate(DocumentEvent arg0) {
				
			}
			
		});

		// corpus
		corpusNode = new DefaultMutableTreeNode(Messages.getString("preferences.program.corpus"));
		treeModel.insertNodeInto(corpusNode, programNode, programNode.getChildCount());
		
		// processing
		processingNode = new DefaultMutableTreeNode(Messages.getString("preferences.processing.page"));
		treeModel.insertNodeInto(processingNode, rootNode, rootNode.getChildCount());

		getViewComponent().processingLink.setText("<HTML><FONT color=\"#000099\"><U>" + Messages.getString("preferences.processing.link") + "</U></FONT></HTML>");
		
		// collect
		collectNode = new DefaultMutableTreeNode(Messages.getString("preferences.processing.collect.page"));
		treeModel.insertNodeInto(collectNode, processingNode, processingNode.getChildCount());

		final DefaultTableModel collectModel = (DefaultTableModel) getViewComponent().collectTable.getModel();
		collectModel.setColumnCount(3);
		
		collectModel.addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent e)
			 {
				if (feedback) return;
				int r = e.getFirstRow();
				int c = e.getColumn();
				if (r == -1 || c == -1) return;
				processing.fromStringCollect(r, c, (String)collectModel.getValueAt(r, c));
			 }
		});
		
		// replace
		replaceNode = new DefaultMutableTreeNode(Messages.getString("preferences.processing.replace.page"));
		treeModel.insertNodeInto(replaceNode, processingNode, processingNode.getChildCount());

		final DefaultTableModel replaceModel = (DefaultTableModel) getViewComponent().replaceTable.getModel();
		replaceModel.setColumnCount(3);
		
		replaceModel.addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent e)
			 {
				if (feedback) return;
				int r = e.getFirstRow();
				int c = e.getColumn();
				if (r == -1 || c == -1) return;
				processing.fromStringReplace(r, c, (String)replaceModel.getValueAt(r, c));
			 }
		});
		
		// expand tree
		DefaultMutableTreeNode currentNode = rootNode.getNextNode();
		do {
			if (currentNode.getLevel()==1) 
				getViewComponent().preferencesTree.expandPath(new TreePath(currentNode.getPath()));
				currentNode = currentNode.getNextNode();
		   }
		while (currentNode != null);
		
		// select tree
		getViewComponent().preferencesTree.setSelectionPath(new TreePath(programNode.getPath()));
	 }
	
	public SwingPreferencesWindowViewComponent getViewComponent() {
		return (SwingPreferencesWindowViewComponent) super.getViewComponent();
	}
	
	
	// INHERIT (INIT)
	@Override
	protected void initProgram(ProgramPreferences program)
	 {
		getViewComponent().categDepthField.setValue(program.getCategoryDepth());
		
		getViewComponent().templatesCheckbox.setSelected(program.isTemplatesFilter());
		getViewComponent().filesCheckbox.setSelected(program.isFilesFilter());
		getViewComponent().introductionCheckbox.setSelected(program.isIntroductionFilter());
		getViewComponent().referencesCheckbox.setSelected(program.isReferencesFilter());
		
		int selected = -1;
		for (int i = 0; i < guiLangCodes.length; i++) {
			if (guiLangCodes[i].equals(program.getGUILangCode())) selected = i;
		}
		if (selected != -1) getViewComponent().guiLangCombo.setSelectedIndex(selected);
	 }
	
	@Override
	protected void initLanguages(LanguagesPreferences languages)
	 {
		ArrayList<String> langCodes = languages.getLangCodes();
		
		JPanel langPane = getViewComponent().langsPane;
		Component[] langComponents = langPane.getComponents();
		
		getViewComponent().getContentPane().setVisible(false);
		
		GridBagLayout langLayout = (GridBagLayout) langPane.getLayout();
		
		for (Component component : langComponents) {
			GridBagConstraints c = langLayout.getConstraints(component);
			if (c.gridy != 0) langPane.remove(component);
		}

		if (langCodes.size() == 0) {
		
			GridBagConstraints c = new GridBagConstraints();
			c.anchor = GridBagConstraints.WEST;
			c.insets = new Insets(5, 7, 5, 7);
			
			c.gridx = 0;
			c.gridy = 1;
			langPane.add(new JLabel("-"), c);

			c.gridx = 1;
			c.gridy = 1;
			langPane.add(new JLabel("-"), c);

		}
		
		for (int i = 0; i < langCodes.size(); i++) {
			
			final String langCode = langCodes.get(i);
			Display display = languages.getDisplay(langCode);
			
			// language
			String langName = MinorityTranslateModel.wikis().getLangName(langCode);
			if (langName == null) continue;
			JLabel langLabel = new JLabel(langName);
			
			// display
			String[] dispList = new String[3];
			dispList[0] = Messages.getString("preferences.program.languages.display.from");
			dispList[1] = Messages.getString("preferences.program.languages.display.to");
			dispList[2] = Messages.getString("preferences.program.languages.display.none");
			final JComboBox<String> dispCombo = new JComboBox<>(dispList);
			dispCombo.setSelectedIndex(display.ordinal());
			dispCombo.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					onDisplayChange(langCode, Display.values()[dispCombo.getSelectedIndex()]);
				}
			});
			
			// remove button
			JButton removeButton = new JButton(Messages.getString("preferences.program.languages.buttons.remove"));
			removeButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					onRemoveLanguage(langCode);
				}
			});

			GridBagConstraints c = new GridBagConstraints();
			c.anchor = GridBagConstraints.WEST;
			c.insets = new Insets(0, 0, 10, 14);
			
			c.gridx = 0;
			c.gridy = i + 1;
			langPane.add(langLabel, c);
			
			c.gridx = 1;
			c.gridy = i + 1;
			langPane.add(dispCombo, c);
			
			c.gridx = 2;
			c.gridy = i + 1;
			langPane.add(removeButton, c);
		}
		
		getViewComponent().pack();
		
		getViewComponent().getContentPane().setVisible(true);
	 }

	@Override
	protected void initCorpus(LanguagesPreferences profuages, CorpusPreferences corpus)
	 {
		ArrayList<String> langCodes = profuages.getLangCodes();
		
		JPanel profPane = getViewComponent().profPane;
		Component[] profComponents = profPane.getComponents();
		
		getViewComponent().getContentPane().setVisible(false);
		
		GridBagLayout profLayout = (GridBagLayout) profPane.getLayout();
		
		for (Component component : profComponents) {
			GridBagConstraints c = profLayout.getConstraints(component);
			if (c.gridy != 0) profPane.remove(component);
		}

		if (langCodes.size() == 0) {
		
			GridBagConstraints c = new GridBagConstraints();
			c.anchor = GridBagConstraints.WEST;
			c.insets = new Insets(5, 7, 5, 7);
			
			c.gridx = 0;
			c.gridy = 1;
			profPane.add(new JLabel("-"), c);

			c.gridx = 1;
			c.gridy = 1;
			profPane.add(new JLabel("-"), c);

		}
		
		for (int i = 0; i < langCodes.size(); i++) {
			
			final String langCode = langCodes.get(i);
			Proficiency proficiency = corpus.getProficiency(langCode);
			
			// language
			String langName = MinorityTranslateModel.wikis().getLangName(langCode);
			if (langName == null) continue;
			JLabel langLabel = new JLabel(langName);
			
			// proficiency
			String[] dispList = new String[6];
			dispList[0] = Messages.getString("preferences.program.corpus.proficiency.unspecified");
			dispList[1] = Messages.getString("preferences.program.corpus.proficiency.level1");
			dispList[2] = Messages.getString("preferences.program.corpus.proficiency.level2");
			dispList[3] = Messages.getString("preferences.program.corpus.proficiency.level3");
			dispList[4] = Messages.getString("preferences.program.corpus.proficiency.level4");
			dispList[5] = Messages.getString("preferences.program.corpus.proficiency.level5");
			final JComboBox<String> profCombo = new JComboBox<>(dispList);
			profCombo.setSelectedIndex(proficiency.ordinal());
			profCombo.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					onProficiencyChange(langCode, Proficiency.values()[profCombo.getSelectedIndex()]);
				}
			});
			
			GridBagConstraints c = new GridBagConstraints();
			c.anchor = GridBagConstraints.WEST;
			c.insets = new Insets(0, 0, 10, 14);
			
			c.gridx = 0;
			c.gridy = i + 1;
			profPane.add(langLabel, c);
			
			c.gridx = 1;
			c.gridy = i + 1;
			profPane.add(profCombo, c);
			
		}
		
		getViewComponent().collectCorpusCheckBox.setSelected(corpus.isCollect());
		
		if (corpus.isExact()) getViewComponent().exactRadiobutton.setSelected(true);
		else getViewComponent().adaptationRadiobutton.setSelected(true);
		
		getViewComponent().pack();
		
		getViewComponent().getContentPane().setVisible(true);
	 }

	@Override
	protected void initProcessing(ProcessingPreferences processing)
	 {
		feedback = true;
		
		visualiseCollects();
		
		visualiseReplaces();
		
		feedback = false;
	 }


	@Override
	protected void openContentAssist(ContentAssistPreferences preferences) {
		// TODO
		
	}
	
	@Override
	protected ContentAssistPreferences closeContentAssist() {
		// TODO
		return null;
	}
	
	
	@Override
	protected void openInserts(InsertsPreferences templates) {
		// TODO
	}
	
	@Override
	protected InsertsPreferences closeInserts() {
		// TODO
		return null;
	}
	
	
	@Override
	protected void openSymbols(SymbolsPreferences preferences) {
		// TODO
	}
	
	@Override
	protected SymbolsPreferences closeSymbols() {
		// TODO
		return null;
	}

	@Override
	protected void openLookups(LookupsPreferences preferences) {
		// TODO
	}
	
	@Override
	protected LookupsPreferences closeLookups() {
		// TODO
		return null;
	}
	
	
	// INHERIT (PROCESSING)
	@Override
	public void createCollect(int i)
	 {
		processing.createCollect(i);
		visualiseCollects();
		getViewComponent().collectTable.setRowSelectionInterval(i, i);
	 }
	
	@Override
	public void removeCollect(int i)
	 {
		processing.removeCollect(i);
		visualiseCollects();
		if (i >= getViewComponent().collectTable.getRowCount()) i = getViewComponent().collectTable.getRowCount() - 1;
		getViewComponent().collectTable.setRowSelectionInterval(i, i);
	 }

	@Override
	public void switchCollect(int i, int j)
	 {
		processing.switchCollect(i, j);
		visualiseCollects();
		getViewComponent().collectTable.setRowSelectionInterval(j, j);
	 }
	
	@Override
	public void createReplace(int i)
	 {
		processing.createReplace(i);
		visualiseReplaces();
		getViewComponent().replaceTable.setRowSelectionInterval(i, i);
	 }
	
	@Override
	public void removeReplace(int i)
	 {
		processing.removeReplace(i);
		visualiseReplaces();
		if (i >= getViewComponent().replaceTable.getRowCount()) i = getViewComponent().replaceTable.getRowCount() - 1;
		getViewComponent().replaceTable.setRowSelectionInterval(i, i);
	 }

	@Override
	public void switchReplace(int i, int j)
	 {
		processing.switchReplace(i, j);
		visualiseReplaces();
		getViewComponent().replaceTable.setRowSelectionInterval(j, j);
	 }
	

	// INHERIT HELPERS
	private void show(DefaultMutableTreeNode node)
	 {
		PreferencesPage page = null;
		if (node == programNode) page = PreferencesPage.PROGRAM;
		else if (node == languagesNode) page = PreferencesPage.LANGUAGES;
		else if (node == corpusNode) page = PreferencesPage.CORPUS;
		else if (node == collectNode) page = PreferencesPage.COLLECT;
		else if (node == processingNode) page = PreferencesPage.PROCESSING;
		else if (node == replaceNode) page = PreferencesPage.REPLACE;
		if (page == null) page = PreferencesPage.NONE;
		
		CardLayout barLayout = (CardLayout)(getViewComponent().pagesPane.getLayout());
		barLayout.show(getViewComponent().pagesPane, page.toString());
	 }
	
	private void visualiseCollects()
	 {
		ArrayList<CollectEntry> collects = processing.getCollects();
		DefaultTableModel collectModel = (DefaultTableModel) getViewComponent().collectTable.getModel();
		collectModel.setNumRows(collects.size());
		for (int r = 0; r < collects.size(); r++) {
			
			collectModel.setValueAt(processing.toStringCollect(r, 0), r, 0);
			collectModel.setValueAt(processing.toStringCollect(r, 1), r, 1);
			collectModel.setValueAt(processing.toStringCollect(r, 2), r, 2);
			
		}
	 }
	
	private void visualiseReplaces()
	 {
		ArrayList<ReplaceEntry> replaces = processing.getReplaces();
		DefaultTableModel replaceModel = (DefaultTableModel) getViewComponent().replaceTable.getModel();
		replaceModel.setNumRows(replaces.size());
		for (int r = 0; r < replaces.size(); r++) {
			
			replaceModel.setValueAt(processing.toStringReplace(r, 0), r, 0);
			replaceModel.setValueAt(processing.toStringReplace(r, 1), r, 1);
			replaceModel.setValueAt(processing.toStringReplace(r, 2), r, 2);
			
		}
	 }
	@Override
	public void focus() {
		// TODO Auto-generated method stub
		
	}
	
	// EVENTS (PROGRAM)
	public void onGUILanguageChange(int i) {
		if (program == null) return;
		if (i != -1) onGUILangCodeChange(this.guiLangCodes[i]);
	}
	
	
}
