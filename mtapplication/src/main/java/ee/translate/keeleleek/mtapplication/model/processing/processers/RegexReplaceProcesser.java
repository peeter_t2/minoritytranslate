package ee.translate.keeleleek.mtapplication.model.processing.processers;

import java.util.HashMap;
import java.util.List;
import java.util.regex.PatternSyntaxException;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle;
import ee.translate.keeleleek.mtapplication.model.processing.RegexCollect;
import ee.translate.keeleleek.mtapplication.model.processing.RegexReplace;

public class RegexReplaceProcesser {

	final public static String SRC_TITLE_KEY = "${Src_title}";
	final public static String DST_TITLE_KEY = "${Dst_title}";
	final public static String SRC_TITLE_KEY_LOWER_CASE = "${src_title}";
	final public static String DST_TITLE_KEY_LOWER_CASE = "${dst_title}";
	
	
	public boolean process(MinorityArticle srcArticle, MinorityArticle dstArticle, List<RegexCollect> regexCollects, List<RegexReplace> regexReplaces) throws PatternSyntaxException
	 {
		String text = srcArticle.getFilteredText();
		
		HashMap<String, String> paramMap = new HashMap<>();

		if (!dstArticle.getTitle().isEmpty()) {
			paramMap.put(SRC_TITLE_KEY, srcArticle.getTitle());
			paramMap.put(DST_TITLE_KEY, dstArticle.getTitle());
			paramMap.put(SRC_TITLE_KEY_LOWER_CASE, decapitalise(srcArticle.getTitle()));
			paramMap.put(DST_TITLE_KEY_LOWER_CASE, decapitalise(dstArticle.getTitle()));
		}
		
		for (RegexCollect regexCollect : regexCollects) {
			regexCollect.collect(text, paramMap);
		}
		
		for (RegexReplace regexReplace : regexReplaces) {
			text = regexReplace.replace(text, paramMap);
		}
		
//		System.out.println("PARAMETERS=" + paramMap);
//		System.out.println("TEXT=" + dstText);
		
		MinorityTranslateModel.content().changeText(dstArticle.getRef(), text);
		
		return true;
	 }

	public String process(String srcTitle, String srcText, String dstTitle, String dstText, List<RegexCollect> regexCollects, List<RegexReplace> regexReplaces) throws PatternSyntaxException
	 {
		HashMap<String, String> paramMap = new HashMap<>();

		if (!dstTitle.isEmpty()) {
			paramMap.put(SRC_TITLE_KEY, srcTitle);
			paramMap.put(DST_TITLE_KEY, dstTitle);
			paramMap.put(SRC_TITLE_KEY_LOWER_CASE, decapitalise(srcTitle));
			paramMap.put(DST_TITLE_KEY_LOWER_CASE, decapitalise(dstTitle));
		}
		
		for (RegexCollect regexCollect : regexCollects) {
			regexCollect.collect(srcText, paramMap);
		}
		
		for (RegexReplace regexReplace : regexReplaces) {
			srcText = regexReplace.replace(srcText, paramMap);
		}
		
		return srcText;
	 }

	
	
	public String capitalise(String str){
	    if(str.length() == 0) return str;
	    return str.substring(0, 1).toUpperCase() + str.substring(1);
	}
	
	public String decapitalise(String str){
	    if(str.length() == 0) return str;
	    return str.substring(0, 1).toLowerCase() + str.substring(1);
	}
	
}
