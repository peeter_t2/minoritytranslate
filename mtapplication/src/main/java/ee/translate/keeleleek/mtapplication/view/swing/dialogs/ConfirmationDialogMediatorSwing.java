package ee.translate.keeleleek.mtapplication.view.swing.dialogs;

import javax.swing.JDialog;

import ee.translate.keeleleek.mtapplication.view.dialogs.ConfirmationDialogMediator;
import ee.translate.keeleleek.mtapplication.view.swing.swingview.SwingConfirmationDialogViewComponent;


public class ConfirmationDialogMediatorSwing extends ConfirmationDialogMediator {

	final public static String NAME = "{D2386EEB-B1F7-4D2F-B18A-9AEAC249E128}";

	
	
	// INIT:
	public ConfirmationDialogMediatorSwing() {
		super();
	}
	
	
	// IMPLEMENTATION:
	@Override
	public SwingConfirmationDialogViewComponent getViewComponent() {
		return (SwingConfirmationDialogViewComponent) super.getViewComponent();
	}
	
	
	@Override
	public void close() {
		((JDialog)getViewComponent()).setVisible(false);
	}
	
	@Override
	public void setText(String message, String details, String yes, String no, String cancel) {
		getViewComponent().messageLabel.setText(message);
		getViewComponent().detailsLabel.setText(details);
		getViewComponent().yesButton.setText(yes);
		getViewComponent().noButton.setText(no);
		getViewComponent().cancelButton.setText(cancel);
	}

    
}
