package ee.translate.keeleleek.mtapplication.view.javafx.helpers;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import ee.translate.keeleleek.mtapplication.common.FileUtil;
import ee.translate.keeleleek.mtapplication.common.requests.FindRequest;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteChoice;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteChoices;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteRequest;
import ee.translate.keeleleek.mtapplication.view.javafx.application.ApplicationMediatorFX;
import ee.translate.keeleleek.mtapplication.view.javafx.elements.TranslateTabMediatorFX;
import ee.translate.keeleleek.mtpluginframework.spellcheck.Misspell;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.concurrent.Worker.State;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.web.WebView;
import netscape.javascript.JSException;
import netscape.javascript.JSObject;

public abstract class AceEditorLayerFX extends EditorLayerFX {
	
	public static final String ARTICLE_UTIL_SCRIPTS_PATH = "/js/articleutil.js";
	public static final String ACE_SCRIPTS_PATH = "/js/ace.js";
	public static final String CARET_COORDINATES_FUNCTION = "findCaretCoordinates()";
	public static final String CARET_PRECEEDING_FUNCTION = "findPreceding()";
	public static final String CARET_SELECTED_FUNCTION = "findSelected()";
	public static final String CARET_EXTEND_BACKWARD = "extendBackward(#)";
	public static final String CARET_MOVE_BACKWARD = "moveBackward(#)";
	public static final String CARET_MOVE_FORWARD = "moveForward(#)";
	public static final String POSITION_CARET = "positionCaret()";
	public static final String GET_VALUE = "editor.getValue()";
	public static final String SET_VALUE = "editor.setValue(\"#\")";
	public static final String PASTE = "editor.insert(\"#\", true)";
	public static final String MARK = "mark(#,#,#,#)";
	public static final String SET_READONLY = "editor.setOptions({ readOnly: true } )";
	public static final String SET_EDITABLE = "editor.setOptions({ readOnly: false } )";
	
	
	@FXML
	protected WebView textEdit;

	private final AceEditorLayerFX callback = this;

	boolean updateDocument = true;
	
	private boolean delay = true;
	private String textDelayed = null;
	private List<Misspell> misspellsDelayed= null;
	
	// INIT
	protected void initEditor()
	 {
		textEdit.addEventFilter(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent e) {
				if (AUTOCOMPLETE_KEYS.match(e)) requestAutocomplete();
				if (popup.isShowing() && e.getCode() != KeyCode.UP && e.getCode() != KeyCode.DOWN) requestAutocomplete();
			}
		});

		textEdit.addEventFilter(KeyEvent.KEY_TYPED, new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				int code = event.getCharacter().codePointAt(0);
				if (popup.isShowing() && code >= 10 && code <= 13) event.consume(); // block new lines
				if (code == 32 && (event.isControlDown() || event.isMetaDown())) event.consume(); // block autocomplete from entering space
			}
		});
		
		textEdit.getEngine().getLoadWorker().stateProperty().addListener(
				new ChangeListener<State>() {
					public void changed(ObservableValue<? extends State> ov, State oldState, State newState)
					 {
						if (newState != Worker.State.SUCCEEDED) return;
						
						JSObject jsobj = (JSObject) textEdit.getEngine().executeScript("window");
						jsobj.setMember("mediator", callback);
						
						delay = false;
						if (textDelayed != null) setText(textDelayed);
						if (misspellsDelayed != null) markMisspells(misspellsDelayed);
						
						textEdit.getEngine().setUserStyleSheetLocation(EDITOR_STYLESHEET_PATH);
					}
				});

		textEdit.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent e) {
				if (PASTE_KEYS.match(e)) {
					Clipboard clipboard = Clipboard.getSystemClipboard();
					String content = clipboard.getString();
					if (content != null) textEdit.getEngine().executeScript(PASTE.replace("#", StringEscapeUtils.escapeEcmaScript(content)));
				}
			}
		});
		

		textEdit.addEventHandler(KeyEvent.ANY, new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				if (event.isControlDown() && event.isAltDown() && event.isShortcutDown()) {
					event.consume(); // TODO Resolve hack for Windows altgr problem!	
				}
			}
		});
		
		// editor
		try {
			String editingTemplate = FileUtil.readFromJar("/editview.html");
			URL jsroot = TranslateTabMediatorFX.class.getResource("/js");
			editingTemplate = editingTemplate.replace("${jsroot}", jsroot.toString());
			
			feedback = true;
			
			String text = "";
			text = editingTemplate.replace("${text}", text);
			textEdit.getEngine().loadContent(text);
			feedback = false;
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// text
		textEdit.setContextMenuEnabled(false);
	}
	
	
	// MISSPELLS
	@Override
	public void markMisspells(List<Misspell> misspells)
	 {
		if (delay) {
			misspellsDelayed = misspells;
			return;
		} else {
			misspellsDelayed = null;
		}
		
		this.misspells = misspells;
		
		textEdit.getEngine().executeScript("clearMisspells()");
		for (Misspell misspell : misspells) {
			//System.out.println(misspell.getMessage());
			textEdit.getEngine().executeScript("markMisspell(" + misspell.getStartRow() + "," + misspell.getStartColumn() + "," + misspell.getEndRow() + "," + misspell.getEndColumn() + ")");
		}
	 }
	

	// EDITOR
	@Override
	public Node getParent() {
		return textEdit;
	}
	
	@Override
	public Point2D findCaretPosition() {
		String coordinates = textEdit.getEngine().executeScript(CARET_COORDINATES_FUNCTION).toString();
		String[] strCoords = coordinates.split(",");
		if (strCoords.length < 2) return null;
		Point2D pos = textEdit.localToScreen(Integer.parseInt(strCoords[0]), Integer.parseInt(strCoords[1]));
		pos = pos.add(0, 24);
		return pos;
	}
	
	@Override
	public void setTextEditEditable(boolean editable)
	 {
		if (textEdit.getEngine().getDocument() != null) {
			if (editable) {
				textEdit.getEngine().executeScript(SET_EDITABLE);
				textEdit.getEngine().executeScript("editor.renderer.$cursorLayer.element.style.opacity=0.75");
			}
			else {
				textEdit.getEngine().executeScript(SET_READONLY);
				textEdit.getEngine().executeScript("editor.renderer.$cursorLayer.element.style.opacity=0.1");
			}
		}
	 }
	
	
	// CONTENT
	@Override
	public String getText()
	 {
		return textEdit.getEngine().executeScript(GET_VALUE).toString();
	 }

	@Override
	public void setText(String text)
	 {
		feedback = true;
		
		if (delay) { // wait for the editor to finish loading
			textDelayed = text;
			return;
		} else {
			textDelayed = null;
		}
		
		if (textEdit.getEngine().getDocument() == null) return;
		
		if (updateDocument) {
			addStylesheet(textEdit.getEngine().getDocument(), getClass().getResource(ApplicationMediatorFX.findFontSizeCSS(MinorityTranslateModel.preferences().getFontSize())).toExternalForm());
			updateDocument = false;
		}
		
		text = StringEscapeUtils.escapeEcmaScript(text);
		
		textEdit.getEngine().executeScript("editor.session.setValue(\"" + text + "\", -1)");
		
		feedback = false;
	 }
	
	@Override
	public void replaceText(String text)
	 {
		if (textEdit.getEngine().getDocument() == null) return;
		
		if (updateDocument) {
			addStylesheet(textEdit.getEngine().getDocument(), getClass().getResource(ApplicationMediatorFX.findFontSizeCSS(MinorityTranslateModel.preferences().getFontSize())).toExternalForm());
			updateDocument = false;
		}
		
		text = StringEscapeUtils.escapeEcmaScript(text);
		textEdit.getEngine().executeScript("editor.setValue(\"" + text + "\", -1)");
	 }

	@Override
	public void pasteText(String text)
	 {
		if (!textEdit.isFocused()) return;

		text = StringEscapeUtils.escapeEcmaScript(text);
		textEdit.getEngine().executeScript("editor.insert(\"" + text + "\", -1)");
	 }

   
	// AUTOCOMPLETE
	private void requestAutocomplete()
	 {
		String preceeding;
		String selected;
		String coordinates;
		
		try {
			
			preceeding = textEdit.getEngine().executeScript(CARET_PRECEEDING_FUNCTION).toString();
			selected = textEdit.getEngine().executeScript(CARET_SELECTED_FUNCTION).toString();
			coordinates = textEdit.getEngine().executeScript(CARET_COORDINATES_FUNCTION).toString();
			
		} catch (JSException e) {
			e.printStackTrace();
			return;
		}
		
		String[] strCoords = coordinates.split(",");
		if (strCoords.length < 2) return;
		
		Point2D pos = textEdit.localToScreen(Integer.parseInt(strCoords[0]), Integer.parseInt(strCoords[1]));
		pos = pos.add(0, 24);
		
		onAutocomplete(new AutocompleteRequest(getReference(), preceeding, selected, pos.getX(), pos.getY()));
	}
   

	@Override
	public void showAutocomplete(AutocompleteChoices autocomplete)
	 {
		// create list
		popupContent.getItems().clear();
		popupContent.setPrefHeight(200);
		List<AutocompleteChoice> choises = autocomplete.getChoises();
		
		// insert automatically
		if (MinorityTranslateModel.preferences().isInsertAuto() && choises.size() == 1 && !popup.isShowing()) {
			autocomplete(choises.get(0));
			return;
		}
		
		for (AutocompleteChoice choise : choises) {
			popupContent.getItems().add(new AutocompleteBox(choise));
		}
		
		//hide
		if (popupContent.getItems().size() == 0) {
			popup.hide();
			return;
		}

		// show
		if (!popup.isShowing()) {
			popup.setX(autocomplete.getX());
			popup.setY(autocomplete.getY());
			popup.show(textEdit.getScene().getWindow());
			popup.requestFocus();
		}
	 }
   
	@Override
	public void autocomplete(AutocompleteChoice choice)
	 {
		if (isReadOnly()) return;
		int textOffset = choice.getTextBackset();
		if (textOffset != 0) textEdit.getEngine().executeScript(CARET_EXTEND_BACKWARD.replace("#", textOffset + ""));
		pasteText(choice.getText());
		int caretOffset = choice.getCaretBackset();
		if (caretOffset != 0) textEdit.getEngine().executeScript(CARET_MOVE_BACKWARD.replace("#", caretOffset + ""));
	 }
   
	
	// FIND
	@Override
	public void find(FindRequest request)
	 {
		String find = "'" + StringEscapeUtils.escapeEcmaScript(request.getFind()) + "'";
		String replace = "'" + StringEscapeUtils.escapeEcmaScript(request.getReplace()) + "'";
		String options;
		
		switch (request.getType()) {
		case FIND_NEXT:
			options = "" + false + "," + request.isWrap() + "," + request.isCaseSensitive() + "," + request.isWholeWord();
			textEdit.getEngine().executeScript("find("+ find + "," + options + ")");
			break;
			
		case FIND_PREVIOUS:
			options = "" + true + "," + request.isWrap() + "," + request.isCaseSensitive() + "," + request.isWholeWord();
			textEdit.getEngine().executeScript("find("+ find + "," + options + ")");
			break;

		case REPLACE:
			textEdit.getEngine().executeScript("replaceFind("+ replace + ")");
			options = "" + false + "," + request.isWrap() + "," + request.isCaseSensitive() + "," + request.isWholeWord();
			textEdit.getEngine().executeScript("find("+ find + "," + options + ")");
			break;

		case REPLACE_ALL:
			options = "" + false + "," + request.isWrap() + "," + request.isCaseSensitive() + "," + request.isWholeWord();
			textEdit.getEngine().executeScript("replaceAll("+ find + "," + replace + "," + options + ")");
			break;

		default:
			break;
		}
	 }
	
	// ACE CALLBACK
	public void onAceEdit()
	 {
		if (feedback) return;
		onTextEdited();
	 }

	public void onAceScroll()
	 {
		closeMisspellTooltip();
	 }
  
	public void onAceCaretMove(String row, String column)
	 {
		closeMisspellTooltip();
  	
		int line = Integer.parseInt(row);
		int col = Integer.parseInt(column);
  	
		Misspell misspell = findMisspell(line, col);
		if (misspell != null) showMisspellTooltip(misspell);
	 }

	public void onAceCopy(String text)
	 {
		if (text.isEmpty()) return;
		Clipboard clipboard = Clipboard.getSystemClipboard();
		ClipboardContent content = new ClipboardContent();
		content.putString(text);
		clipboard.setContent(content);
	 }

	public void onAcePaste(Object object)
	 {
		System.out.println("PASTE: " + object);
	 }
	
	
	// HELPERS
	// http://stackoverflow.com/questions/27604232/javafx-webview-add-stylesheet-parsing-calc-error
	protected void addStylesheet(Document doc, String cssLocation)
	 {
	    Element docElement = doc.getDocumentElement();
	    NodeList heads = docElement.getElementsByTagName("head");
	    Element head ;
	    if (heads.getLength() == 0) {
	        head = doc.createElement("head");
	        docElement.appendChild(head);
	    } else {
	        head = (Element) heads.item(0);
	    }
	    Element link = doc.createElement("link");
	    link.setAttribute("rel", "stylesheet");
	    link.setAttribute("type", "text/css");
	    link.setAttribute("href", cssLocation);
	    head.appendChild(link);     
	 }
   
	
}
