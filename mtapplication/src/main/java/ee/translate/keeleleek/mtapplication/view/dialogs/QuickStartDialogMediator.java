package ee.translate.keeleleek.mtapplication.view.dialogs;

import java.util.ArrayList;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;


public abstract class QuickStartDialogMediator extends Mediator {

	public final static String NAME = "{9F044E66-729E-4926-BB0B-F65BDA3DC824}";
	
	public final static String PAGES_PATH = "/guide";
	
	public final static String INTERFACE_LANGUAGE_PAGE = "interface language v1";
	public final static String LANGUAGES_PAGE = "languages v1";
	public final static String ADDING_ARTICLES_PAGE = "adding articles v1";
	public final static String AUTOCOMPLETE_PAGE = "features v1";
	public final static String TRANSLATING_ARTICLES_PAGE = "extra features v1";
	public final static String SESSIONS_PAGE = "sessions v1";
	public final static String UPLOADING_ARTICLES_PAGE = "uploading v1";
	public final static String TEXT_CORPUS_PAGE = "text corpus v1";
	public final static String EMPTY_PAGE = "empty";
	public final static String[] ALL_PAGES = new String[]{INTERFACE_LANGUAGE_PAGE, LANGUAGES_PAGE, ADDING_ARTICLES_PAGE, AUTOCOMPLETE_PAGE, TRANSLATING_ARTICLES_PAGE, SESSIONS_PAGE, UPLOADING_ARTICLES_PAGE, TEXT_CORPUS_PAGE};	
	
	private String pageName = null;
	private ArrayList<String> pageNames = null;
	
	
	// INIT
	public QuickStartDialogMediator() {
		super(NAME, null);
	}

	@Override
	public void onRegister() {
		
	}
	
	
	// INHERIT (PAGES)
	protected abstract void show(String pageName);
	protected abstract void apply(String pageName);

	
	// PAGES
	public void open(ArrayList<String> pageNames)
	 {
		this.pageNames = pageNames;
		if (pageNames.size() == 0) pageName = EMPTY_PAGE;
		else pageName = pageNames.get(0);
		show(pageName);
	 }
	
	public void close()
	 {
		
	 }
	
	public boolean isLast()
	 {
		if (pageNames.size() == 0) return true;
		return pageNames.get(pageNames.size() - 1).endsWith(pageName);
	 }
	
	public boolean hasPages() {
		return pageNames.size() > 0;
	}
	
	
	// EVENTS
	public String onClosePage()
	 {
		int i = pageNames.indexOf(pageName);
		if (i == -1) return null;
		
		apply(pageName);
		
		MinorityTranslateModel.notifier().sendNotification(Notifications.PREFERENCES_ACKNOWLEDGE, pageName);
		
		String closedName = pageNames.remove(i);
		
		if (pageNames.size() == 0) {
			pageName = EMPTY_PAGE;
			show(pageName);
			return closedName;
		}
		
		if (i < 0) i = 0;
		if (i >= pageNames.size()) i = pageNames.size() - 1;

		pageName = pageNames.get(i);
		show(pageName);
		
		return closedName;
	 }

	
}
