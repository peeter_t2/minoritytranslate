package ee.translate.keeleleek.mtapplication.view.swing.dialogs;

import ee.translate.keeleleek.mtapplication.view.dialogs.LoginDialogMediator;
import ee.translate.keeleleek.mtapplication.view.swing.swingview.SwingLoginWindowViewComponent;


public class LoginDialogMediatorSwing extends LoginDialogMediator {


	// IMPLEMENTATION:
	@Override
	public SwingLoginWindowViewComponent getViewComponent() {
		return (SwingLoginWindowViewComponent) super.getViewComponent();
	}
	
	
	@Override
	protected String getUsername() {
		return getViewComponent().usernameTextField.getText();
	}

	@Override
	protected String getPassword() {
		return new String(getViewComponent().passwordTextField.getPassword());
	}

	@Override
	protected boolean isAutoLogin() {
		return getViewComponent().stayLoggedCheckBox.isSelected();
	}
	
	@Override
	protected void setUsername(String text) {
		getViewComponent().usernameTextField.setText(text);
	}

	@Override
	protected void setPassword(String text) {
		getViewComponent().passwordTextField.setText(text);
	}

	
	
}
