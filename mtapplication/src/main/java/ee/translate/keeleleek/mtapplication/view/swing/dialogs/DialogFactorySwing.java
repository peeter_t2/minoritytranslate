package ee.translate.keeleleek.mtapplication.view.swing.dialogs;

import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import ee.translate.keeleleek.mtapplication.common.requests.ContentRequest;
import ee.translate.keeleleek.mtapplication.view.dialogs.DialogFactory;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtpluginframework.pull.PullCallback;

public class DialogFactorySwing extends DialogFactory {
	
	protected JOptionPane getOptionPane(JComponent parent)
     {
       JOptionPane pane = null;
       if (!(parent instanceof JOptionPane)) {
           pane = getOptionPane((JComponent)parent.getParent());
       } else {
           pane = (JOptionPane) parent;
       }
       return pane;
     }
   @Override
public void showError(String header, String content) {
	// TODO Auto-generated method stub
}
   @Override
public PullCallback showPullMappingsDialog(String title, String header, String ok, String cancel) {
	// TODO Auto-generated method stub
	return null;
}
   @Override
public void showWarning(String header, String content) {
	// TODO Auto-generated method stub
	
}
 @Override
public PullCallback showPullProgressDialog(String title, String header, String ok, String cancel) {
	// TODO Auto-generated method stub
	return null;
}  
   @Override
public void showInfo(String header, String content) {
	// TODO Auto-generated method stub
	
}
	@Override
	public String askString(String title, String header, String content, String initial, boolean allowEmpty)
	 {
		// create
		final JTextField textField = new JTextField(initial);
		final JButton okButton = new JButton(Messages.getString("button.ok"));
		final JButton cancelButton = new JButton(Messages.getString("button.cancel"));
		
		GridLayout layout = new GridLayout(2, 1);
		JPanel panel = new JPanel();
		panel.setLayout(layout);
		panel.add(new JLabel(header));
		panel.add(textField);

		// disable
		if (!allowEmpty || true) {
			okButton.setEnabled(!textField.getText().trim().isEmpty());
			textField.getDocument().addDocumentListener(new DocumentListener() {
				@Override
				public void removeUpdate(DocumentEvent e) {
					okButton.setEnabled(!textField.getText().trim().isEmpty());
				}
				
				@Override
				public void insertUpdate(DocumentEvent e) {
					okButton.setEnabled(!textField.getText().trim().isEmpty());
				}
				
				@Override
				public void changedUpdate(DocumentEvent e) {
					okButton.setEnabled(!textField.getText().trim().isEmpty());
				}
			});
		}
		
		// buttons
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Window window = SwingUtilities.getWindowAncestor(cancelButton);
		        if(window != null) window.setVisible(false);
		        JOptionPane pane = getOptionPane((JComponent) e.getSource());
		        pane.setValue(okButton);
			}
		});

		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Window window = SwingUtilities.getWindowAncestor(cancelButton);
		        if(window != null) window.setVisible(false);
			}
		});
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				textField.requestFocusInWindow();
			}
		});
		
		// show
		int option = JOptionPane.showOptionDialog(
			null, 
			panel,
			title,
			JOptionPane.YES_NO_OPTION, 
			JOptionPane.PLAIN_MESSAGE, 
			null, 
			new Object[]{okButton, cancelButton}, 
			null
		);
		
		if (option == 0) return textField.getText().trim();
		else return null;
	 }
	
	protected boolean askConfirmDecline(String title, String header, String content, String confirmButton, String cancelButton) {
		return false;
	}
	
	@Override
	protected YesNoCancel askConfirmYesNoCancel(String title, String header, String content, String yes, String no, String cancel) {
		return YesNoCancel.YES;
	}
	
	@Override
	public void askQuickStart(ArrayList<String> pageNames) {
		
	}
	
	@Override
	public String askUnicodeCharacter() {
		return null;
	}

	@Override
	public Path askSymbolsSavePath() {
		return null;
	}
	
	@Override
	public Path askSymbolsOpenPath() {
		return null;
	}
	
	@Override
	public List<ContentRequest> askAddContent() {
		return null;
	}
}
