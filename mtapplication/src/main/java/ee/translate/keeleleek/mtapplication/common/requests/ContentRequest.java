package ee.translate.keeleleek.mtapplication.common.requests;

public class ContentRequest {

	private String langCode;
	private Integer namespace;
	private String title;
	
	
	public ContentRequest() {
		langCode = "";
		namespace = -1;
		title = "";
	}
	
	public ContentRequest(ContentRequest other) {
		this.langCode = other.langCode;
		this.namespace = other.namespace;
		this.title = other.title;
	}
	
	public ContentRequest(String langCode, Integer namespace, String title) {
		this.langCode = langCode;
		this.namespace = namespace;
		this.title = title;
	}

	
	public String getLangCode() {
		return langCode;
	}

	public Integer getNamespace() {
		return namespace;
	}

	public String getTitle() {
		return title;
	}
	
	public boolean isCategory() {
		return namespace == 14;
	}
	
	
}
