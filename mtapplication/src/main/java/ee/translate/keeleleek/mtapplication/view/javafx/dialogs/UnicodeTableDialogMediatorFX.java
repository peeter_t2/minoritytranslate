package ee.translate.keeleleek.mtapplication.view.javafx.dialogs;

import java.lang.Character.UnicodeBlock;
import java.util.ArrayList;
import java.util.HashMap;

import ee.translate.keeleleek.mtapplication.view.dialogs.UnicodeTableDialogMediator;
import ee.translate.keeleleek.mtapplication.view.javafx.fxemelents.ExposableConfirmFX;
import ee.translate.keeleleek.mtapplication.view.javafx.helpers.UnicodePage;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;


public class UnicodeTableDialogMediatorFX extends UnicodeTableDialogMediator implements ExposableConfirmFX {

	@FXML
	private TextField symbolsEdit;
	
	@FXML
	private Pagination pages;
	
	@FXML
	private ComboBox<String> groupSelect;
	
	
	private HashMap<Integer, UnicodePage> pageMap = new HashMap<Integer, UnicodePage>();
	
	
	// INIT
	@Override
	public void onRegister()
	 {
		
	 }
	
	@FXML
	private void initialize()
	 {
		groupSelect.getSelectionModel().selectedIndexProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable arg0) {
				pages.setCurrentPageIndex(groupSelect.getSelectionModel().getSelectedIndex());
			}
		});
		
		int rowCount = Character.MAX_VALUE / 0x0010;
		
		// create pages
		String groupName = findBlockName(0);
		ArrayList<String> groupNames = new ArrayList<String>();
		
		UnicodePage unicodePage = new UnicodePage(findBlockName(0), (char)0);
		pageMap.put(pageMap.size(), unicodePage);
		groupNames.add(groupName);
		
		for (int i = 0; i < rowCount; i++) {
			
			int ch = i * 0x0010;
			
			unicodePage.setEnd((char) (ch - 0x0001 + 0x0010));
			
			groupName = findBlockName(ch);
			
			if (groupName.isEmpty()) groupName = "Undefined";
			if (groupName.equals(unicodePage.getName())) continue;
			
			// mew
			unicodePage = new UnicodePage(groupName, (char) ch);
			pageMap.put(pageMap.size(), unicodePage);
			groupNames.add(groupName);
			
		}
		
		ObservableList<String> obsGroupNames = FXCollections.observableList(groupNames);
		groupSelect.setItems(obsGroupNames);
		groupSelect.getSelectionModel().select(0);
		
		pages.setPageCount(groupSelect.getItems().size());
		
		// page factory
		pages.setPageFactory(new Callback<Integer, Node>() {
			@Override
			public Node call(Integer i)
			 {
				// select
				groupSelect.getSelectionModel().select(i);
				
				// create table
				TableView<ArrayList<Character>> symbolTable = new TableView<>();
				symbolTable.setId("unicode-table");
				symbolTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
				symbolTable.getSelectionModel().setCellSelectionEnabled(true);
				
				// create columns
				for (int cl = 0; cl < 16; cl++) {
					final int c = cl;
					TableColumn<ArrayList<Character>, String> column = new TableColumn<ArrayList<Character>, String>();
					
					// value factory
					column.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ArrayList<Character>,String>, ObservableValue<String>>() {
						@Override
						public ObservableValue<String> call(CellDataFeatures<ArrayList<Character>, String> d) {
							return new SimpleStringProperty(d.getValue().get(c).toString());
						}
					});
					
					// cell factory
					column.setCellFactory(new Callback<TableColumn<ArrayList<Character>, String>, TableCell<ArrayList<Character>, String>>() {
						@Override
						public TableCell<ArrayList<Character>, String> call(TableColumn<ArrayList<Character>, String> col) {
							final TableCell<ArrayList<Character>, String> cell = new TableCell<ArrayList<Character>, String>() {
								@Override
								public void updateItem(String value, boolean empty) {
									super.updateItem(value, empty);
									if (empty) {
										setText(null);
										setTooltip(null);
									} else {
										setText(value);
										if (!value.isEmpty()) {
											char ch = value.charAt(0);
											
											int type = Character.getType(ch);
											switch (type) {
											case Character.CONTROL:
												return;
											default:
												String name = Character.getName(ch);
												String hex = String.format("%04x", (int)ch);
												if (name != null) setTooltip(new Tooltip("U+" + hex + " (" + name.toLowerCase().replace('_', ' ') + ")"));
												break;
											}
										}
										
									}
								}
							 };
							 cell.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
								 @Override
								 public void handle(MouseEvent event) {
									if (event.getClickCount() > 1) {
										
										if (cell.getText().length() == 0 || cell.getText().charAt(0) == ' ') return;
										
										char ch = new Character(cell.getText().charAt(0));
										
										int type = Character.getType(ch);
										switch (type) {
										case Character.CONTROL:
											return;
										default:
											String text = symbolsEdit.getText();
											text+= ch;
											symbolsEdit.setText(text);
											break;
										}
									}
								}
							});
							
							return cell;
						}
					});
					
					
					symbolTable.getColumns().add(column);
				}
				
				// characters
				UnicodePage unicodePage = pageMap.get(i);
				
				int ch = unicodePage.getStart();
				int ich;
				
				while (true) {
					
					if (ch >= unicodePage.getEnd()) break;
					
					ich = ch;
					ArrayList<Character> row = new ArrayList<>();
					
					for (int c = 0; c < 16; c++) {
						
						row.add(new Character((char)ich));
						ich+= 0x0001;
						
					}
					
					boolean add = true;
					
					if (ch < '\u0091') { // skip control rows
						if (ch == '\u0000') add = false; // C0
						if (ch == '\u0010') add = false; // C0
						if (ch == '\u0080') add = false; // C1
						if (ch == '\u0090') add = false; // C1
					}
					
					if (add) symbolTable.getItems().add(row);
					
					ch+= 0x0010;
				}
				
				return symbolTable;
			 }
			
		});
	 }

	
	// IMPLEMENT (CONFIRM EXPOSE)
	@Override
	public void exposeConfirm(final Button button) {
		button.setDisable(true);
		symbolsEdit.textProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				button.setDisable(symbolsEdit.getText().isEmpty());
			}
		});
	}
	

	// IMPLEMENT (SYMBOLS)
	public String getSymbols()
	 {
		return symbolsEdit.getText();
	 }
	
	
	// UTILITY
	private String findBlockName(int codePoint)
	 {
		UnicodeBlock block = Character.UnicodeBlock.of(codePoint);
		if (block == null) return "";
		String name = block.toString();
		name = name.toLowerCase().replace('_', ' ');
		if (!name.isEmpty()) name = name.substring(0, 1).toUpperCase() + name.substring(1);
		return name;
	 }
	
	
}
