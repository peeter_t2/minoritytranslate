package ee.translate.keeleleek.mtapplication.view.javafx.elements;

import java.util.List;

import org.w3c.dom.Document;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.Status;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.TitleStatus;
import ee.translate.keeleleek.mtapplication.model.media.IconsProxyFX;
import ee.translate.keeleleek.mtapplication.model.preferences.Symbol;
import ee.translate.keeleleek.mtapplication.view.elements.TranslateTabMediator;
import ee.translate.keeleleek.mtapplication.view.javafx.application.ApplicationMediatorFX;
import ee.translate.keeleleek.mtapplication.view.javafx.helpers.AceEditorLayerFX;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.concurrent.Worker.State;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;


public class TranslateTabMediatorFX extends AceEditorLayerFX {

	public static final String PREVIEW_STYLESHEET_PATH = TranslateTabMediator.class.getResource("/css/wiki.css").toExternalForm();
	
	
	@FXML
	private Pane translateBox;
	
	@FXML
	private TextField titleEdit;
	
	@FXML
	private Label statusLabel;
	@FXML
	private WebView previewView;
	
	@FXML
	private Button resetButton;
	@FXML
	private MenuButton spellerSelect;
	@FXML
	private ToggleButton previewButton;
	
	@FXML
	private VBox editBar;
	@FXML
	private HBox filterBar;
	
	@FXML
	private HBox row0Box;
	@FXML
	private HBox row1Box;
	@FXML
	private HBox row2Box;
	@FXML
	private CheckBox exactCheckbox;

	@FXML
	private CheckBox filterTemplatesCheckbox;
	@FXML
	private CheckBox filterFilesCheckbox;
	@FXML
	private CheckBox filterIntroductionCheckbox;
	@FXML
	private CheckBox filterReferencesCheckbox;
	
	@FXML
	private ProgressIndicator titleCheckIndicator;
	
	boolean feedback = false;
	boolean titleEditable = false;
	boolean textEditable = false;

	
	// INIT
	@Override
	public Tab getTabObject() {
		return (Tab) this.tabObject;
	}
	
	@Override
	public void initialise()
	 {
		initEditor();
		initPopup();
		
		titleEdit.textProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable)
			 {
				if (feedback) return;
				onTitleEdited();
			 }
		});

		previewView.getEngine().getLoadWorker().stateProperty().addListener(
			new ChangeListener<State>() {
				@Override
				public void changed(ObservableValue<? extends State> ov, State oldState, State newState)
				 {
					if (newState != State.SUCCEEDED) return;
					
					Document doc = previewView.getEngine().getDocument();
					
					addStylesheet(doc, getClass().getResource(ApplicationMediatorFX.findFontSizeCSS(MinorityTranslateModel.preferences().getFontSize())).toExternalForm());
					previewView.getEngine().setUserStyleSheetLocation(PREVIEW_STYLESHEET_PATH);
				 }
				
			});
		
		switch (mode()) {
		case NORMAL:
			editBar.setVisible(true);
			filterBar.setVisible(false);
			spellerSelect.setVisible(true);
			break;

		case FILTERED:
			editBar.setVisible(false);
			filterBar.setVisible(true);
			spellerSelect.setVisible(false);
			break;

		default:
			break;
		}
		
		List<String> spellerPluginNames = MinorityTranslateModel.speller().getPluginNames(MinorityTranslateModel.preferences().getGUILangCode());
		
		spellerSelect.getItems().clear();

		boolean first = true;
		ToggleGroup toggleGroup = new ToggleGroup();
		for (String pluginName : spellerPluginNames) {
			RadioMenuItem item = new RadioMenuItem(pluginName);
			if (first) {
				item.setSelected(true);
				first = false;
			}
			item.setToggleGroup(toggleGroup);
			
			item.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					onSpellCheckRequest();
				}
			});
			
			spellerSelect.getItems().add(item);
		}
	 }
	
	
	// INHERIT (CONTENT)
	@Override
	public String getTitle() {
		return titleEdit.getText();
	}
	
	@Override
	public void setName(String name) {
		getTabObject().setText(name);
	}
	
	@Override
	public void setTitle(String title) {
		feedback = true;
		titleEdit.setText(title);
		feedback = false;
	}
	
	@Override
	public void setStatus(Status status)
	 {
		if (status != null) {
			
			String text = Messages.getString("statusbar." + status.toString().toLowerCase().replace('_', '.'));
			statusLabel.setTooltip(new Tooltip(text));
			
			IconsProxyFX icons = (IconsProxyFX) MinorityTranslateModel.icons();
			Image icon = icons.getStatusIcon(status);
			ImageView image = icon != null ? new ImageView(icon) : null;
			statusLabel.setGraphic(image);
			
			statusLabel.setVisible(true);
			
		} else {
			statusLabel.setText("");
			statusLabel.setVisible(false);
		}
		
		boolean disable = status == null;
		
		titleEditable = isNnew() && (status == Status.STANDBY) && mode() == TabMode.NORMAL;
		textEditable = (status == Status.STANDBY) && mode() == TabMode.NORMAL;
		
		titleEdit.setEditable(titleEditable);
		setTextEditEditable(textEditable);
		
		exactCheckbox.setDisable(!textEditable || disable);
	 }
	

	@Override
	public void setTitleStatus(TitleStatus status)
	 {
		if (status == null) status = TitleStatus.UNKNOWN;
		
		
		switch (status) {
		case CONFLICT:
			titleEdit.getStyleClass().add("problem");
			break;

		default:
			titleEdit.getStyleClass().removeAll("problem");
			break;
		}
		
		if (status == TitleStatus.CHECKING || status == TitleStatus.CHECKING_REQUIRED) {
			
			titleCheckIndicator.setVisible(true);
			titleCheckIndicator.setManaged(true);
			
		} else {
			
			titleCheckIndicator.setVisible(false);
			titleCheckIndicator.setManaged(false);
			
		}
	 }
	
	@Override
	public void setPreview(String html) {
		previewView.getEngine().loadContent(html);
	}
	
	@Override
	public void setView(TabView view)
	 {
		switch (view) {
		case EDIT:
			textEdit.setVisible(true);
			previewView.setVisible(false);
			textEdit.requestFocus();
			previewButton.setSelected(false);
			break;
			
		case PREVIEW:
			textEdit.setVisible(false);
			previewView.setVisible(true);
			previewView.requestFocus();
			previewButton.setSelected(true);
			break;

		default:
			break;
		}
	 }
	

	@Override
	public void setExact(boolean exact) {
		exactCheckbox.setSelected(exact);
	}

	@Override
	protected void setSymbols(List<Symbol> symbols)
	 {
		row0Box.getChildren().clear();
		row1Box.getChildren().clear();
		row2Box.getChildren().clear();
		
		for (Symbol symbol : symbols) {
			Hyperlink link = new Hyperlink(symbol.getSymbol().toString());
			link.setFocusTraversable(false);
			link.setVisited(true);
			link.setMinHeight(10);
			link.setPadding(new Insets(0, 2.5, 0, 2.5));
			String name = symbol.getName();
			if (!symbol.getTrigger().isEmpty()) name+= " (" + symbol.getTrigger() + " & Ctrl+Space)";
			link.setTooltip(new Tooltip(name));
			link.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					pasteText(((Hyperlink) event.getSource()).getText());
				}
			});
			
			switch (symbol.getRow()) {
			case 0:
				row0Box.getChildren().add(link);
				break;

			case 1:
				row1Box.getChildren().add(link);
				break;

			case 2:
				row2Box.getChildren().add(link);
				break;

			default:
				break;
			}
			
		}
	 }
	
	
	@Override
	protected void setFilterTemplates(boolean filter) {
		filterTemplatesCheckbox.setSelected(filter);
	}
	
	@Override
	protected void setFilterFiles(boolean filter) {
		filterFilesCheckbox.setSelected(filter);
	}
	
	@Override
	protected void setFilterIntroduction(boolean filter) {
		filterIntroductionCheckbox.setSelected(filter);
	}
	
	@Override
	protected void setFilterReferences(boolean filter) {
		filterReferencesCheckbox.setSelected(filter);
	}
	
	
	@Override
	public boolean isEditFocused()
	 {
		return textEdit.isFocused() || previewView.isFocused();
	 }
	

	// INHERIT (SPELLCHECK)
	@Override
	public String getSpellerPluginName() {
		ObservableList<MenuItem> menu = spellerSelect.getItems();
		for (MenuItem menuItem : menu) {
			if (menuItem instanceof RadioMenuItem) {
				if (((RadioMenuItem) menuItem).isSelected()) return menuItem.getText();
			}
		}
		return null;
	}
	
	
	// EVENTS
	@FXML
	protected void onResetClick() {
		onReset();
	}
	
	@FXML
	protected void onPreviewToggle() {
		if (previewButton.isSelected()) super.onChangeView(TabView.PREVIEW);
		else super.onChangeView(TabView.EDIT);
	}

	@FXML
	protected void onExactCheck() {
		super.onChangeExact(exactCheckbox.isSelected());
	}

	@FXML
	protected void onFilterTemplatesCheck() {
		super.onChangeTemplatesFilter(filterTemplatesCheckbox.isSelected());
	}

	@FXML
	protected void onFilterFilesCheck() {
		super.onChangeFilesFilter(filterFilesCheckbox.isSelected());
	}

	@FXML
	protected void onFilterIntroductionCheck() {
		super.onChangeIntroductionFilter(filterIntroductionCheckbox.isSelected());
	}

	@FXML
	protected void onFilterReferencesCheck() {
		super.onChangeReferencesFilter(filterReferencesCheckbox.isSelected());
	}
	
	
}
