package ee.translate.keeleleek.mtapplication.model.content;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.wikis.WikisProxy;


public class MinorityArticle {

	public enum WikiType { UNKNOWN, REGULAR, INCUBATOR }
	public enum Status { UNKNOWN, DOWNLOAD_REQUESTED, DOWNLOAD_IN_PROGRESS, STANDBY, UPLOAD_REQUESTED, UPLOAD_PREPARING, UPLOAD_IN_PROGRESS, UPLOAD_CONFIRMING, LINKING_ARTICLES, SENDING_USAGE_INFO, UPLOAD_CONFLICT, UPLOAD_FAILED }
	public enum TitleStatus { UNKNOWN, OK, CHECKING_REQUIRED, CHECKING, CONFLICT }
	public enum Articon { UNKNOWN, EXISTS, NEW, EDITED, BUSY, PROBLEM }
	
	private static int EMPTY_HASH = new String("").hashCode();
	
	private WikiReference request;
	private Reference ref;
	
	private String prevrev;
	private String title;
	private String text;
	private Boolean exact;
	private String preview;
	private String filterPreview;

	private int titleHash = 1;
	private int textHash = 2;
	private int origTitleHash = 4;
	private int origTextHash = 5;
	private int savedTitleHash = 6;
	private int savedTextHash = 7;
	private int previewSourceHash = 8;
	private int filterPreviewSourceHash = 9;
	
	private Status status = Status.UNKNOWN;
	private TitleStatus titleStatus = TitleStatus.UNKNOWN;
	
	private boolean requestPreview = false;
	private boolean requestFilterPreview = false;

	private String srcLangCode = null;
	
	
	// INIT
	public MinorityArticle(WikiReference wref, Reference ref, String wikiTitle, String text)
	 {
		if (wref == null) throw new NullPointerException("wiki reference cannot be null");
		if (ref == null) throw new NullPointerException("reference cannot be null");
		if (wikiTitle == null) wikiTitle = "";
		if (text == null) text = "";
		
		this.request = wref;
		this.ref = ref;
		this.title = title(wikiTitle);
		this.text = text;
		this.exact = false;
		this.preview = "";
		this.filterPreview = "";
	 }
	
	
	// CONVERSION
	public FullRequest createFullRef() {
		return new FullRequest(ref.getQid(), ref.getLangCode(), request.getNamespace(), WikisProxy.wikiTitle(title, ref.getLangCode()));
	}
	
	
	// REFERENCE
	public Reference getRef() {
		return ref;
	}

	
	// REQUEST
	public WikiReference createRequest()
	 {
		return new WikiReference(ref.getLangCode(), request.getNamespace(), WikisProxy.wikiTitle(title, ref.getLangCode()));
	 }
	
	
	// CONTENT
	public WikiReference getOriginalWikiReference() {
		return request;
	}
	
	public String getPreviousRevision() {
		return prevrev;
	}
	
	public Status getStatus() {
		return status;
	}
	
	public TitleStatus getTitleStatus() {
		if (titleStatus == null) return TitleStatus.UNKNOWN;
		return titleStatus;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getText() {
		return text;
	}

	public String getFilteredText() {
		return ContentUtil.filterText(text);
	}
	
	public boolean isExact() {
		return exact;
	}
	
	public boolean isPreviewRequired() {
		return previewSourceHash != textHash;
	}
	
	public String getPreview() {
		return preview;
	}
	
	public boolean isRequestPreview() {
		return requestPreview;
	}

	public boolean isFilteredPreviewRequired() {
		int filteredTextHash = getFilteredText().hashCode();
		return filterPreviewSourceHash != filteredTextHash;
	}
	
	public String getFilteredPreview() {
		return filterPreview;
	}
	
	public boolean isRequestFilteredPreview() {
		return requestFilterPreview;
	}

	public boolean isUploadRequired() {
		return !isOriginal();
	}
	
	public boolean isOriginal() {
		return titleHash == origTitleHash && textHash == origTextHash;
	}

	public boolean isSaved() {
		return titleHash == savedTitleHash && textHash == savedTextHash;
	}

	public boolean isLinkableNamespace() {
		Namespace namespace = request.getNamespace();
		return  namespace == Namespace.ARTICLE || namespace == Namespace.CATEGORY || namespace == Namespace.WIKIPEDIA;
	}
	
	public boolean isLinkingRequired() {
		return isNew() && isLinkableNamespace();
	}
	
	public boolean isTitleOriginal() {
		return titleHash == origTitleHash;
	}
	
	public boolean isNew() {
		return origTitleHash == EMPTY_HASH;
	}

	public Articon findArticon()
	 {
		switch (status) {
		case UPLOAD_CONFLICT: return Articon.PROBLEM;
		case UPLOAD_FAILED: return Articon.PROBLEM;
		case UNKNOWN: return Articon.BUSY;
		case DOWNLOAD_REQUESTED: return Articon.BUSY;
		case DOWNLOAD_IN_PROGRESS: return Articon.BUSY;
		case UPLOAD_REQUESTED: return Articon.BUSY;
		case UPLOAD_PREPARING: return Articon.BUSY;
		case UPLOAD_IN_PROGRESS: return Articon.BUSY;
		case UPLOAD_CONFIRMING: return Articon.BUSY;
		case LINKING_ARTICLES: return Articon.BUSY;

		default:
			if (!isOriginal()) return Articon.EDITED;
			if (isNew()) return Articon.NEW;
			else return Articon.EXISTS;
		}
	 }
	
	public boolean isEmpty() {
		return title.isEmpty() || text.isEmpty();
	}
	
	public String getSrcLangCode() {
		return srcLangCode;
	}
	
	
	// GENERALY UNMUTABLE
	void setStatus(Status status) {
		this.status = status;
	}
	
	public void setTitleStatus(TitleStatus titleStatus) {
		this.titleStatus = titleStatus;
	}

	void setPreviousRevision(String rev) {
		this.prevrev = rev;
	}

	void setTitle(String title) {
		this.title = title(title);
		this.titleHash = this.title.hashCode();
	}

	void setText(String text) {
		this.text = text;
		this.textHash = this.text.hashCode();
	}

	void setExact(boolean exact) {
		this.exact = exact;
	}

	void requestPreview()
	 {
		if (!isPreviewRequired()) return;
		
		requestPreview = true;
	 }

	void requestFilteredPreview()
	 {
		if (!isFilteredPreviewRequired()) return;
		
		requestFilterPreview = true;
	 }

	void requestUpload()
	 {
		if (!isUploadRequired()) return;
		
		if (status != Status.STANDBY) return;
		
		status = Status.UPLOAD_REQUESTED;
	 }
	
	void cancelRequestUpload()
	 {
		if (status != Status.UPLOAD_REQUESTED) return;
		
		status = Status.STANDBY;
	 }
	
	boolean setPreview(String source, String preview)
	 {
		if (this.previewSourceHash == this.textHash) return false;
		
		int previewSourceHash = source.hashCode();
		
		this.preview = preview;
		this.previewSourceHash = previewSourceHash;
		
		if (previewSourceHash == this.previewSourceHash) requestPreview = false;
		else requestPreview = true;
		
		return true;
	 }

	boolean setFilteredPreview(String source, String preview)
	 {
		int filteredTextHash = getFilteredText().hashCode();
		
		if (this.filterPreviewSourceHash == filteredTextHash) return false;
		
		int filteredPreviewSourceHash = source.hashCode();
		
		this.filterPreview = preview;
		this.filterPreviewSourceHash = filteredPreviewSourceHash;
		
		if (filteredPreviewSourceHash == this.filterPreviewSourceHash) requestFilterPreview = false;
		else requestFilterPreview = true;
		
		return true;
	 }
	
	void setOriginal() {
		origTitleHash = titleHash;
		origTextHash = textHash;
	}
	
	void setSaved() {
		savedTitleHash = titleHash;
		savedTextHash = textHash;
	}
	
	void setSrcLangCode(String srcLangCode) {
		this.srcLangCode = srcLangCode;
	}
	
	
	// WORKINGS
	@Override
	public String toString() {
		return "{ref=" + ref.toString() + " title=" + title + " ...}";
	}
	
	private String title(String title)
	 {
		WikiType wikiType = MinorityTranslateModel.wikis().getWikiType(ref.getLangCode());
		switch (wikiType) {
		case REGULAR:
			return title;
			
		case INCUBATOR:
			String prefix = WikisProxy.prefix(ref.getLangCode());
			return WikisProxy.deprefixTitle(title, prefix);

		default:
			return title;
		}
	 }
	
	
}
