package ee.translate.keeleleek.mtapplication.view.swing.swingview;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import ee.translate.keeleleek.mtapplication.view.swing.dialogs.ConfirmationDialogMediatorSwing;


public class SwingConfirmationDialogViewComponent extends JDialog {

	private static final long serialVersionUID = 6355992936367530274L;
	
	public ConfirmationDialogMediatorSwing mediator;
	
	private final JPanel contentPanel = new JPanel();
	public JLabel messageLabel;
	public JLabel detailsLabel;
	public JButton cancelButton;
	public JButton noButton;
	public JButton yesButton;

	/**
	 * Create the dialog.
	 */
	public SwingConfirmationDialogViewComponent(JFrame parent) {
		super(parent);
		setBounds(100, 100, 300, 130);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			messageLabel = new JLabel("!Confirmation text?!");
			messageLabel.setFont(new Font("Dialog", Font.BOLD, 13));
		}
		{
			detailsLabel = new JLabel("!Details text.!");
			detailsLabel.setFont(new Font("Dialog", Font.PLAIN, 12));
		}
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
						.addComponent(messageLabel, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 260, Short.MAX_VALUE)
						.addComponent(detailsLabel, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 260, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(messageLabel, GroupLayout.DEFAULT_SIZE, 18, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(detailsLabel, GroupLayout.DEFAULT_SIZE, 17, Short.MAX_VALUE)
					.addGap(5))
		);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				yesButton = new JButton("!Yes!");
				yesButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						mediator.onYesClick();
					}
				});
				{
					cancelButton = new JButton("!Cancel!");
					cancelButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							mediator.onCancelClick();
						}
					});
					cancelButton.setActionCommand("OK");
					buttonPane.add(cancelButton);
				}
				{
					JPanel panel = new JPanel();
					buttonPane.add(panel);
				}
				{
					noButton = new JButton("!No!");
					noButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							mediator.onNoClick();
						}
					});
					noButton.setActionCommand("OK");
					buttonPane.add(noButton);
				}
				yesButton.setActionCommand("OK");
				buttonPane.add(yesButton);
				getRootPane().setDefaultButton(yesButton);
			}
		}
	}

}
