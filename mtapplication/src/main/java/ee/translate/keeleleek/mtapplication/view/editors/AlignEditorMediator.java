package ee.translate.keeleleek.mtapplication.view.editors;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.mediator.Mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteChoice;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteChoices;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteRequest;
import ee.translate.keeleleek.mtapplication.view.swing.application.ApplicationMediatorSwing;


public abstract class AlignEditorMediator extends Mediator {

	protected static Logger LOGGER = LoggerFactory.getLogger(ApplicationMediatorSwing.class);

	
	public final static String NAME = "{7882E43F-64F9-4686-AF9D-C5544781F28C}";
	public final static String REGEX = "\n\n|(?<===)\n";
	protected static int ROW_COUNT = 4;
	
	protected boolean feedback = false;
	
	
	// INIT
	public AlignEditorMediator() {
		super(NAME, null);
	}
	
	@Override
	public void onRegister() {
		
	}

	@Override
	public String[] listNotificationInterests() {
		return new String[]
		 {
			Notifications.ALIGN_EDITOR_OPENED,
			Notifications.ALIGN_EDITOR_CLOSED,
			Notifications.ALIGN_EDITOR_SEGMENTS_MOVED
		 };
	}

	@Override
	public void handleNotification(INotification notification)
	 {
		switch (notification.getName()) {

		case Notifications.ALIGN_EDITOR_OPENED:
		case Notifications.ALIGN_EDITOR_CLOSED:
			case Notifications.ALIGN_EDITOR_SEGMENTS_MOVED:
			refreshSegments();
			break;

		default:
			break;
		}
	 }
	
	
	// REFRESH
	public void refreshSegments()
	 {
		int pages = (int) Math.ceil((double)MinorityTranslateModel.align().getSegmentCount() / ROW_COUNT);
		if (pages == 0) pages = 1;
		setPageCount(pages);
		
		fetchSegments();
	 }
	
	
	// INHERIT
	protected abstract void setPageCount(int pages);
	
	protected abstract void fetchSegments();
	
	public abstract void showAutocomplete(AutocompleteChoices autocomplete);

	public abstract void autocomplete(AutocompleteChoice autocomplete);
	
	
	// EVENTS
	public void onUpdateSegment(final int i, final String segment) {
		MinorityTranslateModel.notifier().doThreadSafe(new Runnable() {
			@Override
			public void run() {
				MinorityTranslateModel.align().updateSegment(i, segment);
			}
		});
	}

	public void onAutocomplete(AutocompleteRequest request)
	 {
		sendNotification(Notifications.REQUEST_AUTOCOMPLETE, request);
	 }
	
	public void onOk()
	 {
		sendNotification(Notifications.CLOSE_ALIGN_EDITOR, true);
	 }

	public void onCancel()
	 {
		sendNotification(Notifications.CLOSE_ALIGN_EDITOR, false);
	 }

	
	
}
