package ee.translate.keeleleek.mtapplication.view.elements;

import java.util.ArrayList;
import java.util.List;

import org.puremvc.java.multicore.patterns.mediator.Mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.LazyCaller;
import ee.translate.keeleleek.mtapplication.common.requests.FindRequest;
import ee.translate.keeleleek.mtapplication.common.requests.SpellCheckRequest;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteChoices;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteRequest;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.Status;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.TitleStatus;
import ee.translate.keeleleek.mtapplication.model.content.Reference;
import ee.translate.keeleleek.mtapplication.model.filters.FilesFilter;
import ee.translate.keeleleek.mtapplication.model.filters.IntroductionFilter;
import ee.translate.keeleleek.mtapplication.model.filters.ReferencesFilter;
import ee.translate.keeleleek.mtapplication.model.filters.TemplatesFilter;
import ee.translate.keeleleek.mtapplication.model.preferences.Symbol;
import ee.translate.keeleleek.mtpluginframework.spellcheck.Misspell;


public abstract class TranslateTabMediator extends Mediator {

	public final static String NAME = "{0A54F657-F4D6-450F-B43A-D8D8FB4F06AC}";
	
	public enum TabView { EDIT, PREVIEW };
	public enum TabMode { NORMAL, FILTERED };
	
	
	public final static long TITLE_DELAY = 500;
	public final static long TEXT_DELAY = 500;
	public final static long SPELLER_DELAY = 500;
	
	protected static Logger LOGGER = LoggerFactory.getLogger(TranslateTabMediator.class);
	
	protected Object tabObject = null; 
	
	
	private Reference ref = null;
	
	private boolean nnew;
	
	private TabView view = TabView.EDIT;
	private TabMode mode = TabMode.NORMAL;
	private String langCode;
	
	private LazyCaller titleCaller;
	private LazyCaller textCaller;
	private LazyCaller spellerCaller;
	
	
	// INIT
	public TranslateTabMediator()
	 {
		super(NAME, null);
	 }

	final public void initialise(TabMode mode)
	 {
		this.mode = mode;
		initialise();
	 }
	
	protected TabMode mode() {
		return mode;
	}
	
	protected void initialise() {
		
	}
	
	public Object getTabObject() {
		return tabObject;
	}
	
	public void setTabObject(Object tabObject) {
		this.tabObject = tabObject;
	}
	
	
	// COMMIT
	public void commit()
	 {
		switch (mode) {
		case NORMAL:
			if (titleCaller != null) titleCaller.force();
			if (textCaller != null) textCaller.force();
			break;
			
		case FILTERED:
			break;
			
		default:
			break;
		}
	 }
	
	
	// CONTENT
	public void fetchContent()
	 {
		String title = "";
		String text = "";
		String preview = "";
		boolean exact = false;
		boolean nnew = false;
		
		MinorityArticle article = null;
		if (ref != null) article = MinorityTranslateModel.content().getArticle(ref);
		
		if (article != null) {
			
			title = article.getTitle();
				
			switch (mode) {
			case NORMAL:
				text = article.getText();
				preview = article.getPreview();
				break;
				
			case FILTERED:
				text = article.getFilteredText();
				preview = article.getFilteredPreview();
				break;
				
			default:
				break;
			}
			
			exact = article.isExact();
			nnew = article.isNew();
				
		}

		changeReference(ref);
		setTitle(title);
		setText(text);
		setPreview(preview);
		setExact(exact);
		setNew(nnew);
		
		if (spellerCaller != null) spellerCaller.call();
	 }

	public void fetchStatus()
	 {
		Status status = MinorityTranslateModel.content().getStatus(ref);
		setStatus(status);
	 }

	public void fetchTitleStatus()
	 {
		TitleStatus status = MinorityTranslateModel.content().getTitleStatus(ref);
		setTitleStatus(status);
	 }
	
	public void fetchTitle()
	 {
		String title = MinorityTranslateModel.content().getTitle(ref);
		if (title == null) title = "";
		setTitle(title);
	 }
	
	public void fetchFilteredText()
	 {
		if (mode != TabMode.FILTERED) return;
		
		String text = MinorityTranslateModel.content().getFilteredText(ref);;
		if (text == null) text = "";
		
		setText(text);
	 }

	public void fetchPreview()
	 {
		String preview = null;
		
		switch (mode) {
		case NORMAL:
			preview = MinorityTranslateModel.content().getPreview(ref);
			break;
			
		case FILTERED:
			preview = MinorityTranslateModel.content().getFilteredPreview(ref);
			break;
			
		default:
			break;
		}
		
		if (preview == null) preview = "";
		
		setPreview(preview);
	 }
	
	public void fetchFilters()
	 {
		switch (mode) {
		case NORMAL:
			break;
			
		case FILTERED:
			if (view == TabView.PREVIEW && ref != null) MinorityTranslateModel.notifier().sendNotification(Notifications.ENGINE_REQUEST_FETCH_FILTERED_PREVIEW, ref);
			setFilterTemplates(MinorityTranslateModel.preferences().program().isTemplatesFilter());
			setFilterFiles(MinorityTranslateModel.preferences().program().isFilesFilter());
			setFilterIntroduction(MinorityTranslateModel.preferences().program().isIntroductionFilter());
			setFilterReferences(MinorityTranslateModel.preferences().program().isReferencesFilter());
			break;
			
		default:
			break;
		}
	}

	public void fetchExact()
	 {
		if (ref == null) return;
		
		switch (mode) {
		case NORMAL:
		case FILTERED:
			boolean exact = MinorityTranslateModel.content().isExact(ref);
			setExact(exact);
			break;
			
		default:
			break;
		}
	}

	public void fetchSymbols()
	 {
		ArrayList<Symbol> symbols = MinorityTranslateModel.preferences().getSymbols().getSymbols(langCode);
		setSymbols(symbols);
	 }

	public void fetchSpellcheck()
	 {
		markMisspells(MinorityTranslateModel.speller().getMisspells(getReference()));
	 }

	
	// LANGUAGE
	public void setLangCode(String langCode)
	 {
		this.langCode = langCode;
	 }
	
	
	// REFERENCE
	public void changeReference(final Reference ref)
	 {
		this.ref = ref;
		
		if (titleCaller != null) titleCaller.force();
		if (textCaller != null) textCaller.force();
			
		switch (mode) {
		case NORMAL:
			titleCaller = new LazyCaller(new Runnable() {
				
				@Override
				public void run() {
					if (ref == null) return;
					MinorityTranslateModel.notifier().sendNotification(Notifications.ENGINE_CHANGE_ARTICLE_TITLE, ref, getTitle());
				}
				
			}, TITLE_DELAY);
			
			textCaller = new LazyCaller(new Runnable() {
				
				@Override
				public void run() {
					if (ref == null) return;;
					MinorityTranslateModel.notifier().sendNotification(Notifications.ENGINE_CHANGE_ARTICLE_TEXT, ref, getText());
				}
				
			}, TEXT_DELAY, true);
			
			spellerCaller = new LazyCaller(new Runnable() {
				
				@Override
				public void run() {
					if (ref == null) return;;
					
					String pluginName = getSpellerPluginName();
					if (pluginName == null) return;
					
					MinorityTranslateModel.notifier().sendNotification(Notifications.REQUEST_SPELLCHECK, new SpellCheckRequest(pluginName, ref));
				}
				
			}, SPELLER_DELAY, true);
			
			
			if (view == TabView.PREVIEW && ref != null) MinorityTranslateModel.notifier().sendNotification(Notifications.ENGINE_REQUEST_FETCH_PREVIEW, ref);
			
			break;

		case FILTERED:
			titleCaller = new LazyCaller(new Runnable() {
				
				@Override
				public void run() {
					// DO NOTHING
				}
				
			}, TITLE_DELAY);
			textCaller = new LazyCaller(new Runnable() {
				
				@Override
				public void run() {
					// DO NOTHING
				}
				
			}, TEXT_DELAY);
			
			if (view == TabView.PREVIEW && ref != null) MinorityTranslateModel.notifier().sendNotification(Notifications.ENGINE_REQUEST_FETCH_FILTERED_PREVIEW, ref);
			
			break;
			
		default:
			break;
		}
	 }
	
	public Reference getReference() {
		return ref;
	}
	
	
	// STATUS
	public TabView getTabView() {
		return view;
	}
	
	public TabMode getMode() {
		return mode;
	}
	
	public boolean isReadOnly() {
		return mode != TabMode.NORMAL;
	}
	
	
	// NEW
	public boolean isNnew() {
		return nnew;
	}
	
	protected void setNew(boolean nnew) {
		this.nnew = nnew;
	}

	
	// INHERIT (CONTENT)
	public abstract String getTitle();
	public abstract String getText();
	
	public abstract void setName(String name);
	
	protected abstract void setTitle(String title);
	protected abstract void setText(String text);
	public abstract void replaceText(String text);
	protected abstract void setStatus(Status status);
	public abstract void setTitleStatus(TitleStatus status);
	
	protected abstract void setPreview(String html);
	protected abstract void setView(TabView view);

	protected abstract void setExact(boolean exact);
	protected abstract void setSymbols(List<Symbol> symbols);
	
	protected abstract void setFilterTemplates(boolean filter);
	protected abstract void setFilterFiles(boolean filter);
	protected abstract void setFilterIntroduction(boolean filter);
	protected abstract void setFilterReferences(boolean filter);
	
	public abstract boolean isEditFocused();

	public abstract void pasteText(String text);
	
	// INHERIT (SPELLING)
	public abstract String getSpellerPluginName();
	public abstract void markMisspells(List<Misspell> misspells);
	
	// INHERIT (AUTOCOMPLETE)
	public abstract void showAutocomplete(AutocompleteChoices autocomplete);

	// INHERIT (FIND)
	public abstract void find(FindRequest request);
	
	
	// EVENTS
	public void onTitleEdited() {
		titleCaller.call();
	}
	
	public void onTextEdited() {
		textCaller.call();
		spellerCaller.call();
	}
	
	public void onChangeView(TabView view)
	 {
		this.view = view;
		
		setView(this.view);
		
		switch (mode) {
		case NORMAL:
			switch (this.view)
			 {
				case PREVIEW:
					if (ref != null) {
						commit();
						MinorityTranslateModel.notifier().sendNotification(Notifications.ENGINE_REQUEST_FETCH_PREVIEW, ref);
					}
					break;
	
				default:
					break;
			 }
			
			break;
			
		case FILTERED:
			switch (this.view)
			 {
				case PREVIEW:
					if (ref != null) {
						commit();
						MinorityTranslateModel.notifier().sendNotification(Notifications.ENGINE_REQUEST_FETCH_FILTERED_PREVIEW, ref);
					}
					break;
	
				default:
					break;
			 }
			
			break;

		default:
			break;
		}
		
	 }

	public void onChangeExact(boolean exact)
	 {
		String truefalse = exact ? "true" : "false";
		MinorityTranslateModel.notifier().sendNotification(Notifications.ENGINE_CHANGE_ARTICLE_EXACT, ref, truefalse);
	 }

	public void onChangeTemplatesFilter(boolean filter)
	 {
		MinorityTranslateModel.notifier().sendNotification(Notifications.PREFERENCES_CHANGE_FILTER, filter, TemplatesFilter.NAME);
		MinorityTranslateModel.notifier().sendNotification(Notifications.PREFERENCES_SAVE);
	 }

	public void onChangeFilesFilter(boolean filter)
	 {
		MinorityTranslateModel.notifier().sendNotification(Notifications.PREFERENCES_CHANGE_FILTER, filter, FilesFilter.NAME);
		MinorityTranslateModel.notifier().sendNotification(Notifications.PREFERENCES_SAVE);
	 }

	public void onChangeIntroductionFilter(boolean filter)
	 {
		MinorityTranslateModel.notifier().sendNotification(Notifications.PREFERENCES_CHANGE_FILTER, filter, IntroductionFilter.NAME);
		MinorityTranslateModel.notifier().sendNotification(Notifications.PREFERENCES_SAVE);
	 }

	public void onChangeReferencesFilter(boolean filter)
	 {
		MinorityTranslateModel.notifier().sendNotification(Notifications.PREFERENCES_CHANGE_FILTER, filter, ReferencesFilter.NAME);
		MinorityTranslateModel.notifier().sendNotification(Notifications.PREFERENCES_SAVE);
	 }

	public void onAutocomplete(AutocompleteRequest request)
	 {
		MinorityTranslateModel.notifier().sendNotification(Notifications.REQUEST_AUTOCOMPLETE, request);
	 }

	public void onSpellCheckRequest()
	 {
		if (ref == null) return;
		
		String pluginName = getSpellerPluginName();
		if (pluginName == null) return;
		
		MinorityTranslateModel.notifier().sendNotification(Notifications.REQUEST_SPELLCHECK, new SpellCheckRequest(pluginName, ref));
	 }

	public void onReset()
	 {
		Reference ref = getReference();
		if (ref == null) return;
		
		commit();
		
		MinorityTranslateModel.notifier().sendNotification(Notifications.ENGINE_RESET_ARTICLE, ref);
	 }

}
