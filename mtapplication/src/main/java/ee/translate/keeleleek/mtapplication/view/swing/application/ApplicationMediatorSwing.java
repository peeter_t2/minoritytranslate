package ee.translate.keeleleek.mtapplication.view.swing.application;

import java.awt.Desktop;
import java.awt.Dialog.ModalityType;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Window;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.net.URI;
import java.nio.file.Path;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.keys.KeyHandlerProxy.KeyShortcut;
import ee.translate.keeleleek.mtapplication.model.media.IconsProxy;
import ee.translate.keeleleek.mtapplication.view.application.ApplicationMediator;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtapplication.view.swing.MediatorLoaderSwing;
import ee.translate.keeleleek.mtapplication.view.swing.dialogs.MessageDialogMediatorSwing;
import ee.translate.keeleleek.mtapplication.view.windows.TranslateWindowMediator;

public class ApplicationMediatorSwing extends ApplicationMediator {

	
//	private Image icon;
	
	private JFrame transtaleWindow;
	private JDialog loginWindow;
	private JDialog preferencesWindow;
	private JDialog addCategoryWindow;
	private JDialog addArticleWindow;
	private JDialog removeArticleWindow;
	private JDialog addLinksWindow;
	private JDialog aboutWindow;
	private JDialog manualWindow;
	private JDialog listsWindow;
	
	private FileNameExtensionFilter filter;

	private boolean ctrl = false;
//	private boolean alt = false;
	
	
	// INIT
	public ApplicationMediatorSwing(Object translateWindow)
	 {
		super();
		
		this.transtaleWindow = (JFrame) translateWindow;
		
		// File chooser:
		filter = new FileNameExtensionFilter(Messages.getString("session.dialog.session.file"), "ses");
	 }
	
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
		ImageIcon icon = new ImageIcon(IconsProxy.class.getResource("/icon.png"));
		transtaleWindow.setIconImage(icon.getImage());
		
		transtaleWindow.setTitle(Messages.getString("window.translate"));
		transtaleWindow.setLocationByPlatform(true);
//		icon = new Image("icon.png");
//		transtaleWindow.getIcons().add(icon);
		
		addArticleWindow = new JDialog(transtaleWindow);
		removeArticleWindow = new JDialog(transtaleWindow);
		addCategoryWindow = new JDialog(transtaleWindow);
		addLinksWindow = new JDialog(transtaleWindow);
		preferencesWindow = new JDialog(transtaleWindow);
//		aboutWindow = new Stage(StageStyle.UTILITY);
//		manualWindow = new Stage(StageStyle.UTILITY);
//		
		Mediator mediator;

//		// Articlons pane:
//		mediator = (ArticlonsPaneMediator) MediatorLoader.loadArticlonsPaneMediator();
//		getFacade().registerMediator(mediator);
//	
//		// Addons pane:
//		mediator = (AddonsPaneMediator) MediatorLoader.loadAddonsPaneMediator();
//		getFacade().registerMediator(mediator);

		// Translate window:
		mediator = (TranslateWindowMediator) MediatorLoaderSwing.loadTranslateWindowMediator();
		getFacade().registerMediator(mediator);
		transtaleWindow.setContentPane((JPanel)mediator.getViewComponent());

		// Login window:
		mediator = MediatorLoaderSwing.loadLoginWindowMediator(transtaleWindow);
		getFacade().registerMediator(mediator);
		loginWindow = (JDialog) mediator.getViewComponent();

		// Preferences window:
		mediator = MediatorLoaderSwing.loadPreferencesWindowMediator(transtaleWindow);
		getFacade().registerMediator(mediator);
		preferencesWindow = (JDialog)mediator.getViewComponent();
		
		// Add category window:
		mediator = MediatorLoaderSwing.loadAddCategoryWindowMediator();
		getFacade().registerMediator(mediator);
		addCategoryWindow.setContentPane((JPanel)mediator.getViewComponent());

		// Add article window:
		mediator = MediatorLoaderSwing.loadAddArticleWindowMediator();
		getFacade().registerMediator(mediator);
		addArticleWindow.setContentPane((JPanel)mediator.getViewComponent());

		// Remove article window:
		mediator = MediatorLoaderSwing.loadRemoveArticleWindowMediator();
		getFacade().registerMediator(mediator);
		removeArticleWindow.setContentPane((JPanel)mediator.getViewComponent());

		// Add category window:
		mediator = MediatorLoaderSwing.loadAddLinksWindowMediator();
		getFacade().registerMediator(mediator);
		addLinksWindow.setContentPane((JPanel)mediator.getViewComponent());

		// About window:
		mediator = MediatorLoaderSwing.loadAboutWindowMediator(transtaleWindow);
		getFacade().registerMediator(mediator);
		aboutWindow = (JDialog) mediator.getViewComponent();

		// Manual window:
		mediator = MediatorLoaderSwing.loadManualWindowMediator(transtaleWindow);
		getFacade().registerMediator(mediator);
		manualWindow = (JDialog) mediator.getViewComponent();

		// Lists window:
		mediator = MediatorLoaderSwing.loadListsWindowMediator(transtaleWindow);
		getFacade().registerMediator(mediator);
		listsWindow = (JDialog) mediator.getViewComponent();

		// Modality:
		setupModalWindow(loginWindow, Messages.getString("window.login"));
		setupModalWindow(addArticleWindow, Messages.getString("window.add.article"));
		setupModalWindow(removeArticleWindow, Messages.getString("window.remove.article"));
		setupModalWindow(addCategoryWindow, Messages.getString("window.add.category"));
		setupModalWindow(addLinksWindow, Messages.getString("window.add.links"));
		setupModalWindow(preferencesWindow, Messages.getString("window.preferences"));
		setupModalWindow(aboutWindow, Messages.getString("window.about"));
		setupModalWindow(manualWindow, Messages.getString("window.manual"));
		setupModalWindow(listsWindow, Messages.getString("window.lists"));
		
		// exceptions
		preferencesWindow.setResizable(true);
		
		// keys
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {
            @Override
            public boolean dispatchKeyEvent(KeyEvent e) {
            	if (e.getID() == KeyEvent.KEY_PRESSED) {
            		
            		switch (e.getKeyCode()) {
            		case KeyEvent.VK_CONTROL:
						ctrl = true;
						break;
						
            		case KeyEvent.VK_ALT:
//						alt = true;
						break;

					default:
						break;
					}
            		
            	} else if (e.getID() == KeyEvent.KEY_RELEASED) {
            		
            		switch (e.getKeyCode()) {
            		case KeyEvent.VK_CONTROL:
						ctrl = false;
						break;
					
            		case KeyEvent.VK_ALT:
//						alt = false;
						break;
						
            		case KeyEvent.VK_1:
						if (ctrl) MinorityTranslateModel.keys().handle(KeyShortcut.SNIPPET_1);
						return true;
						
            		case KeyEvent.VK_2:
						if (ctrl) MinorityTranslateModel.keys().handle(KeyShortcut.SNIPPET_2);
						return true;
						
            		case KeyEvent.VK_3:
						if (ctrl) MinorityTranslateModel.keys().handle(KeyShortcut.SNIPPET_3);
						return true;
						
            		case KeyEvent.VK_4:
						if (ctrl) MinorityTranslateModel.keys().handle(KeyShortcut.SNIPPET_4);
						return true;
						
            		case KeyEvent.VK_5:
						if (ctrl) MinorityTranslateModel.keys().handle(KeyShortcut.SNIPPET_5);
						return true;

            		case KeyEvent.VK_6:
						if (ctrl) MinorityTranslateModel.keys().handle(KeyShortcut.SNIPPET_6);
						return true;

            		case KeyEvent.VK_7:
						if (ctrl) MinorityTranslateModel.keys().handle(KeyShortcut.SNIPPET_7);
						return true;

            		case KeyEvent.VK_8:
						if (ctrl) MinorityTranslateModel.keys().handle(KeyShortcut.SNIPPET_8);
						return true;

            		case KeyEvent.VK_9:
						if (ctrl) MinorityTranslateModel.keys().handle(KeyShortcut.SNIPPET_9);
						return true;

					default:
						break;
					}
            		
            	}
            	
            	return false;
            }
		});
		
		// Quit request:
		transtaleWindow.addWindowListener(new WindowAdapter() {
			
			@Override
			public void windowClosing(WindowEvent e) {
				sendNotification(Notifications.REQUEST_QUIT);
			}
			
		});
		
		setupSessionDialog();
		
		// Show:
		transtaleWindow.setSize(transtaleWindow.getPreferredSize());
		transtaleWindow.pack();
	 }
	
	private void setupSessionDialog()
	 {
		// Open/Save dialogs:
		UIManager.put("FileChooser.saveDialogTitleText", Messages.getString("session.dialog.save"));
		UIManager.put("FileChooser.openDialogTitleText", Messages.getString("session.dialog.open"));
		
		UIManager.put("FileChooser.openButtonText", Messages.getString("session.dialog.open"));
		UIManager.put("FileChooser.directoryOpenButtonText", Messages.getString("session.dialog.open"));
		
		UIManager.put("FileChooser.saveButtonText", Messages.getString("session.dialog.save"));
		
		UIManager.put("FileChooser.cancelButtonText", Messages.getString("button.cancel"));
		
		UIManager.put("FileChooser.lookInLabelText", Messages.getString("session.dialog.look.in"));
		UIManager.put("FileChooser.saveInLabelText", Messages.getString("session.dialog.save.in"));
		
		UIManager.put("FileChooser.fileNameLabelText", Messages.getString("session.dialog.file.name"));
		UIManager.put("FileChooser.filesOfTypeLabelText", Messages.getString("session.dialog.files.type"));
		
		UIManager.put("FileChooser.newFolderErrorText", Messages.getString("session.dialog.new.folder.error"));
		
		UIManager.put("FileChooser.fileNameHeaderText", Messages.getString("session.dialog.file.name"));
		UIManager.put("FileChooser.fileTypeHeaderText", Messages.getString("session.dialog.file.type"));
		UIManager.put("FileChooser.fileSizeHeaderText", Messages.getString("session.dialog.file.size"));
		UIManager.put("FileChooser.fileDateHeaderText", Messages.getString("session.dialog.file.date"));
		
		UIManager.put("FileChooser.upFolderToolTipText", "");
		UIManager.put("FileChooser.homeFolderToolTipText", "");
		UIManager.put("FileChooser.newFolderToolTipText", "");
		UIManager.put("FileChooser.listViewButtonToolTipText", "");
		UIManager.put("FileChooser.detailsViewButtonToolTipText", "");
		UIManager.put("FileChooser.openButtonToolTipText", "");
		UIManager.put("FileChooser.saveButtonToolTipText", "");
		UIManager.put("FileChooser.directoryOpenButtonToolTipText", "");
		UIManager.put("FileChooser.cancelButtonToolTipText", "");
		
		// Option pane:
		UIManager.put("OptionPane.yesButtonText", Messages.getString("session.dialog.yes"));
		UIManager.put("OptionPane.noButtonText", Messages.getString("session.dialog.no"));
	 }
	
	private void setupModalWindow(JDialog window, String title)
	 {
		window.setTitle(title);
//		stage.getIcons().add(icon);
		window.setModalityType(ModalityType.APPLICATION_MODAL);
		window.pack();
		window.setResizable(false);
	 }
	
	private void showModalWindow(Window window) {
		window.setLocationRelativeTo(transtaleWindow);
		window.setVisible(true);
	}


	// IMPLEMENTATION:
	@Override
	public Object getWindow() {
		return transtaleWindow;
	}
	
	@Override
	protected void setTitle(String title) {
		transtaleWindow.setTitle(title);
	}

	@Override
	protected void translateWindowOpen() {
		transtaleWindow.setVisible(true);
	}

	@Override
	protected void translateWindowClose() {
		transtaleWindow.dispose();
	}

	@Override
	protected void loginWindowOpen() {
		showModalWindow(loginWindow);
	}

	@Override
	protected void preferencesWindowOpen() {
		showModalWindow(preferencesWindow);
	}

	@Override
	protected void addArticleWindowOpen() {
		showModalWindow(addArticleWindow);;
	}

	@Override
	protected void addCategoryWindowOpen() {
		showModalWindow(addCategoryWindow);
	}

	@Override
	protected void removeArticleWindowOpen() {
		showModalWindow(removeArticleWindow);
	}

	@Override
	protected void addLinksWindowOpen() {
		showModalWindow(addLinksWindow);
	}

	@Override
	protected void aboutWindowOpen() {
		showModalWindow(aboutWindow);
	}

	@Override
	protected void manualWindowOpen() {
		showModalWindow(manualWindow);
	}

	@Override
	protected void sessionOpenWindowOpen() {
		JFileChooser sessionChooser = new JFileChooser();
		sessionChooser.setFileFilter(filter);
		sessionChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		sessionChooser.setAcceptAllFileFilterUsed(false);
		
		Path initial = MinorityTranslateModel.session().getWorkingPath();
		File file = initial.toFile();
		sessionChooser.setCurrentDirectory(file);
		
		if (sessionChooser.showOpenDialog(transtaleWindow) == JFileChooser.APPROVE_OPTION) {
			file = sessionChooser.getSelectedFile();
			if (file != null) {
				if (file != null) onSessionOpenConfirmed(file.toPath());
			}
		}
	}

	@Override
	protected void sessionSaveWindowOpen() {
		JFileChooser sessionChooser = new JFileChooser();
		sessionChooser.setFileFilter(filter);
		sessionChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		sessionChooser.setAcceptAllFileFilterUsed(false);
		
		Path initial = MinorityTranslateModel.session().getWorkingPath();
		File file = initial.toFile();
		sessionChooser.setCurrentDirectory(file);
		if (MinorityTranslateModel.session().getName() != null) sessionChooser.setSelectedFile(new File(file, MinorityTranslateModel.session().getName()));

		if (sessionChooser.showSaveDialog(transtaleWindow) == JFileChooser.APPROVE_OPTION) {
			file = sessionChooser.getSelectedFile();
			if (file != null) {
				if(!file.getName().endsWith(".ses")) file = new File(file.getAbsolutePath() + ".ses");
				onSessionSaveConfirmed(file.toPath());
			}
		}
	}

	@Override
	protected void listsWindowOpen() {
		listsWindow.setVisible(true);
	}
	
	
	@Override
	protected void showMessage(String title, String message, String details)
	 {
		MessageDialogMediatorSwing mediator = MediatorLoaderSwing.loadMessageWindowMediator(transtaleWindow);
		mediator.setText(message, details);
		setupModalWindow(mediator.getViewComponent(), title);
		mediator.getViewComponent().setVisible(true);
	 }
	
	@Override
	public void showDocument(String uri) {
		try {
			Desktop.getDesktop().browse(new URI(uri));
		} catch (Exception e) {
			LOGGER.warn("Failed to open URI", e);
		}
	}
	
	
	@Override
	protected void exit() {
		transtaleWindow.dispose();
	}
	
}
