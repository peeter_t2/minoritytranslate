package ee.translate.keeleleek.mtapplication.controller.gui;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;
import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.MinorityTranslate;
import ee.translate.keeleleek.mtapplication.view.javafx.application.ApplicationMediatorFX;
import ee.translate.keeleleek.mtapplication.view.swing.application.ApplicationMediatorSwing;

public class PrepareGUICommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		Object translateWindow = notification.getBody();
		
		Mediator mediator;
		
		if (!MinorityTranslate.FALLBACK_GUI) mediator = new ApplicationMediatorFX(translateWindow);
		else mediator = new ApplicationMediatorSwing(translateWindow);
		
		getFacade().registerMediator(mediator);
	 }
	
}
