package ee.translate.keeleleek.mtapplication.view.swing.dialogs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ListSelectionModel;

import ee.translate.keeleleek.mtapplication.model.lists.ListSuggestion;
import ee.translate.keeleleek.mtapplication.view.dialogs.ListsDialogMediator;
import ee.translate.keeleleek.mtapplication.view.swing.swingview.SwingListsWindowViewComponent;

public class ListsDialogMediatorSwing extends ListsDialogMediator {

	boolean manual = false;
	
	public final static int PROGRESS_MAX = 100;
	
	
	// IMPLEMENTATION:
	@Override
	public SwingListsWindowViewComponent getViewComponent() {
		return (SwingListsWindowViewComponent) super.getViewComponent();
	}
	
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
		getViewComponent().listNameComboBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (!manual) onSelectionChange();
			}
			
		});
		
		getViewComponent().languageComboBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (!manual) onSelectionChange();
			}
			
		});
		
		getViewComponent().list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	 }
	
	@Override
	protected void setListNames(String[] listNames) {
		manual = true;
		getViewComponent().listNameComboBox.setModel(new DefaultComboBoxModel<String>(listNames));
		if (listNames.length > 0) getViewComponent().listNameComboBox.setSelectedIndex(0);
		manual = false;
	}
	@Override
	public void addSuggestion(ListSuggestion suggestion) {
	}
	
	@Override
	public void removeSuggestion(ListSuggestion suggestion) {
		// TODO Auto-generated method stub
		
	}
	@Override
	protected void setLanguages(String[] languages) {
		manual = true;
		getViewComponent().languageComboBox.setModel(new DefaultComboBoxModel<String>(languages));
		if (languages.length > 0) getViewComponent().languageComboBox.setSelectedIndex(0);
		manual = false;
	}
	
	@Override
	public String getListName() {
		return (String) getViewComponent().listNameComboBox.getSelectedItem();
	}

	@Override
	public String getLanguage() {
		return (String) getViewComponent().languageComboBox.getSelectedItem();
	}

	@Override
	protected void setList(String[] values) {
		DefaultListModel<String> model = new DefaultListModel<>();
		for (int i = 0; i < values.length; i++) {
			model.addElement(values[i]);
		}
		int[] indices = getViewComponent().list.getSelectedIndices();
		getViewComponent().list.setModel(model);
		getViewComponent().list.setSelectedIndices(indices);
	}

	@Override
	public String[] getSelected() {
		String[] selected = new String[getViewComponent().list.getSelectedValuesList().size()];
		for (int i = 0; i < selected.length; i++) {
			selected[i] = getViewComponent().list.getSelectedValuesList().get(i);
		}
		return selected;
	}

	@Override
	protected void setProgress(double progress) {
		getViewComponent().progressBar.setValue((int)(progress * PROGRESS_MAX));
	}

	@Override
	protected void setProgressVisible(boolean visible) {
		getViewComponent().progressBar.setVisible(visible);
	}

	
	@Override
	protected void unselect() {
		getViewComponent().list.setSelectedIndices(new int[0]);
	}
	
	
	@Override
	public void setListsEnabled(boolean enabled) {
		getViewComponent().enableToggleButton.setSelected(enabled);
	}
	
	@Override
	public boolean isListsEnabled() {
		return getViewComponent().enableToggleButton.isSelected();
	}
	
	
}
