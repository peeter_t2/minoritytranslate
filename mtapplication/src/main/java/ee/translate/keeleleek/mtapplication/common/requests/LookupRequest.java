package ee.translate.keeleleek.mtapplication.common.requests;


public class LookupRequest {

	private String target;
	private String name;
	private String search;
	
	
	public LookupRequest(String target, String name, String search) {
		super();
		this.target = target;
		this.name = name;
		this.search = search;
	}
	
	
	public LookupResponse fulfill(String found)
	 {
		return new LookupResponse(target, name, search, found);
	 }
	
	
	public String getTarget() {
		return target;
	}
	
	public String getName() {
		return name;
	}
	
	public String getSearch() {
		return search;
	}
	
	
	public static class LookupResponse {
		
		private String target;
		private String name;
		private String search;
		private String found;
		
		
		public LookupResponse(String target, String lookup, String name, String found) {
			super();
			this.target = target;
			this.name = lookup;
			this.search = name;
			this.found = found;
		}

		
		public String getTarget() {
			return target;
		}
		
		public String getName() {
			return name;
		}
		
		public String getSearch() {
			return search;
		}
		
		public String getFound() {
			return found;
		}
		
		
	}
	
}
