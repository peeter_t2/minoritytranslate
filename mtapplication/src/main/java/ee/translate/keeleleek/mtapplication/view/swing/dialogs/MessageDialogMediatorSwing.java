package ee.translate.keeleleek.mtapplication.view.swing.dialogs;

import ee.translate.keeleleek.mtapplication.view.dialogs.MessageDialogMediator;
import ee.translate.keeleleek.mtapplication.view.swing.swingview.SwingMessageWindowViewComponent;


public class MessageDialogMediatorSwing extends MessageDialogMediator {
	
	// IMPLEMENTATION:
	@Override
	public SwingMessageWindowViewComponent getViewComponent() {
		return (SwingMessageWindowViewComponent) super.getViewComponent();
	}
	
	@Override
	public void setText(String message, String details) {
		getViewComponent().messageLabel.setText(message);
		getViewComponent().descriptionLabel.setText(details);
	}
	
	@Override
	public void close() {
		getViewComponent().setVisible(false);
	}

}
