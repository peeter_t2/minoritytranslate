package ee.translate.keeleleek.mtapplication.controller.session;

import java.nio.file.Path;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;

public class RequestSaveSessionCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		Path path = (Path) notification.getBody();
		
		sendNotification(Notifications.SESSION_SAVE, path);
		
		Path parent = path.getParent();
		if (parent != null) MinorityTranslateModel.session().setWorkingPath(parent);
	 }
	
}
