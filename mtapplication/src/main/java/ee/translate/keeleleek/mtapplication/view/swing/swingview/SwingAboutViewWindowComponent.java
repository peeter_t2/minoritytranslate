package ee.translate.keeleleek.mtapplication.view.swing.swingview;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtapplication.view.swing.dialogs.AboutDialogMediatorSwing;

public class SwingAboutViewWindowComponent extends JDialog {

	private static final long serialVersionUID = -3268112000786740767L;
	
	public AboutDialogMediatorSwing mediator;
	
	public JLabel minorityTranslateLogoLabel;
	public JLabel wikimediaLabel;
	public JLabel keeleleekLogoLabel;
	public JLabel versionLabel;
	public JLabel licenseLabel;

	/**
	 * Create the dialog.
	 */
	public SwingAboutViewWindowComponent(JFrame parent) {
		super(parent);
		setPreferredSize(new Dimension(450, 500));
		setBounds(100, 100, 450, 500);
		
		minorityTranslateLogoLabel = new JLabel("");
		minorityTranslateLogoLabel.setHorizontalAlignment(SwingConstants.CENTER);
		minorityTranslateLogoLabel.setIconTextGap(0);
		minorityTranslateLogoLabel.setIcon(null);
		
		JSeparator separator = new JSeparator();
		
		JLabel supportedByTextLabel = new JLabel(Messages.getString("about.supported.by")); //$NON-NLS-1$
		
		JPanel panel = new JPanel();
		
		JLabel thanksToTextLabel = new JLabel(Messages.getString("about.thanks.to"));
		
		JLabel thanksToLabel = new JLabel(Messages.getString("SwingAboutViewWindowComponent.thanksToLabel.text")); //$NON-NLS-1$
		thanksToLabel.setFont(new Font("Dialog", Font.PLAIN, 12));
		
		JPanel descPanel = new JPanel();
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(minorityTranslateLogoLabel, GroupLayout.DEFAULT_SIZE, 424, Short.MAX_VALUE)
						.addComponent(separator, GroupLayout.DEFAULT_SIZE, 424, Short.MAX_VALUE)
						.addComponent(supportedByTextLabel)
						.addComponent(panel, GroupLayout.DEFAULT_SIZE, 424, Short.MAX_VALUE)
						.addComponent(thanksToTextLabel)
						.addComponent(thanksToLabel)
						.addComponent(descPanel, GroupLayout.DEFAULT_SIZE, 424, Short.MAX_VALUE))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(minorityTranslateLogoLabel, GroupLayout.PREFERRED_SIZE, 192, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(descPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(separator, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(supportedByTextLabel)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(thanksToTextLabel)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(thanksToLabel)
					.addContainerGap(26, Short.MAX_VALUE))
		);
		GridBagLayout gbl_descPanel = new GridBagLayout();
		gbl_descPanel.columnWidths = new int[] {60, 0, 0};
		gbl_descPanel.rowHeights = new int[]{15, 0, 0, 0};
		gbl_descPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_descPanel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		descPanel.setLayout(gbl_descPanel);
		
		JLabel versionTextLabel = new JLabel(Messages.getString("about.version")); //$NON-NLS-1$
		versionTextLabel.setFont(new Font("Dialog", Font.PLAIN, 12));
		GridBagConstraints gbc_versionTextLabel = new GridBagConstraints();
		gbc_versionTextLabel.anchor = GridBagConstraints.WEST;
		gbc_versionTextLabel.insets = new Insets(0, 0, 5, 5);
		gbc_versionTextLabel.gridx = 0;
		gbc_versionTextLabel.gridy = 0;
		descPanel.add(versionTextLabel, gbc_versionTextLabel);
		
		versionLabel = new JLabel("-"); //$NON-NLS-1$
		GridBagConstraints gbc_versionLabel = new GridBagConstraints();
		gbc_versionLabel.anchor = GridBagConstraints.WEST;
		gbc_versionLabel.insets = new Insets(0, 0, 5, 0);
		gbc_versionLabel.gridx = 1;
		gbc_versionLabel.gridy = 0;
		descPanel.add(versionLabel, gbc_versionLabel);
		
		JLabel authorsTextLabel = new JLabel(Messages.getString("about.authors")); //$NON-NLS-1$
		authorsTextLabel.setFont(new Font("Dialog", Font.PLAIN, 12));
		GridBagConstraints gbc_authorsTextLabel = new GridBagConstraints();
		gbc_authorsTextLabel.anchor = GridBagConstraints.WEST;
		gbc_authorsTextLabel.insets = new Insets(0, 0, 5, 5);
		gbc_authorsTextLabel.gridx = 0;
		gbc_authorsTextLabel.gridy = 1;
		descPanel.add(authorsTextLabel, gbc_authorsTextLabel);
		
		JLabel authorsLabel = new JLabel("Andrjus Frantskjavit\u015Dius, Ivo Kruusam\u00E4gi");
		authorsLabel.setFont(new Font("Dialog", Font.PLAIN, 12));
		GridBagConstraints gbc_authorsLabel = new GridBagConstraints();
		gbc_authorsLabel.anchor = GridBagConstraints.WEST;
		gbc_authorsLabel.insets = new Insets(0, 0, 5, 0);
		gbc_authorsLabel.gridx = 1;
		gbc_authorsLabel.gridy = 1;
		descPanel.add(authorsLabel, gbc_authorsLabel);
		
		JLabel licenceTextLabel = new JLabel(Messages.getString("about.license")); //$NON-NLS-1$
		licenceTextLabel.setFont(new Font("Dialog", Font.PLAIN, 12));
		GridBagConstraints gbc_licenceTextLabel = new GridBagConstraints();
		gbc_licenceTextLabel.anchor = GridBagConstraints.WEST;
		gbc_licenceTextLabel.insets = new Insets(0, 0, 0, 5);
		gbc_licenceTextLabel.gridx = 0;
		gbc_licenceTextLabel.gridy = 2;
		descPanel.add(licenceTextLabel, gbc_licenceTextLabel);
		
		licenseLabel = new JLabel("<HTML><FONT color=\"#000099\"><U>CC BY-SA 3.0</U></FONT></HTML>");
		licenseLabel.setFont(new Font("Dialog", Font.PLAIN, 12));
		GridBagConstraints gbc_licenseLabel = new GridBagConstraints();
		gbc_licenseLabel.anchor = GridBagConstraints.WEST;
		gbc_licenseLabel.gridx = 1;
		gbc_licenseLabel.gridy = 2;
		descPanel.add(licenseLabel, gbc_licenseLabel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{210, 210, 0};
		gbl_panel.rowHeights = new int[]{107, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		wikimediaLabel = new JLabel("");
		wikimediaLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				mediator.onWMFImageViewClick();
			}
		});
		wikimediaLabel.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_wikimediaLabel = new GridBagConstraints();
		gbc_wikimediaLabel.insets = new Insets(0, 0, 0, 5);
		gbc_wikimediaLabel.gridx = 0;
		gbc_wikimediaLabel.gridy = 0;
		panel.add(wikimediaLabel, gbc_wikimediaLabel);
		
		keeleleekLogoLabel = new JLabel("");
		keeleleekLogoLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				mediator.onKeeleleekImageViewClick();
			}
		});
		keeleleekLogoLabel.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_keeleleekLogoLabel = new GridBagConstraints();
		gbc_keeleleekLogoLabel.gridx = 1;
		gbc_keeleleekLogoLabel.gridy = 0;
		panel.add(keeleleekLogoLabel, gbc_keeleleekLogoLabel);
		getContentPane().setLayout(groupLayout);

	}
}
