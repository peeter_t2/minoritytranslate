package ee.translate.keeleleek.mtapplication.view.javafx.fxemelents;

import ee.translate.keeleleek.mtapplication.model.preferences.TableEntry;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableRow;
import javafx.scene.control.TextField;

public abstract class ProcessingCell extends TableCell<TableEntry,String> {

	private TextField textEdit;
	
	
	// INIT
	public ProcessingCell()
	 {
		setPadding(new Insets(0));
		setAlignment(Pos.CENTER_LEFT);
	 }
	
	
	// ENTRY
	private int getRow()
	 {
		TableRow<?> row = getTableRow();
		if (row == null) return -1;
		return row.getIndex();
	 }
	
	public abstract int getColumn();
	
	protected String retrieveValue()
	 {
		int r = getRow();
		int c = getColumn();
		
		if (r == -1 || c == -1) return null;
		
		return getTableView().getItems().get(r).toStringEntry(c);
	 }
	
	protected void changeValue(String value)
	 {
		int r = getRow();
		int c = getColumn();
		
		if (r == -1 || c == -1) return;
		
		getTableView().getItems().get(r).fromStringEntry(c, value);
	 }
	
	protected abstract void createEntry(int i);
	protected abstract void removeEntry(int i);
	protected abstract void switchEntries(int i, int j);
	
	
	// EDIT
	@Override
	public void startEdit()
	 {
		super.startEdit();
		
		String text = null;
		textEdit = createTextField();
		
		String value = retrieveValue();
		textEdit.setText(value);
//		textEdit.setPrefRowCount(countRows(value) + 2);
		
		setText(text);
		setGraphic(textEdit);
	 }

	@Override
	public void commitEdit(String newValue)
	 {
		super.commitEdit(newValue);
		
		String text = null;
		textEdit = null;

		text = retrieveValue();
		
		setText(text);
		setGraphic(textEdit);
	 }
	
	@Override
	public void cancelEdit()
	 {
		super.cancelEdit();
		
		String text = null;
		textEdit = null;
		
		text = retrieveValue();
		
		setText(text);
		setGraphic(null);
	 }
	
	@Override
	protected void updateItem(String item, boolean empty)
	 {
		super.updateItem(item, empty);
		
		if (isEmpty()) {
			setText(null);
			setGraphic(null);
			setContextMenu(null);
			return;
		}

		String text = "";
		
		if (isEditing()) {
			
			text = null;
			if (textEdit != null) textEdit.setText(retrieveValue());
		
		} else {
		
			text = retrieveValue();
			textEdit = null;

		}
		
		setText(text);
		setGraphic(textEdit);
		if (getContextMenu() == null) setContextMenu(createCellMenu());
	 }
	
	private TextField createTextField()
	 {
		final TextField textEdit = new TextField();
		
		textEdit.setMinWidth(this.getWidth() - this.getGraphicTextGap()* 2);
		textEdit.setPadding(new Insets(0));
		
		textEdit.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> obs, Boolean previousValue, Boolean currentValue)
			 {
				if (!currentValue) {
					
					commitEdit(textEdit.getText());
					
					changeValue(textEdit.getText());

					updateItem(textEdit.getText(), false);
					
				}
			 }
		});
		
		return textEdit;
	 }

	private ContextMenu createCellMenu()
	 {
		ContextMenu contextMenu = new ContextMenu();

		// create before
		MenuItem createBefore = new MenuItem(Messages.getString("preferences.processing.create.row.before"));
		createBefore.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				int j = getTableRow().getIndex();
				createEntry(j);
			}
		});
		
		// create after
		MenuItem createAfter = new MenuItem(Messages.getString("preferences.processing.create.row.after"));
		createAfter.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				int j = getTableRow().getIndex() + 1;
				createEntry(j);
			}
		});

		// remove
		MenuItem remove = new MenuItem(Messages.getString("preferences.processing.remove.row"));
		remove.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				int j = getTableRow().getIndex();
				removeEntry(j);
			}
		});

		// move up
		MenuItem moveUp = new MenuItem(Messages.getString("preferences.processing.move.row.up"));
		moveUp.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				int i = getTableRow().getIndex();
				int j = i - 1;
				if (j < 0) return;
				switchEntries(i, j);
			}
		});
		
		// move down
		MenuItem moveDown = new MenuItem(Messages.getString("preferences.processing.move.row.down"));
		moveDown.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				int i = getTableRow().getIndex();
				int j = i + 1;
				if (j >= getTableView().getItems().size()) return;
				switchEntries(i, j);
			}
		});
		
		contextMenu.getItems().addAll(createBefore, createAfter, remove, moveUp, moveDown);
		
		return contextMenu;
	 }
	
	
	// UTILITY
	public static int countRows(String string)
	 {
		int count = 0;
		int i = 0;

		while ((i = string.indexOf('\n', i)) != -1){
			i++;
			count++;
		}

		return count + 1;
	  }
	

}
