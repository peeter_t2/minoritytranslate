package ee.translate.keeleleek.mtapplication.controller.editors;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.requests.SrcDstRequest;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.Status;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;

public class OpenAlignCommand extends SimpleCommand {

	private static Logger LOGGER = LoggerFactory.getLogger(OpenAlignCommand.class);
	
	
	@Override
	public void execute(INotification notification)
	 {
		SrcDstRequest request = (SrcDstRequest) notification.getBody();
		
		// check articles
		MinorityArticle srcArticle = MinorityTranslateModel.content().getArticle(request.getSrcRef());
		if (srcArticle == null) {
			LOGGER.error("Failed to align, because the article with reference " + request.getSrcRef() + " is missing!");
			return;
		}

		MinorityArticle dstArticle = MinorityTranslateModel.content().getArticle(request.getDstRef());
		if (dstArticle == null) {
			LOGGER.error("Failed to align, because the article with reference " + request.getDstRef() + " is missing!");
			return;
		}

		// check status
		if (dstArticle.getStatus() != Status.STANDBY) {
			sendNotification(Notifications.ERROR_MESSAGE, Messages.getString("messages.align.error.header"), Messages.getString("messages.align.error.content.destination.not.editable"));
			return;
		}

		if (srcArticle.getStatus() != Status.STANDBY) {
			sendNotification(Notifications.ERROR_MESSAGE, Messages.getString("messages.align.error.header"), Messages.getString("messages.align.error.content.source.not.ready"));
			return;
		}
		
		MinorityTranslateModel.align().open(srcArticle, dstArticle);
	 }
	
}
