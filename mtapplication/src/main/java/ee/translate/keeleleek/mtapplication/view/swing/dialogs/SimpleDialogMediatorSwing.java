package ee.translate.keeleleek.mtapplication.view.swing.dialogs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import ee.translate.keeleleek.mtapplication.view.dialogs.SimpleDialogMediator;
import ee.translate.keeleleek.mtapplication.view.swing.swingview.SwingSimpleDialogWindowViewComponent;

public class SimpleDialogMediatorSwing extends SimpleDialogMediator {

	boolean textManual = false;
	boolean languageManual = false;
	
	private JPopupMenu currentMenu = null;
	private JMenuItem currentMenuItem = null;
	
	
	// INIT
	public SimpleDialogMediatorSwing(String name) {
		super(name);
	}

	
	// NOTIFICATIONS
	public SwingSimpleDialogWindowViewComponent getViewComponent() {
		return (SwingSimpleDialogWindowViewComponent) super.getViewComponent();
	}
	
	public void onRegister()
	 {
		super.onRegister();

		getViewComponent().titleEdit.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				onTitleChanged(getViewComponent().titleEdit.getText());
			}
			
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				onTitleChanged(getViewComponent().titleEdit.getText());
			}
			
			@Override
			public void changedUpdate(DocumentEvent arg0) {
				onTitleChanged(getViewComponent().titleEdit.getText());
			}
			
		});
	
		getViewComponent().titleEdit.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) { }
			
			@Override
			public void keyReleased(KeyEvent e) { }
			
			@Override
			public void keyPressed(KeyEvent e)
			 {
				if (e.getKeyCode() != KeyEvent.VK_ENTER) return;
				if (currentMenuItem != null) onTitleSelected(currentMenuItem.getText());
			 }
		});
		
	 }
	
	
	// INHERIT
	@Override
	protected void initHeadingLabel(String label) {
		getViewComponent().headingLabel.setText(label);
	}

	@Override
	protected void initNameLabel(String label) {
		getViewComponent().titleLabel.setText(label);
	}
	
	@Override
	protected void initLanguages(String[] langNames) {
		languageManual = true;
		DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(langNames);
		getViewComponent().langCombo.setModel(model);
		if (langNames.length > 0) model.setSelectedItem(langNames[0]);
		languageManual = false;
	}

	
	@Override
	protected String getName() {
		return getViewComponent().titleEdit.getText();
	}
	
	@Override
	protected String getLangName() {
		int i = getViewComponent().langCombo.getSelectedIndex();
		if (i == -1) return null;
		return getViewComponent().langCombo.getItemAt(i);
	}

	
	@Override
	protected void updateTitle(String text)
	 {
		getViewComponent().titleEdit.setText(text);
		getViewComponent().titleEdit.setCaretPosition(text.length());
		if (currentMenu != null) {
			currentMenu.setVisible(false);
			currentMenu = null;
		}
	 }
	
	@Override
	protected void suggestTitles(String[] results)
	 {
		JPopupMenu menu = new JPopupMenu();
		
		for (int i = 0; i < results.length; i++) {
			
			JMenuItem menuItem = new JMenuItem(results[i]);
			
			menuItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					onTitleSelected(e.getActionCommand());
				}
			});
			
			menuItem.addChangeListener(new ChangeListener() {
				
				@Override
				public void stateChanged(ChangeEvent e) {
					if (e.getSource() instanceof JMenuItem) {
						JMenuItem aMenuItem = (JMenuItem) e.getSource();
						if (aMenuItem.isArmed()) currentMenuItem = aMenuItem;
						else currentMenuItem = null;
					}
				}
			});
			
			menu.add(menuItem);
		}
		
		if (results.length > 0){
			menu.show(getViewComponent().titleEdit, -1, getViewComponent().titleEdit.getHeight());
			getViewComponent().titleEdit.requestFocus();
			currentMenu = menu;
		} else {
			if (currentMenu != null) {
				currentMenu.setVisible(false);
				currentMenu = null;
			}
		}
	 }
	
    
	@Override
	protected void startEditing() {
		getViewComponent().titleEdit.setCaretPosition(getViewComponent().titleEdit.getText().length());
		getViewComponent().titleEdit.requestFocus();
	}
	
	@Override
	public void focus() {
		// TODO Auto-generated method stub
		
	}
	
	
}
