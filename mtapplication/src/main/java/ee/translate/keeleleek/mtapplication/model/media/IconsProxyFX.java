package ee.translate.keeleleek.mtapplication.model.media;

import java.util.HashMap;

import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.Articon;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.Status;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class IconsProxyFX extends IconsProxy {

	
	private HashMap<Status, Image> icons = new HashMap<>();
	private HashMap<Articon, Image> articons = new HashMap<>();
	
	
	// INIT
	public IconsProxyFX()
	 {
		icons.put(Status.UPLOAD_CONFIRMING, new Image("icons/UPLOAD_CONFIRMING.png"));
		icons.put(Status.UNKNOWN, new Image("icons/UNKNOWN.png"));
		icons.put(Status.UPLOAD_CONFLICT, new Image("icons/UPLOAD_CONFLICT.png"));
		icons.put(Status.UPLOAD_FAILED, new Image("icons/UPLOAD_FAILED.png"));
//		icons.put(Status.STANDBY, new Image("/icons/STANDBY.png"));
		icons.put(Status.DOWNLOAD_REQUESTED, new Image("icons/DOWNLOAD_REQUESTED.png"));
		icons.put(Status.DOWNLOAD_IN_PROGRESS, new Image("icons/DOWNLOADING.png"));
		icons.put(Status.UPLOAD_REQUESTED, new Image("icons/UPLOAD_REQUESTED.png"));
		icons.put(Status.UPLOAD_PREPARING, new Image("icons/PREPARING_UPLOAD.png"));
		icons.put(Status.UPLOAD_IN_PROGRESS, new Image("icons/UPLOADING.png"));
		
		articons.put(Articon.UNKNOWN, new Image("icons/MT-icon-UNKNOWN.png"));
		articons.put(Articon.EXISTS, new Image("icons/MT-icon-EXISTS.png"));
		articons.put(Articon.NEW, new Image("icons/MT-icon-NEW.png"));
		articons.put(Articon.EDITED, new Image("icons/MT-icon-EDITED.png"));
		articons.put(Articon.BUSY, new Image("icons/MT-icon-BUSY.png"));
		articons.put(Articon.PROBLEM, new Image("icons/MT-icon-PROBLEM.png"));
	 }


	// INHERIT (ICONS)
	@Override
	public Image getStatusIcon(Status status) {
		return icons.get(status);
	}

	@Override
	public Image getArticon(Articon articon) {
		return articons.get(articon);
	}

	public ImageView getArticonImage(Articon articon) {
		Image image = articons.get(articon);
		if (image != null) return new ImageView(image);
		return null;
	}
	
	
}
