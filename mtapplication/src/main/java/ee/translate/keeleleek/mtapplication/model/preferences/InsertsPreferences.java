package ee.translate.keeleleek.mtapplication.model.preferences;

import java.util.ArrayList;

public class InsertsPreferences {

	public final static char CARET_SYMBOL = '\u00BD';
	
	
	private ArrayList<Insert> inserts;

	
	// INIT
	public InsertsPreferences()
	 {
		inserts = new ArrayList<>();
	 }
	

	public InsertsPreferences(ArrayList<Insert> templates)
	 {
		this.inserts = templates;
	 }

	public InsertsPreferences(InsertsPreferences other)
	 {
		inserts = new ArrayList<>();
		
		for (Insert template : other.inserts) {
			this.inserts.add(new Insert(template));
		}
	 }

	public static InsertsPreferences create()
	 {
		InsertsPreferences preferences = new InsertsPreferences();
		
		preferences.addInsert("[[]]", "[[", "[[" + CARET_SYMBOL + "]]");
		preferences.addInsert("[[#]]", "[[#", "[[#]]");
		preferences.addInsert("{{}}", "{{", "{{" + CARET_SYMBOL + "}}");
		preferences.addInsert("{{#}}", "{{#", "{{#}}");
		preferences.addInsert("''' '''", "'''", "'''" + CARET_SYMBOL + "'''");
		preferences.addInsert("'''#'''", "'''#", "'''#'''");
		preferences.addInsert("== ==", "==", "==" + CARET_SYMBOL + "==");
		preferences.addInsert("==#==", "==#", "==#==\n" + CARET_SYMBOL + "");
		preferences.addInsert("<ref></ref>", "<ref>", "<ref>" + CARET_SYMBOL + "</ref>");
		preferences.addInsert("<ref name=\"\"></ref>", "<ref name=\"\">", "<ref name=\"" + CARET_SYMBOL + "\"></ref>");
		preferences.addInsert("<ref name=\"#\" />", "ref=#", "<ref name=\"#\" />");
		preferences.addInsert("<gallery></gallery>", "<gallery>", "<gallery>\n" + CARET_SYMBOL + "\n</gallery>");
		preferences.addInsert("<nowiki></nowiki>", "<nowiki>", "<nowiki>" + CARET_SYMBOL + "</nowiki>");
		preferences.addInsert("<math></math>", "<math>", "<math>\n" + CARET_SYMBOL + "\n</math>");
		preferences.addInsert("<small></small>", "<small>", "<small>" + CARET_SYMBOL + "</small>");
		preferences.addInsert("<!-- -->", "<!--", " <!--" + CARET_SYMBOL + "-->");
		preferences.addInsert("<!--#-->", "<!--#", "<!--#-->\n" + CARET_SYMBOL + "");
		preferences.addInsert("<includeonly></includeonly>", "<includeonly>", "<includeonly>" + CARET_SYMBOL + "</includeonly>");
		preferences.addInsert("<noinclude></noinclude>", "<noinclude>", "<noinclude>" + CARET_SYMBOL + "</noinclude>");
		preferences.addInsert("<s></s>", "<s>", "<s>" + CARET_SYMBOL + "</s>");
		preferences.addInsert("<sub></sub>", "<sub>", "<sub>" + CARET_SYMBOL + "</sub>");
		preferences.addInsert("<sup></sup>", "<sup>", "<sup>" + CARET_SYMBOL + "</sup>");
		preferences.addInsert("[[File:|thumb| ]]", "${File}:", "[[${File}:|thumb|" + CARET_SYMBOL + "]]");
		preferences.addInsert("[[Category: ]]", "${Category}:", "[[${Category}:" + CARET_SYMBOL + "]]");
		preferences.addInsert("<code></code>", "<code>", "<code>" + CARET_SYMBOL + "</code>");
		
		return preferences;
	 }
	

	// TEMPLATES
	public ArrayList<Insert> getInserts() {
		return inserts;
	}

	public void addInsert(String name, String trigger, String template)
	 {
		inserts.add(new Insert(name, trigger, template));
	 }
	
	
	// HELPERS
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((inserts == null) ? 0 : inserts.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		InsertsPreferences other = (InsertsPreferences) obj;
		if (inserts == null) {
			if (other.inserts != null) return false;
		} else if (!inserts.equals(other.inserts)) return false;
		return true;
	}

	
}
