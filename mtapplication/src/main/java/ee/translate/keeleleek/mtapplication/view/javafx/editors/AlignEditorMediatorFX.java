package ee.translate.keeleleek.mtapplication.view.javafx.editors;

import java.util.List;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteChoice;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteChoices;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteRequest;
import ee.translate.keeleleek.mtapplication.view.editors.AlignEditorMediator;
import ee.translate.keeleleek.mtapplication.view.javafx.application.ApplicationMediatorFX;
import ee.translate.keeleleek.mtapplication.view.javafx.helpers.AutocompleteBox;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.control.Pagination;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Path;
import javafx.stage.Popup;
import javafx.stage.Window;
import javafx.util.Callback;


public class AlignEditorMediatorFX extends AlignEditorMediator {

	@FXML
	private GridPane translatesGrid;
	@FXML
	private Pagination pagesBox;
	@FXML
	private CheckBox followChecker;
	
	private int currentIndex = 0;
	private TextArea[] srcElements = new TextArea[ROW_COUNT];
	private TextArea[] dstElements = new TextArea[ROW_COUNT];

	private Popup popup = new Popup();
	private ListView<AutocompleteBox> popupContent = new ListView<>();
	
	private TextArea initEdit;
	
	
	// INIT
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
		for (int r = 0; r < ROW_COUNT; r++) {
			
			final TextArea srcElement = new TextArea("");
			final TextArea dstElement = new TextArea("");
			
			if (followChecker.isSelected()) srcElement.scrollTopProperty().bindBidirectional(dstElement.scrollTopProperty());
			
			srcElement.setEditable(false);
			
			srcElement.setWrapText(true);
			dstElement.setWrapText(true);
			
			srcElements[r] = srcElement;
			dstElements[r] = dstElement;

			final int i = r;
			
			dstElement.addEventFilter(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
				@Override
				public void handle(KeyEvent event)
				 {
					if (event.isControlDown() && event.getCode() == KeyCode.SPACE) {
						Path caret = findCaret((Parent) event.getSource());
						if (caret == null) return;
						Point2D pos = findScreenLocation(caret);
						pos = pos.add(4.0, 4.0 + caret.getBoundsInLocal().getHeight());
						initEdit = dstElement;
						onAutocomplete(new AutocompleteRequest(MinorityTranslateModel.align().getDstRef(), initEdit.getText(0, initEdit.getCaretPosition()), initEdit.getSelectedText(), pos.getX(), pos.getY()));
						
					}
				 }
			});
			
			dstElement.textProperty().addListener(new InvalidationListener() {
				@Override
				public void invalidated(Observable element)
				 {
					if (feedback) return;
					
					final int j = currentIndex * ROW_COUNT + i;
					
					String text = dstElement.getText();
					
					// update elements
					onUpdateSegment(j, text);
				 }
			});
			
		}
		
		pagesBox.setPageFactory(new Callback<Integer, Node>() {
			@Override
			public Node call(Integer index) {
				
				currentIndex = index;
				
				translatesGrid = new GridPane();

				ColumnConstraints column1 = new ColumnConstraints();
				column1.setPercentWidth(50);
				ColumnConstraints column2 = new ColumnConstraints();
				column2.setPercentWidth(50);
				translatesGrid.getColumnConstraints().addAll(column1, column2);
				
				for (int i = 0; i < ROW_COUNT; i++) {
					
					final TextArea srcElement = srcElements[i];
					final TextArea dstElement = dstElements[i];

					translatesGrid.add(srcElement, 0, i);
					translatesGrid.add(dstElement, 1, i);
					
				}
				
				fetchSegments();
				
				if (!doSizing()) Platform.runLater(new Runnable() {
					@Override
					public void run() {
						doSizing();
					}
				});
				
				return translatesGrid;
			}
		});
	 }
	
	@FXML
	private void initialize()
	 {
		// popup
		popup.getScene().getStylesheets().add(getClass().getResource(ApplicationMediatorFX.findFontSizeCSS(MinorityTranslateModel.preferences().getFontSize())).toExternalForm());
		
		popupContent.addEventFilter(MouseEvent.MOUSE_CLICKED ,new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event)
			 {
				if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
					autocomplete(popupContent.getSelectionModel().getSelectedItem().getChoise());
					event.consume();
					popup.hide();
				}
			 }
		});
		
		popupContent.addEventFilter(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event)
			 {
				switch (event.getCode()) {
				case ENTER:
					autocomplete(popupContent.getSelectionModel().getSelectedItem().getChoise());
				case ESCAPE:
					event.consume();
					popup.hide();	
					break;

				default:
					break;
				}
			 }
		});
		
		popup.getContent().addAll(popupContent);
		popup.setAutoHide(true);
	 }
	
	// INHERIT
	@Override
	protected void setPageCount(int pages) {
		pagesBox.setPageCount(pages);
	}
	
	@Override
	protected void fetchSegments()
	 {
		feedback = true;
		
		for (int i = 0; i < ROW_COUNT; i++) {
			
			TextArea srcElement = srcElements[i];
			TextArea dstElement = dstElements[i];

			int j = currentIndex * ROW_COUNT + i;
			
			String srcText = MinorityTranslateModel.align().getSourceSegment(j);
			String dstText = MinorityTranslateModel.align().getSegment(j);

			if (!srcElement.getText().equals(srcText)) srcElement.setText(srcText);
			if (!dstElement.getText().equals(dstText)) dstElement.setText(dstText);
			
			if (j == MinorityTranslateModel.align().getLastIndex()) dstElement.requestFocus();

		}
		
		feedback = false;
	 }
	
	private boolean doSizing()
	 {
		if (translatesGrid == null) return false;
		
//		translatesGrid.getRowConstraints().clear();
//		
//		while (translatesGrid.getRowConstraints().size() < ROW_COUNT) {
//			RowConstraints row = new RowConstraints();
//			row.setMinHeight(46);
//			row.setVgrow(Priority.ALWAYS);
//			translatesGrid.getRowConstraints().add(row);
//		}
//		
//		for (int k = 0; k < ROW_COUNT; k++) {
//			
//			Text text = (Text)srcElements[k].lookup(".text");
//			if (text == null) return false;
//			double height = text.getBoundsInLocal().getHeight();
//			if (height > 182) height = 182;
//			
//			srcElements[k].setPrefHeight(height);
//			
//		}
		
		return true;
	 }
	
	@Override
	public void showAutocomplete(AutocompleteChoices autocomplete)
	 {
		// create list
		popupContent.getItems().clear();
		popupContent.setPrefHeight(200);
		List<AutocompleteChoice> choises = autocomplete.getChoises();
		
		// insert automatically
		if (MinorityTranslateModel.preferences().isInsertAuto() && choises.size() == 1 && !popup.isShowing()) {
			autocomplete(choises.get(0));
			return;
		}
		
		for (AutocompleteChoice choise : choises) {
			popupContent.getItems().add(new AutocompleteBox(choise));
		}
		
		//hide
		if (popupContent.getItems().size() == 0) {
			popup.hide();
			return;
		}

		// show
		if (!popup.isShowing()) {
			popup.setX(autocomplete.getX());
			popup.setY(autocomplete.getY());
			popup.show(translatesGrid.getScene().getWindow());
			popup.requestFocus();
		}
	 }
	
	@Override
	public void autocomplete(AutocompleteChoice choise)
	 {
		if (initEdit == null) return;
		TextArea textEdit = initEdit;
		int textOffset = choise.getTextBackset();
		for (int i = 0; i < textOffset; i++) textEdit.selectBackward();
		textEdit.replaceSelection(choise.getText());
		int caretOffset = choise.getCaretBackset();
		if (caretOffset != 0) textEdit.positionCaret(textEdit.getCaretPosition() - caretOffset);
	 }
	
	// EVENTS
	@FXML
	public void onFollowCheck()
	 {
		for (int i = 0; i < ROW_COUNT; i++) {
			if (followChecker.isSelected()) srcElements[i].scrollTopProperty().bindBidirectional(dstElements[i].scrollTopProperty());
			else srcElements[i].scrollTopProperty().unbindBidirectional(dstElements[i].scrollTopProperty());
		}
	 }
	
	@FXML
	public void onOkClick() {
		super.onOk();
	}
	
	@FXML
	public void onCancelClick() {
		super.onCancel();
	}
	
	
	// HELPERS
	private Path findCaret(Parent parent)
	 {
		// TODO: resolve hack
		for (Node n : parent.getChildrenUnmodifiable()) {
			if (n instanceof Path) {
				return (Path) n;
			}
			else if (n instanceof Parent) {
				Path p = findCaret((Parent) n);
				if (p != null) {
					return p;
				}
			}
		}
		return null;
	 }
	
	private Point2D findScreenLocation(Node node)
	 {
		double x = 0;
		double y = 0;
		for (Node n = node; n != null; n = n.getParent()) {
			Bounds parentBounds = n.getBoundsInParent();
			x += parentBounds.getMinX();
			y += parentBounds.getMinY();
		}
		Scene scene = node.getScene();
		x += scene.getX();
		y += scene.getY();
		Window window = scene.getWindow();
		x += window.getX();
		y += window.getY();
		Point2D screenLoc = new Point2D(x, y);
		return screenLoc;
	 }
	
	
}
