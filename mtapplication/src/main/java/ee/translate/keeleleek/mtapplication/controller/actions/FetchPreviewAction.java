package ee.translate.keeleleek.mtapplication.controller.actions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringEscapeUtils;

import net.sourceforge.jwbf.core.actions.Post;
import net.sourceforge.jwbf.core.actions.util.HttpAction;
import net.sourceforge.jwbf.mediawiki.ApiRequestBuilder;
import net.sourceforge.jwbf.mediawiki.actions.util.MWAction;

public class FetchPreviewAction extends MWAction {

	private HttpAction action;
	
	private String text;
	
	private String html = null;
	
	
	public FetchPreviewAction(String text)
	 {
		this.text = text;
		action = getAction();
	 }
	
	public HttpAction getAction()
	 {
		Post action = new ApiRequestBuilder()
			.action("parse")
			.postParam("text", text)
			.postParam("format", "xml")
			.postParam("disabletoc", "true")
			.postParam("disablepp", "true")
			.postParam("preview", "true")
			.postParam("contentmodel", "wikitext")
			.postParam("disableeditsection", "true")
			.buildPost();
			
		return action;
	 }
	
	@Override
	public HttpAction getNextMessage() {
		HttpAction action = this.action;
		this.action = null;
		return action;
	}

	@Override
	public boolean hasMoreMessages() {
		return action != null;
	}

	@Override
	public String processReturningText(String response, HttpAction action)
	 {
		Pattern pattern = Pattern.compile("<text.*?>(.*?)</text>", Pattern.DOTALL | Pattern.MULTILINE);
	    Matcher matcher = pattern.matcher(response);
	    if (!matcher.find()) return super.processReturningText(response, action);
	    
	    html = matcher.group(1);
	    html = StringEscapeUtils.unescapeHtml3(html);
	    html = "<html><body><div id=\"content\" class=\"mw-body\"><div id=\"bodyContent\" class=\"mw-body-content\">" + html.replace("//upload.wikimedia.org", "https://upload.wikimedia.org") + "</div></div></body></html>";
	    
		return super.processReturningText(response, action);
	 }
	
	/**
	 * Gets the parsed text.
	 * 
	 * @return parsed text
	 */
	public String getParsedText() {
		return html;
	}
	
}
