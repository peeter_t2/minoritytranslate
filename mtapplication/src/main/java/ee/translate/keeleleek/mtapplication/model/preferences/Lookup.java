package ee.translate.keeleleek.mtapplication.model.preferences;


public class Lookup {

	private String name;
	private String url;
	private String parameters;
	private String srcElement;
	private String opening;
	private String dstElement;
	private String closing;
	

	public Lookup(String name, String url, String parameters, String srcElement, String opening, String dstElement, String closing) {
		super();
		this.name = name;
		this.url = url;
		this.parameters = parameters;
		this.srcElement = srcElement;
		this.opening = opening;
		this.dstElement = dstElement;
		this.closing = closing;
	}


	public Lookup(Lookup other) {
		this.name = other.name;
		this.url = other.url;
		this.parameters = other.parameters;
		this.srcElement = other.srcElement;
		this.opening = other.opening;
		this.dstElement = other.dstElement;
		this.closing = other.closing;
	}

	
	
	public String getName() {
		return name;
	}

	public String getURL() {
		return url;
	}

	public String getParameters() {
		return parameters;
	}

	public String getSrcElement() {
		return srcElement;
	}
	
	public String getOpening() {
		return opening;
	}
	
	public String getDstElement() {
		return dstElement;
	}
	
	public String getClosing() {
		return closing;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((closing == null) ? 0 : closing.hashCode());
		result = prime * result
				+ ((dstElement == null) ? 0 : dstElement.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((opening == null) ? 0 : opening.hashCode());
		result = prime * result
				+ ((parameters == null) ? 0 : parameters.hashCode());
		result = prime * result
				+ ((srcElement == null) ? 0 : srcElement.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lookup other = (Lookup) obj;
		if (closing == null) {
			if (other.closing != null)
				return false;
		} else if (!closing.equals(other.closing))
			return false;
		if (dstElement == null) {
			if (other.dstElement != null)
				return false;
		} else if (!dstElement.equals(other.dstElement))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (opening == null) {
			if (other.opening != null)
				return false;
		} else if (!opening.equals(other.opening))
			return false;
		if (parameters == null) {
			if (other.parameters != null)
				return false;
		} else if (!parameters.equals(other.parameters))
			return false;
		if (srcElement == null) {
			if (other.srcElement != null)
				return false;
		} else if (!srcElement.equals(other.srcElement))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}


	
}
