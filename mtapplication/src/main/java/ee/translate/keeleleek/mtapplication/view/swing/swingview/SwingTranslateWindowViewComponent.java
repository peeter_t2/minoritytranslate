package ee.translate.keeleleek.mtapplication.view.swing.swingview;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtapplication.view.windows.TranslateWindowMediator;

public class SwingTranslateWindowViewComponent extends JPanel {

	private static final long serialVersionUID = 1L;
	
	public TranslateWindowMediator mediator;
	public JMenuBar menuBar;
	public JMenu mnProgram;
	public JSeparator separator;
	public JMenuItem loginMenuItem;
	public JSeparator separator_1;
	public JSeparator separator_2;
	public JMenu mnArticles;
	public JMenuItem uploadMenuItem;
	public JMenuItem addCategoryMenuItem;
	public JMenuItem removeArticleMenuItem;
	public JMenu mnNavigation;
	public JMenu mnInfo;
	public JMenuItem nextArticleMenuItem;
	public JMenuItem previousArticleMenuItem;
	public JSeparator separator_4;
	public JMenuItem addArticleMenuItem;
	public JPanel panel;
	public JToggleButton uploadButton;
	public JButton btnPrevious;
	public JButton btnNext;
	public JPanel statusBar;
	public JLabel loginStatusLabel;
	private JSeparator separator_6;
	private JSeparator separator_7;
	private JSeparator separator_8;
	private JMenuItem mntmRemoveCurrent;
	public JMenuItem listsMenuItem;
	public JMenuItem logoutMenuItem;
	public JMenuItem addLinksMenuItem;
	private JSeparator separator_10;
	private JMenuItem stopDownloadingMenuItem;
	public JSplitPane addonsSplitPane;
	public JSplitPane translateSplitPane;
	public JTabbedPane upperTabPane;
	public JTabbedPane lowerTabPane;
	private JTabbedPane tabbedPane;
	public JTextArea notesEdit;
	private JToolBar controlsToolbar;
	private JSeparator separator_5;
	public JCheckBoxMenuItem uploadEnabledMenuItem;
	private JScrollPane scrollPane;
	public JList<JLabel> articleList;
	private Component horizontalStrut;
	public JProgressBar quererProgress;
	public JLabel quererProgressLabel;
	public JPanel queingPanel;
	public JPanel downloadingPanel;
	public JProgressBar downloadingProgress;
	public JLabel downloadingProgressLabel;
	public JPanel previewingPanel;
	public JLabel label;
	public JProgressBar progressBar;
	public JPanel uploadingPanel;
	public JProgressBar uploadingProgress;
	public JLabel uploadingProgressLabel;
	private JPanel statusPanel;
	public JPanel wikisPanel;
	private JSeparator separator_3;
	public JButton pullButton;
	private JSeparator separator_9;
	public JMenuItem toggleExactMenuItem;
	public JPanel snippetsPage;
	private JSeparator separator_11;
	private JButton tagButton;
	public JLabel tagLabel;
	public JLabel countLabel;

	
	/**
	 * Create the panel.
	 */
	public SwingTranslateWindowViewComponent() {
		setPreferredSize(new Dimension(900, 600));
		setLayout(new BorderLayout(0, 0));
		
		menuBar = new JMenuBar();
		menuBar.setBackground(UIManager.getColor("ToolBar.background"));
		add(menuBar, BorderLayout.NORTH);
		
		mnProgram = new JMenu(Messages.getString("menu.program")); //$NON-NLS-1$
		menuBar.add(mnProgram);
		
		JMenuItem mntmNewSession = new JMenuItem(Messages.getString("menu.program.new.session")); //$NON-NLS-1$
		mntmNewSession.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
		mntmNewSession.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onMenuSessionNewClick();
			}
		});
		mnProgram.add(mntmNewSession);
		
		JMenuItem mntmOpenSession = new JMenuItem(Messages.getString("menu.program.open.session")); //$NON-NLS-1$
		mntmOpenSession.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
		mntmOpenSession.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onMenuSessionOpenClick();
			}
		});
		mnProgram.add(mntmOpenSession);
		
		JMenuItem mntmSaveSession = new JMenuItem(Messages.getString("menu.program.save.session")); //$NON-NLS-1$
		mntmSaveSession.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
		mntmSaveSession.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onMenuSessionSaveClick();
			}
		});
		mnProgram.add(mntmSaveSession);
		
		JMenuItem mntmSaveSessionAs = new JMenuItem(Messages.getString("menu.program.save.session.as")); //$NON-NLS-1$
		mntmSaveSessionAs.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		mntmSaveSessionAs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onMenuSessionSaveAsClick();
			}
		});
		mnProgram.add(mntmSaveSessionAs);
		
		separator = new JSeparator();
		mnProgram.add(separator);
		
		JMenuItem mntmPeferences = new JMenuItem(Messages.getString("menu.program.preferences")); //$NON-NLS-1$
		mntmPeferences.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_MASK));
		mntmPeferences.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mediator.onMenuPreferencesClick();
			}
		});
		mnProgram.add(mntmPeferences);
		
		separator_1 = new JSeparator();
		mnProgram.add(separator_1);
		
		loginMenuItem = new JMenuItem(Messages.getString("menu.program.login")); //$NON-NLS-1$
		loginMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.CTRL_MASK));
		loginMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onMenuLoginClick();
			}
		});
		mnProgram.add(loginMenuItem);
		
		logoutMenuItem = new JMenuItem(Messages.getString("menu.program.logout")); //$NON-NLS-1$
		logoutMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_MASK));
		logoutMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onMenuLogoutClick();
			}
		});
		mnProgram.add(logoutMenuItem);
		
		separator_2 = new JSeparator();
		mnProgram.add(separator_2);
		
		JMenuItem mntmQuit = new JMenuItem(Messages.getString("menu.program.quit")); //$NON-NLS-1$
		mntmQuit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_MASK));
		mntmQuit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onMenuQuitClick();
			}
		});
		mnProgram.add(mntmQuit);
		
		mnNavigation = new JMenu(Messages.getString("menu.navigation")); //$NON-NLS-1$
		menuBar.add(mnNavigation);
		
		nextArticleMenuItem = new JMenuItem(Messages.getString("menu.navigation.next")); //$NON-NLS-1$
		nextArticleMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, InputEvent.ALT_MASK));
		nextArticleMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onMenuNextArticleClick();
			}
		});
		mnNavigation.add(nextArticleMenuItem);
		
		previousArticleMenuItem = new JMenuItem(Messages.getString("menu.navigation.previous")); //$NON-NLS-1$
		previousArticleMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, InputEvent.ALT_MASK));
		previousArticleMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onMenuPreviousArticleClick();
			}
		});
		mnNavigation.add(previousArticleMenuItem);
		
		separator_4 = new JSeparator();
		mnNavigation.add(separator_4);
		
		JMenuItem menuTogglePreview = new JMenuItem(Messages.getString("menu.navigation.toggle.preview")); //$NON-NLS-1$
		menuTogglePreview.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.ALT_MASK));
		menuTogglePreview.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onMenuTogglePreviewClick();;
			}
		});
		mnNavigation.add(menuTogglePreview);
		
		separator_9 = new JSeparator();
		mnNavigation.add(separator_9);
		
		toggleExactMenuItem = new JMenuItem(Messages.getString("menu.navigation.toggle.exact")); //$NON-NLS-1$
		toggleExactMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onMenuToggleExactClick();
			}
		});
		toggleExactMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.ALT_MASK));
		mnNavigation.add(toggleExactMenuItem);
		
		mnArticles = new JMenu(Messages.getString("menu.articles")); //$NON-NLS-1$
		menuBar.add(mnArticles);
		
		uploadMenuItem = new JMenuItem(Messages.getString("menu.articles.upload")); //$NON-NLS-1$
		uploadMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		uploadMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onMenuUploadClick();
			}
		});
		
		uploadEnabledMenuItem = new JCheckBoxMenuItem(Messages.getString("menu.articles.upload.enabled")); //$NON-NLS-1$
		uploadEnabledMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onUploadEnable(uploadEnabledMenuItem.isSelected());
			}
		});
		uploadEnabledMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		mnArticles.add(uploadEnabledMenuItem);
		mnArticles.add(uploadMenuItem);
		
		addArticleMenuItem = new JMenuItem(Messages.getString("menu.articles.add.article")); //$NON-NLS-1$
		addArticleMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		addArticleMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onAddArticle();
			}
		});
		
		separator_7 = new JSeparator();
		mnArticles.add(separator_7);
		mnArticles.add(addArticleMenuItem);
		
		addCategoryMenuItem = new JMenuItem(Messages.getString("menu.articles.add.category")); //$NON-NLS-1$
		addCategoryMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		addCategoryMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onAddCategory();
			}
		});
		mnArticles.add(addCategoryMenuItem);
		
		removeArticleMenuItem = new JMenuItem(Messages.getString("menu.articles.remove.article")); //$NON-NLS-1$
		removeArticleMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		removeArticleMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onMenuRemoveArticleClick();
			}
		});
		
		listsMenuItem = new JMenuItem(Messages.getString("menu.articles.suggestion.lists")); //$NON-NLS-1$
		listsMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		listsMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mediator.onMenuListsClick();
			}
		});
		
		addLinksMenuItem = new JMenuItem(Messages.getString("menu.articles.add.links")); //$NON-NLS-1$
		addLinksMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		addLinksMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onMenuAddLinksClick();
			}
		});
		mnArticles.add(addLinksMenuItem);
		mnArticles.add(listsMenuItem);
		
		separator_8 = new JSeparator();
		mnArticles.add(separator_8);
		
		mntmRemoveCurrent = new JMenuItem(Messages.getString("menu.articles.remove.selected")); //$NON-NLS-1$
		mntmRemoveCurrent.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		mntmRemoveCurrent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mediator.onMenuRemoveSelectedArticleClick();
			}
		});
		
		stopDownloadingMenuItem = new JMenuItem(Messages.getString("menu.articles.stop.downloading")); //$NON-NLS-1$
		stopDownloadingMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onMenuStopDownloadingClick();
			}
		});
		stopDownloadingMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		mnArticles.add(stopDownloadingMenuItem);
		
		separator_10 = new JSeparator();
		mnArticles.add(separator_10);
		mnArticles.add(mntmRemoveCurrent);
		mnArticles.add(removeArticleMenuItem);
		
		mnInfo = new JMenu(Messages.getString("menu.info")); //$NON-NLS-1$
		menuBar.add(mnInfo);
		
		JMenuItem mntmAbout = new JMenuItem(Messages.getString("menu.info.about")); //$NON-NLS-1$
		mntmAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onMenuAboutClick();
			}
		});
		mnInfo.add(mntmAbout);
		
		JMenuItem mntmManual = new JMenuItem(Messages.getString("menu.info.manual")); //$NON-NLS-1$
		mntmManual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onMenuManualClick();
			}
		});
		mnInfo.add(mntmManual);
		
		panel = new JPanel();
		add(panel, BorderLayout.CENTER);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{380, 0};
		gbl_panel.rowHeights = new int[]{0, 59, 0, 0};
		gbl_panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		controlsToolbar = new JToolBar();
		controlsToolbar.setFloatable(false);
		GridBagConstraints gbc_controlsToolbar = new GridBagConstraints();
		gbc_controlsToolbar.fill = GridBagConstraints.HORIZONTAL;
		gbc_controlsToolbar.insets = new Insets(0, 0, 5, 0);
		gbc_controlsToolbar.gridx = 0;
		gbc_controlsToolbar.gridy = 0;
		panel.add(controlsToolbar, gbc_controlsToolbar);
		
		btnPrevious = new JButton(Messages.getString("toolbar.previous.button")); //$NON-NLS-1$
		btnPrevious.setIcon(new ImageIcon(SwingTranslateWindowViewComponent.class.getResource("/icons/MT_previous.png")));
		controlsToolbar.add(btnPrevious);
		
		btnNext = new JButton(Messages.getString("toolbar.next.button")); //$NON-NLS-1$
		btnNext.setIcon(new ImageIcon(SwingTranslateWindowViewComponent.class.getResource("/icons/MT_next.png")));
		controlsToolbar.add(btnNext);
		
		separator_5 = new JSeparator();
		separator_5.setMaximumSize(new Dimension(25, 32767));
		separator_5.setOrientation(SwingConstants.VERTICAL);
		controlsToolbar.add(separator_5);
		
		uploadButton = new JToggleButton(Messages.getString("toolbar.upload.button")); //$NON-NLS-1$
		uploadButton.setIcon(new ImageIcon(SwingTranslateWindowViewComponent.class.getResource("/icons/MT_upload.png")));
		controlsToolbar.add(uploadButton);
		
		separator_3 = new JSeparator();
		separator_3.setMaximumSize(new Dimension(25, 32767));
		separator_3.setOrientation(SwingConstants.VERTICAL);
		controlsToolbar.add(separator_3);
		
		pullButton = new JButton(Messages.getString("toolbar.pull.button")); //$NON-NLS-1$
		pullButton.setIcon(new ImageIcon(SwingTranslateWindowViewComponent.class.getResource("/icons/MT_pull.png")));
		pullButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onPull();
			}
		});
		controlsToolbar.add(pullButton);
		
		separator_11 = new JSeparator();
		separator_11.setMaximumSize(new Dimension(25, 32767));
		separator_11.setOrientation(SwingConstants.VERTICAL);
		controlsToolbar.add(separator_11);
		
		tagButton = new JButton(Messages.getString("toolbar.tag.button")); //$NON-NLS-1$
		tagButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onTag();
			}
		});
		tagButton.setIcon(new ImageIcon(SwingTranslateWindowViewComponent.class.getResource("/icons/MT_tag.png")));
		controlsToolbar.add(tagButton);
		
		horizontalStrut = Box.createHorizontalStrut(20);
		horizontalStrut.setMaximumSize(new Dimension(32767, 32767));
		controlsToolbar.add(horizontalStrut);
		
		loginStatusLabel = new JLabel(Messages.getString("statusbar.no.connection")); //$NON-NLS-1$
		controlsToolbar.add(loginStatusLabel);
		loginStatusLabel.setHorizontalTextPosition(SwingConstants.CENTER);
		loginStatusLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		uploadButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onUpload(uploadButton.isSelected());
			}
		});
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onNextClick();
			}
		});
		btnPrevious.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onPreviousClick();
			}
		});
		
		addonsSplitPane = new JSplitPane();
		GridBagConstraints gbc_addonsSplitPane = new GridBagConstraints();
		gbc_addonsSplitPane.insets = new Insets(0, 0, 5, 0);
		gbc_addonsSplitPane.fill = GridBagConstraints.BOTH;
		gbc_addonsSplitPane.gridx = 0;
		gbc_addonsSplitPane.gridy = 1;
		panel.add(addonsSplitPane, gbc_addonsSplitPane);
		
		translateSplitPane = new JSplitPane();
		translateSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		addonsSplitPane.setRightComponent(translateSplitPane);
		
		upperTabPane = new JTabbedPane(JTabbedPane.TOP);
		translateSplitPane.setLeftComponent(upperTabPane);
		
		lowerTabPane = new JTabbedPane(JTabbedPane.TOP);
		translateSplitPane.setRightComponent(lowerTabPane);
		translateSplitPane.setDividerLocation(0.5);
		translateSplitPane.setDividerLocation(250);
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		addonsSplitPane.setLeftComponent(tabbedPane);
		
		scrollPane = new JScrollPane();
		tabbedPane.addTab(Messages.getString("addons.articles"), null, scrollPane, null); //$NON-NLS-1$
		
		articleList = new JList<JLabel>();
		scrollPane.setViewportView(articleList);
		
		notesEdit = new JTextArea();
		notesEdit.setText("");
		tabbedPane.addTab(Messages.getString("addons.notes"), null, notesEdit, null); //$NON-NLS-1$
		
		snippetsPage = new JPanel();
		tabbedPane.addTab(Messages.getString("addons.snippets"), null, snippetsPage, null);
		snippetsPage.setLayout(new GridLayout(0, 1, 0, 0));
		addonsSplitPane.setDividerLocation(0.25);
		addonsSplitPane.setDividerLocation(200);
		
		separator_6 = new JSeparator();
		GridBagConstraints gbc_separator_6 = new GridBagConstraints();
		gbc_separator_6.fill = GridBagConstraints.HORIZONTAL;
		gbc_separator_6.gridx = 0;
		gbc_separator_6.gridy = 2;
		panel.add(separator_6, gbc_separator_6);
		
		statusBar = new JPanel();
		statusBar.setBorder(new EmptyBorder(0, 7, 0, 7));
		add(statusBar, BorderLayout.SOUTH);
		GridBagLayout gbl_statusBar = new GridBagLayout();
		gbl_statusBar.columnWidths = new int[]{110, 0, 0, 168, 0};
		gbl_statusBar.rowHeights = new int[]{15, 0, 0};
		gbl_statusBar.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_statusBar.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		statusBar.setLayout(gbl_statusBar);

		wikisPanel = new JPanel();
		GridBagConstraints gbc_wikisPanel = new GridBagConstraints();
		gbc_wikisPanel.insets = new Insets(0, 0, 5, 5);
		gbc_wikisPanel.anchor = GridBagConstraints.WEST;
		gbc_wikisPanel.gridx = 0;
		gbc_wikisPanel.gridy = 0;
		statusBar.add(wikisPanel, gbc_wikisPanel);
		wikisPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 0));
		
		tagLabel = new JLabel("");
		GridBagConstraints gbc_tagLabel = new GridBagConstraints();
		gbc_tagLabel.insets = new Insets(0, 0, 5, 5);
		gbc_tagLabel.gridx = 1;
		gbc_tagLabel.gridy = 0;
		statusBar.add(tagLabel, gbc_tagLabel);
		
		countLabel = new JLabel("");
		GridBagConstraints gbc_countLabel = new GridBagConstraints();
		gbc_countLabel.insets = new Insets(0, 0, 5, 5);
		gbc_countLabel.gridx = 2;
		gbc_countLabel.gridy = 0;
		statusBar.add(countLabel, gbc_countLabel);

		statusPanel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) statusPanel.getLayout();
		flowLayout.setVgap(0);
		GridBagConstraints gbc_statusPanel = new GridBagConstraints();
		gbc_statusPanel.insets = new Insets(0, 0, 5, 0);
		gbc_statusPanel.fill = GridBagConstraints.HORIZONTAL;
		gbc_statusPanel.gridx = 3;
		gbc_statusPanel.gridy = 0;
		statusBar.add(statusPanel, gbc_statusPanel);
		
		queingPanel = new JPanel();
		queingPanel.setMaximumSize(new Dimension(32767, 15));
		statusPanel.add(queingPanel);
		GridBagLayout gbl_queingPanel = new GridBagLayout();
		gbl_queingPanel.columnWidths = new int[]{25, 59, 0};
		gbl_queingPanel.rowHeights = new int[]{15, 0};
		gbl_queingPanel.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_queingPanel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		queingPanel.setLayout(gbl_queingPanel);
		
		quererProgress = new JProgressBar();
		GridBagConstraints gbc_quererProgress = new GridBagConstraints();
		gbc_quererProgress.fill = GridBagConstraints.HORIZONTAL;
		gbc_quererProgress.insets = new Insets(0, 0, 0, 5);
		gbc_quererProgress.gridx = 0;
		gbc_quererProgress.gridy = 0;
		queingPanel.add(quererProgress, gbc_quererProgress);
		quererProgress.setPreferredSize(new Dimension(25, 14));
		quererProgress.setMaximumSize(new Dimension(25, 14));
		quererProgress.setIndeterminate(true);
		
		quererProgressLabel = new JLabel(Messages.getString("statusbar.queuing"));
		GridBagConstraints gbc_quererProgressLabel = new GridBagConstraints();
		gbc_quererProgressLabel.anchor = GridBagConstraints.WEST;
		gbc_quererProgressLabel.gridx = 1;
		gbc_quererProgressLabel.gridy = 0;
		queingPanel.add(quererProgressLabel, gbc_quererProgressLabel);
		
		downloadingPanel = new JPanel();
		downloadingPanel.setMaximumSize(new Dimension(32767, 15));
		statusPanel.add(downloadingPanel);
		GridBagLayout gbl_downloadingPanel = new GridBagLayout();
		gbl_downloadingPanel.columnWidths = new int[]{25, 93, 0};
		gbl_downloadingPanel.rowHeights = new int[]{15, 0};
		gbl_downloadingPanel.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_downloadingPanel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		downloadingPanel.setLayout(gbl_downloadingPanel);
		
		downloadingProgress = new JProgressBar();
		downloadingProgress.setPreferredSize(new Dimension(25, 14));
		downloadingProgress.setMaximumSize(new Dimension(25, 14));
		downloadingProgress.setIndeterminate(true);
		GridBagConstraints gbc_downloadingProgress = new GridBagConstraints();
		gbc_downloadingProgress.fill = GridBagConstraints.HORIZONTAL;
		gbc_downloadingProgress.insets = new Insets(0, 0, 0, 5);
		gbc_downloadingProgress.gridx = 0;
		gbc_downloadingProgress.gridy = 0;
		downloadingPanel.add(downloadingProgress, gbc_downloadingProgress);
		
		downloadingProgressLabel = new JLabel(Messages.getString("statusbar.downloading")); //$NON-NLS-1$
		GridBagConstraints gbc_downloadingProgressLabel = new GridBagConstraints();
		gbc_downloadingProgressLabel.anchor = GridBagConstraints.WEST;
		gbc_downloadingProgressLabel.gridx = 1;
		gbc_downloadingProgressLabel.gridy = 0;
		downloadingPanel.add(downloadingProgressLabel, gbc_downloadingProgressLabel);
		
		previewingPanel = new JPanel();
		previewingPanel.setMaximumSize(new Dimension(32767, 15));
		statusPanel.add(previewingPanel);
		GridBagLayout gbl_previewingPanel = new GridBagLayout();
		gbl_previewingPanel.columnWidths = new int[]{10, 93, 0};
		gbl_previewingPanel.rowHeights = new int[]{15, 0};
		gbl_previewingPanel.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_previewingPanel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		previewingPanel.setLayout(gbl_previewingPanel);
		
		progressBar = new JProgressBar();
		progressBar.setPreferredSize(new Dimension(25, 14));
		progressBar.setMaximumSize(new Dimension(25, 14));
		progressBar.setIndeterminate(true);
		GridBagConstraints gbc_progressBar = new GridBagConstraints();
		gbc_progressBar.fill = GridBagConstraints.HORIZONTAL;
		gbc_progressBar.insets = new Insets(0, 0, 0, 5);
		gbc_progressBar.gridx = 0;
		gbc_progressBar.gridy = 0;
		previewingPanel.add(progressBar, gbc_progressBar);
		
		label = new JLabel(Messages.getString("statusbar.previewing")); //$NON-NLS-1$
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.anchor = GridBagConstraints.WEST;
		gbc_label.gridx = 1;
		gbc_label.gridy = 0;
		previewingPanel.add(label, gbc_label);
		
		uploadingPanel = new JPanel();
		uploadingPanel.setMaximumSize(new Dimension(32767, 15));
		statusPanel.add(uploadingPanel);
		GridBagLayout gbl_uploadingPanel = new GridBagLayout();
		gbl_uploadingPanel.columnWidths = new int[]{10, 93, 0};
		gbl_uploadingPanel.rowHeights = new int[]{15, 0};
		gbl_uploadingPanel.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_uploadingPanel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		uploadingPanel.setLayout(gbl_uploadingPanel);
		
		uploadingProgress = new JProgressBar();
		uploadingProgress.setPreferredSize(new Dimension(25, 14));
		uploadingProgress.setMaximumSize(new Dimension(25, 14));
		uploadingProgress.setIndeterminate(true);
		GridBagConstraints gbc_uploadingProgress = new GridBagConstraints();
		gbc_uploadingProgress.fill = GridBagConstraints.HORIZONTAL;
		gbc_uploadingProgress.insets = new Insets(0, 0, 0, 5);
		gbc_uploadingProgress.gridx = 0;
		gbc_uploadingProgress.gridy = 0;
		uploadingPanel.add(uploadingProgress, gbc_uploadingProgress);
		
		uploadingProgressLabel = new JLabel(Messages.getString("statusbar.uploading")); //$NON-NLS-1$
		GridBagConstraints gbc_uploadingProgressLabel = new GridBagConstraints();
		gbc_uploadingProgressLabel.anchor = GridBagConstraints.WEST;
		gbc_uploadingProgressLabel.gridx = 1;
		gbc_uploadingProgressLabel.gridy = 0;
		uploadingPanel.add(uploadingProgressLabel, gbc_uploadingProgressLabel);
		
	}
}
