package ee.translate.keeleleek.mtapplication.model.autocomplete;

import ee.translate.keeleleek.mtapplication.model.content.Reference;

public class AutocompleteRequest {

	private Reference ref;
	private String preceeding;
	private String selected;
	private double x;
	private double y;
	
	
	public AutocompleteRequest(Reference ref, String preceeding, String selected, double x, double y) {
		super();
		this.ref = ref;
		this.preceeding = preceeding;
		this.selected = selected;
		this.x = x;
		this.y = y;
	}
	

	public Reference getRef() {
		return ref;
	}

	public String getPreceeding() {
		return preceeding;
	}

	public String getSelected() {
		return selected;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
	
	
}
