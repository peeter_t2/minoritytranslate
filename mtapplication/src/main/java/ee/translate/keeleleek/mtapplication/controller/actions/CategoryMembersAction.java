package ee.translate.keeleleek.mtapplication.controller.actions;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sourceforge.jwbf.core.actions.RequestBuilder;
import net.sourceforge.jwbf.core.actions.util.HttpAction;
import net.sourceforge.jwbf.mediawiki.MediaWiki;
import net.sourceforge.jwbf.mediawiki.actions.util.MWAction;

public class CategoryMembersAction extends MWAction {
	
	public static final int LIMIT = 50;
	
	private String cmcontinue;
	private String title;
	private List<String> articles = new ArrayList<>();
	private List<String> categories = new ArrayList<>();
	
	private HttpAction action;
	
	
	public CategoryMembersAction(String title, String cmcontinue)
	 {
		int i = title.indexOf(':');
		if (i != -1) title = title.substring(i + 1);
		
		this.title = title;
		this.cmcontinue = cmcontinue;
		
		action = getAction();
	 }
	
	public CategoryMembersAction(String title) {
		this(title, null);
	}
	
	
	public HttpAction getAction()
	 {
		RequestBuilder builder = new RequestBuilder(MediaWiki.URL_API)
			.param("action", "query")
			.param("list", "categorymembers")
			.param("cmtitle", "Category:" + MediaWiki.urlEncode(title))
			.param("cmprop", "title")
			.param("cmlimit", LIMIT)
			.param("format", "xml");
		
		if (cmcontinue != null)  builder.param("cmcontinue", MediaWiki.urlEncode(cmcontinue));
		
		return builder.buildGet();
	 }
	
	@Override
	public HttpAction getNextMessage() {
		HttpAction action = this.action;
		this.action = null;
		return action;
	}

	@Override
	public boolean hasMoreMessages() {
		return action != null;
	}

	@Override
	public String processReturningText(String response, HttpAction action)
	 {
//		System.out.println("RESPONSE=" + response);
		
		Pattern p3 = Pattern.compile("cmcontinue=\"(.*?)\"");
		Matcher m3 = p3.matcher(response);
		if (m3.find()) cmcontinue = m3.group(1);
		else cmcontinue = null;
		
		Pattern p0 = Pattern.compile("<cm (.*?) />");
		Matcher m0 = p0.matcher(response);

		while (m0.find()) {
			String member = m0.group(1);

			Pattern p1 = Pattern.compile("title=\"(.*?)\"");
			Matcher m1 = p1.matcher(member);
			
			Pattern p2 = Pattern.compile("ns=\"(.*?)\"");
			Matcher m2 = p2.matcher(member);
			
			m2.find();
			String ns = m2.group(1);
			
			m1.find();
			String title = m1.group(1);
			
			int i = title.lastIndexOf(':');
			if (i != -1) title = title.substring(i + 1);
			
			if (ns.equals("0")) articles.add(title);
			else if (ns.equals("14")) categories.add(title);
		}
		
		return super.processReturningText(response, action);
	 }
	

	public List<String> getArticles() {
		return articles;
	}
	
	public List<String> getCategories() {
		return categories;
	}
	
	
	public CategoryMembersAction next()
	 {
		if (cmcontinue != null) {
			CategoryMembersAction categorymembers = new CategoryMembersAction(title, cmcontinue);
			categorymembers.categories = this.categories;
			categorymembers.articles = this.articles;
			return categorymembers;
		}
		
//		if (categories.size() > 0) {
//			CategoryMembersAction categorymembers = new CategoryMembersAction(categories.remove(categories.size() - 1), cmcontinue);
//			categorymembers.categories = this.categories;
//			categorymembers.articles = this.articles;
//			return categorymembers;
//		}
		
		return null;
	 }
	
}
