package ee.translate.keeleleek.mtapplication.model.editors;

import java.util.ArrayList;

import org.puremvc.java.multicore.patterns.proxy.Proxy;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle;
import ee.translate.keeleleek.mtapplication.model.content.Reference;

public class AlignEditorProxy extends Proxy {

	public final static String NAME = "{6B358226-5653-4CC2-A9AE-6B49E6C512EE}";
	public final static String REGEX = "\n\n|(?<===)\n";
	
	private ArrayList<String> srcSegments = new ArrayList<>();
	private ArrayList<String> dstSegments = new ArrayList<>();

	private long dstOrigHash = dstSegments.hashCode();
	
	private Reference dstRef = null;
	private int lastIndex = -1;
	
	
	// INIT
	public AlignEditorProxy() {
		super(NAME, null);
	}

	
	// EDITOR
	public void open(MinorityArticle srcArticle, MinorityArticle dstArticle)
	 {
		dstRef = dstArticle.getRef();
		
		alignText(srcArticle.getFilteredText().split(REGEX), dstArticle.getText().split(REGEX));
		
		dstOrigHash = dstSegments.hashCode();
		
		sendNotification(Notifications.ALIGN_EDITOR_OPENED);
	 }
	
	public void close()
	 {
		dstRef = null;
		
		srcSegments.clear();
		dstSegments.clear();
		
		dstRef = null;
		lastIndex = -1;
		
		sendNotification(Notifications.ALIGN_EDITOR_CLOSED);
	 }

	public void apply()
	 {
		MinorityTranslateModel.content().changeText(dstRef, compileText());
		
		sendNotification(Notifications.ALIGN_EDITOR_APPLIED);
	 }

	

	// SEGMENTS
	public int getSegmentCount() {
		return srcSegments.size();
	}
	
	public String getSegment(int i)
	 {
		if (i >= dstSegments.size()) return "";
		return dstSegments.get(i);
	 }

	public String getSourceSegment(int i)
	 {
		if (i >= srcSegments.size()) return "";
		return srcSegments.get(i);
	 }

	public void updateSegment(int i, String segment)
	 {
		if (!segment.isEmpty()) {
			String[] split = segment.split(REGEX);
			if (split.length == 1) {
				setSegment(i, segment);
			}
			else {
				setAddSegment(i, split);
				sendNotification(Notifications.ALIGN_EDITOR_SEGMENTS_MOVED);
			}
		} else {
			removeSegment(i);
			sendNotification(Notifications.ALIGN_EDITOR_SEGMENTS_MOVED);
		}
		
		sendNotification(Notifications.ALIGN_EDITOR_SEGMENTS_CHANGED);
	 }
	
	
	public Reference getDstRef() {
		return dstRef;
	}
	
	public int getLastIndex() {
		return lastIndex;
	}
	
	
	// SEGMENT HELPERS
	private void setSegment(int i, String segment)
	 {
		while (dstSegments.size() <= i) {
			dstSegments.add("");
		}

		while (segment.startsWith("\n")) segment = segment.substring(1);
		while (segment.endsWith("\n")) segment = segment.substring(0, segment.length() - 1);
		
		dstSegments.set(i, segment);
		
		lastIndex = i;
	 }

	private void setAddSegment(int i, String[] segments)
	 {
		if (segments.length == 0) return;
		
		while (dstSegments.size() <= i) {
			dstSegments.add("");
		}
		
		for (int j = 0; j < segments.length; j++) {
			
			String segment = segments[j];
			
			while (segment.startsWith("\n")) segment = segment.substring(1);
			while (segment.endsWith("\n")) segment = segment.substring(0, segment.length() - 1);
			
			if (j == 0) dstSegments.set(i + j, segment);
			else dstSegments.add(i + j, segment);
			
			lastIndex = i + j;
		}
		
		equalise();
	 }

	private void removeSegment(int i)
	 {
		if (i >= dstSegments.size()) return;
		dstSegments.remove(i);

		lastIndex = i;
		
		equalise();
	 }
	
	
	// ALIGN
	private void alignText(String[] srcText, String[] dstText)
	 {
		srcSegments.clear();
		dstSegments.clear();
		
		int length = srcText.length > dstText.length ? srcText.length : dstText.length;
		
		for (int i = 0; i < length; i++) {
			
			if (i < srcText.length) srcSegments.add(srcText[i]);
			else srcSegments.add("");
			if (i < dstText.length) dstSegments.add(dstText[i]);
			else dstSegments.add("");
			
		}
		
		equalise();
	 }

	public String compileText()
	 {
		StringBuilder result = new StringBuilder();
		
		boolean emptyLine = true;
		
		for (String segment : dstSegments) {

			if (result.length() != 0) {
				if (emptyLine) result.append("\n\n");
				else result.append("\n");
			}
			
			result.append(segment);
			
			if (segment.startsWith("==") && segment.endsWith("==")) emptyLine = false;
			else emptyLine = true;
			
		}
		
		String text = result.toString();
		
		while (text.endsWith("\n")) text = text.substring(0, text.length() - 1);
		
		return text;
	 }
	
	private void equalise()
	 {
		int srcLength = srcSegments.size();
		while (srcLength > 0 && srcSegments.get(srcLength - 1).isEmpty()) {
			srcLength--;
		}
		
		int dstLength = dstSegments.size();
		while (dstLength > 0 && dstSegments.get(dstLength - 1).isEmpty()) {
			dstLength--;
		}
		
		int length = srcLength > dstLength ? srcLength : dstLength;

		while (srcSegments.size() < length) {
			srcSegments.add("");
		}

		while (srcSegments.size() > length) {
			srcSegments.remove(srcSegments.size() - 1);
		}
		
		while (dstSegments.size() < length) {
			dstSegments.add("");
		}

		while (dstSegments.size() > length) {
			dstSegments.remove(dstSegments.size() - 1);
		}
	 }
	
	
	// CONTENT
	public boolean isOpen()
	 {
		return dstRef != null;
	 }
	
	public boolean isSaved()
	 {
		return !isOpen() || dstOrigHash == dstSegments.hashCode();
	 }
	
	public void setSaved()
	 {
		dstOrigHash = dstSegments.hashCode();
	 }
	
	
}
