package ee.translate.keeleleek.mtapplication.controller.editors;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;

public class CloseAlignCommand extends SimpleCommand {
	
	@Override
	public void execute(INotification notification)
	 {
		Boolean apply = (Boolean) notification.getBody();
		
		if (apply) MinorityTranslateModel.align().apply();
		
		MinorityTranslateModel.align().close();
	 }
	
}
