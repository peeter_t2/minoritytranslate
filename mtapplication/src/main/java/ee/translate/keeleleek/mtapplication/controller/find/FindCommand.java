package ee.translate.keeleleek.mtapplication.controller.find;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.requests.FindRequest;

public class FindCommand extends SimpleCommand {

	
	@Override
	public void execute(INotification notification)
	 {
		FindRequest request = (FindRequest) notification.getBody();
		
		sendNotification(Notifications.FIND_FROM_ACTIVE_TAB, request);
	 }
	
}
