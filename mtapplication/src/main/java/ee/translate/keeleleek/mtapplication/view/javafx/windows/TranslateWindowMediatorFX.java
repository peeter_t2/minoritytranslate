package ee.translate.keeleleek.mtapplication.view.javafx.windows;

import java.util.ArrayList;
import java.util.List;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.view.elements.TranslateTabMediator;
import ee.translate.keeleleek.mtapplication.view.javafx.MediatorLoaderFX;
import ee.translate.keeleleek.mtapplication.view.javafx.elements.ArticlesMediatorFX;
import ee.translate.keeleleek.mtapplication.view.javafx.elements.NotesMediatorFX;
import ee.translate.keeleleek.mtapplication.view.pages.FindAddonPageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.LookupAddonPageMediator;
import ee.translate.keeleleek.mtapplication.view.windows.TranslateWindowMediator;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class TranslateWindowMediatorFX extends TranslateWindowMediator {

	@FXML
	protected SplitPane addonsSplitPane;

	@FXML
	protected MenuItem loginMenuItem;
	@FXML
	protected MenuItem logoutMenuItem;
	@FXML
	protected MenuItem nextArticleMenuItem;
	@FXML
	protected MenuItem previousArticleMenuItem;
	@FXML
	protected MenuItem menuExactToggleItem;
	@FXML
	protected CheckMenuItem uploadEnabledMenuItem;
	@FXML
	protected MenuItem uploadMenuItem;
	@FXML
	protected MenuItem addArticleMenuItem;
	@FXML
	protected MenuItem addCategoryMenuItem;
	@FXML
	protected MenuItem addLinksMenuItem;
	@FXML
	protected MenuItem listsMenuItem;
	@FXML
	protected CheckMenuItem filterTemplatesMenuItem;
	@FXML
	protected CheckMenuItem filterFilesMenuItem;
	@FXML
	protected CheckMenuItem filterIntroductionMenuItem;
	@FXML
	protected CheckMenuItem filterReferencesMenuItem;

	@FXML
	protected StackPane editorsPane;
	@FXML
	protected SplitPane translateEditorPage;
	@FXML
	protected Parent alignEditorPage;
	
	@FXML
	protected Circle loginIndicatorCircle;

	@FXML
	protected ProgressBar progressBar;

	@FXML
	protected ToggleButton uploadButton;

	@FXML
	protected SplitMenuButton pullButton;
	@FXML
	protected ToggleButton spellCheckToggle;
	@FXML
	protected Button alignButton;

	@FXML
	protected Button nextButton;
	@FXML
	protected Button previousButton;
	@FXML
	protected Button doneButton;

	@FXML
	protected TabPane upperTabPane;
	@FXML
	protected TabPane lowerTabPane;

	@FXML
	protected HBox wikisBox;
	@FXML
	protected Label tagLabel;
	@FXML
	protected Label countsLabel;

	@FXML
	protected ProgressIndicator spellCheckingProgress;
	@FXML
	protected Label spellCheckingProgressLabel;
	@FXML
	protected ProgressIndicator quererProgress;
	@FXML
	protected Label quererProgressLabel;
	@FXML
	protected ProgressIndicator downloaderProgress;
	@FXML
	protected Label downloaderProgressLabel;
	@FXML
	protected ProgressIndicator previewerProgress;
	@FXML
	protected Label previewerProgressLabel;
	@FXML
	protected ProgressIndicator uploaderProgress;
	@FXML
	protected Label uploaderProgressLabel;
	@FXML
	protected ProgressIndicator savingSessionProgress;
	@FXML
	protected Label savingSessionProgressLabel;
	@FXML
	protected ProgressIndicator pullingProgress;
	@FXML
	protected Label pullingProgressLabel;
	@FXML
	protected ProgressIndicator checkingTitleProgress;
	@FXML
	protected Label checkingTitleProgressLabel;
	

	@FXML
	protected Tab snippetsTab;
	@FXML
	protected Tab lookupTab;
	@FXML
	protected Tab findTab;
	@FXML
	protected ListView<Label> articleList;
	@FXML
	protected TextArea notesEdit;
	
	private Editor editor = Editor.TRANSLATE;
	
	protected ArrayList<TranslateTabMediator> upperTabs = new ArrayList<>();
	protected ArrayList<TranslateTabMediator> lowerTabs = new ArrayList<>();
	
	
	// INIT
	@FXML
	public void initialize()
	 {
		List<String> pullPluginNames = MinorityTranslateModel.pull().getPluginNames(MinorityTranslateModel.preferences().getGUILangCode());
		
		pullButton.getItems().clear();

		boolean first = true;
		ToggleGroup toggleGroup = new ToggleGroup();
		for (String pluginName : pullPluginNames) {
			RadioMenuItem item = new RadioMenuItem(pluginName);
			if (first) {
				item.setSelected(true);
				first = false;
			}
			item.setToggleGroup(toggleGroup);
			pullButton.getItems().add(item);
		}
	 };
	
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
		// Snippets addon
		Mediator snippetsMediator = MediatorLoaderFX.loadSnippetsPaneMediator();
		snippetsTab.setContent((Node) snippetsMediator.getViewComponent());
		getFacade().registerMediator(snippetsMediator);
		
		// lookup addon
		lookupTab.selectedProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable obs) {
				if (lookupTab.getContent() != null) return;
				LookupAddonPageMediator lookupMediator = MediatorLoaderFX.loadLookupAddonPageMediator();
				lookupTab.setContent((Node) lookupMediator.getViewComponent());
				getFacade().registerMediator(lookupMediator);
				lookupMediator.open();
			}
		});

		// find addon
		findTab.selectedProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable obs) {
				if (findTab.getContent() != null) return;
				FindAddonPageMediator findMediator = MediatorLoaderFX.loadFindAddonPageMediator();
				findTab.setContent((Node) findMediator.getViewComponent());
				getFacade().registerMediator(findMediator);
				findMediator.open();
			}
		});
	 }
	
	@Override
	public Mediator createArticlesMediator() {
		return new ArticlesMediatorFX(articleList);
	}

	@Override
	public Mediator createNotesMediator() {
		return new NotesMediatorFX(notesEdit);
	}


	// INHERIT (EDITORS)
	public void openEditor(Editor editor)
	 {
		ObservableList<Node> editors;
		
		switch (editor) {
		case TRANSLATE:
			translateEditorPage.requestLayout();
			
			editors = editorsPane.getChildren();
			for (Node node : editors) {
				node.setVisible(node == translateEditorPage);
			}

			this.editor = editor;
			break;
		
		case ALIGN:
			if (alignEditorPage != null) alignEditorPage.requestLayout();
			
			if (alignEditor == null) {
				alignEditor = MediatorLoaderFX.loadAlignEditorMediator();
				alignEditorPage = (Parent) alignEditor.getViewComponent();
				editorsPane.getChildren().add(alignEditorPage);
				getFacade().registerMediator(alignEditor);
				alignEditor.refreshSegments();
			}
			
			editors = editorsPane.getChildren();
			for (Node node : editors) {
				node.setVisible(node == alignEditorPage);
			}
			
			this.editor = editor;
			break;

		default:
			break;
		}
	 }
	
	@Override
	public Editor getEditor() {
		return editor;
	}

	
	
	// INHERIT (SELECTED)
	@Override
	public String findSelectedSrcLangCode() {
		Tab tab = lowerTabPane.getSelectionModel().getSelectedItem();
		if (tab == null) return null;
		String text = tab.getText();
		int i, j;
		i = text.indexOf('(');
		j = text.indexOf(')');
		if (i > 0 && j > i) return text.substring(i + 1, j);
		return text;
	}
	
	
	
	// INHERIT (TABS)
	@Override
	protected TranslateTabMediator createTab(TabsBox box)
	 {
		// create
		TranslateTabMediator tabMediator = MediatorLoaderFX.loadTranslateTabMediator();
		List<TranslateTabMediator> tabs = getTabs(box);
		tabs.add(tabMediator);
		
		// add to view
		Tab tab = new Tab();
		tab.setContent((Node) tabMediator.getViewComponent());
		tabMediator.setTabObject(tab);
		getTabPane(box).getTabs().add(tab);
		
		return tabMediator;
	 }
	
	@Override
	protected TranslateTabMediator destroyTab(TabsBox box)
	 {
		// remove
		List<TranslateTabMediator> tabs = getTabs(box);
		TranslateTabMediator tab = tabs.remove(tabs.size() - 1);
		
		// remove from view
		getTabPane(box).getTabs().remove(tabs.size());
		
		return tab;
	 }
	
	@Override
	protected TranslateTabMediator getTab(TabsBox box, int i) {
		return getTabs(box).get(i);
	}
	
	@Override
	protected TranslateTabMediator getSelectedTab(TabsBox box)
	 {
		int i = getTabPane(box).getSelectionModel().getSelectedIndex();
		List<TranslateTabMediator> tabs = getTabs(box);
		if (i < 0 || i >= tabs.size()) return null;
		return tabs.get(i);
	 }
	
	@Override
	protected int getTabCount(TabsBox box) {
		return getTabs(box).size();
	}
	
	@Override
	protected List<TranslateTabMediator> getTabs(TabsBox box)
	 {
		if (box == TabsBox.UPPER) return upperTabs;
		else if (box == TabsBox.LOWER) return lowerTabs;
		else return new ArrayList<>();
	 }
	

	// INHERIT (MENU CONTROLS)
	@Override
	protected void setMenuUploadEnabled(boolean enabled) {
		uploadEnabledMenuItem.setSelected(enabled);
	}
	
	@Override
	protected void disableMenuExactToggle(boolean disabled) {
		menuExactToggleItem.setDisable(disabled);
	}
	

	// INHERIT (CONTROLS)
	protected void setNavigationDisable(boolean disabled)
	 {
		nextButton.setDisable(disabled);
		nextArticleMenuItem.setDisable(disabled);
		
		previousButton.setDisable(disabled);
		previousArticleMenuItem.setDisable(disabled);
	 };


	@Override
	protected boolean isUpload() {
		return uploadButton.isSelected();
	}
	
	@Override
	protected void setUpload(boolean upload) {
		uploadButton.setSelected(upload);
	}

	@Override
	protected void setUploadDisable(boolean disabled) {
		uploadButton.setDisable(disabled);
		uploadMenuItem.setDisable(disabled);
	}

	@Override
	protected String getPullPluginName() {
		ObservableList<MenuItem> menu = pullButton.getItems();
		for (MenuItem menuItem : menu) {
			if (menuItem instanceof RadioMenuItem) {
				if (((RadioMenuItem) menuItem).isSelected()) return menuItem.getText();
			}
		}
		return null;
	}
	
	@Override
	protected void setPullDisable(boolean disabled) {
		pullButton.setDisable(disabled);
	}

	@Override
	protected void setAlignDisable(boolean disabled) {
		alignButton.setDisable(disabled);
	}
	

	// INHERIT (DIVIDER)
	@Override
	public int getAddonsDivider() {
		return (int)Math.floor(addonsSplitPane.getDividerPositions()[0] * addonsSplitPane.getWidth());
	}
	
	@Override
	public void setAddonsDivider(int position) {
		if ((int)addonsSplitPane.getWidth() > 0.0) addonsSplitPane.setDividerPositions((double)position / addonsSplitPane.getWidth());
	}
	
	@Override
	public double getTranslateDivider() {
		return translateEditorPage.getDividerPositions()[0];
	}
	
	@Override
	public void setTranslateDivider(double position) {
		translateEditorPage.setDividerPositions(position);
	}
	
	
	// INHERIT (STATUS)
	protected void showWikis(String[] langCodes)
	 {
		ObservableList<Node> children = wikisBox.getChildren();
		children.clear();
		for (final String langCode : langCodes) {
			Hyperlink hyperlink = new Hyperlink(langCode);
			hyperlink.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					onWikiClicked(langCode);
				}
			});
			children.add(hyperlink);
		}
	 }
	
	@Override
	protected void showTag(String tag) {
		tagLabel.setText(tag);
	}
	
	@Override
	protected void showCount(String count) {
		countsLabel.setText(count);
	}
	
	
	@Override
	protected void setLoginIndicator(java.awt.Color colour, String tooltip) {
		loginIndicatorCircle.setFill(new Color((double)colour.getRed()/255.0, (double)colour.getGreen()/255.0, (double)colour.getBlue()/255.0, (double)colour.getAlpha()/255.0));
		Tooltip.install(loginIndicatorCircle, new Tooltip(tooltip));
	}
	

	protected void setProgressVisible(boolean visible) {
		progressBar.setVisible(visible);
	}
	
	protected void setProgress(double value) {
		progressBar.setProgress(value);
	}


	@Override
	protected void showSpellCheckerBusy(boolean busy) {
		spellCheckingProgress.setVisible(busy);
		spellCheckingProgress.setManaged(busy);
		spellCheckingProgressLabel.setVisible(busy);
		spellCheckingProgressLabel.setManaged(busy);
	}

	@Override
	protected void showQueuerBusy(boolean busy) {
		quererProgress.setVisible(busy);
		quererProgress.setManaged(busy);
		quererProgressLabel.setVisible(busy);
		quererProgressLabel.setManaged(busy);
	}

	@Override
	protected void showDownloaderBusy(boolean busy) {
		downloaderProgress.setVisible(busy);
		downloaderProgress.setManaged(busy);
		downloaderProgressLabel.setVisible(busy);
		downloaderProgressLabel.setManaged(busy);
	}

	@Override
	protected void showPreviewerBusy(boolean busy) {
		previewerProgress.setVisible(busy);
		previewerProgress.setManaged(busy);
		previewerProgressLabel.setVisible(busy);
		previewerProgressLabel.setManaged(busy);
	}

	@Override
	protected void showUploaderBusy(boolean busy) {
		uploaderProgress.setVisible(busy);
		uploaderProgress.setManaged(busy);
		uploaderProgressLabel.setVisible(busy);
		uploaderProgressLabel.setManaged(busy);
	}

	@Override
	protected void showSessionSaveBusy(boolean busy) {
		savingSessionProgress.setVisible(busy);
		savingSessionProgress.setManaged(busy);
		savingSessionProgressLabel.setVisible(busy);
		savingSessionProgressLabel.setManaged(busy);
	}

	@Override
	protected void showPullingBusy(boolean busy) {
		pullingProgress.setVisible(busy);
		pullingProgress.setManaged(busy);
		pullingProgressLabel.setVisible(busy);
		pullingProgressLabel.setManaged(busy);
	}
	
	@Override
	protected void showCheckingTitleBusy(boolean busy) {
		checkingTitleProgress.setVisible(busy);
		checkingTitleProgress.setManaged(busy);
		checkingTitleProgressLabel.setVisible(busy);
		checkingTitleProgressLabel.setManaged(busy);
	}

	// EVENTS
	@FXML
	public void onNextClick() {
		super.onNextClick();
	}

	@FXML
	public void onPreviousClick() {
		super.onPreviousClick();
	}
	
	
	@FXML
	public void onMenuSessionNewClick() {
		super.onMenuSessionNewClick();
	}

	@FXML
	public void onMenuSessionOpenClick() {
		super.onMenuSessionOpenClick();
	}

	@FXML
	public void onMenuSessionSaveClick() {
		super.onMenuSessionSaveClick();
	}

	@FXML
	public void onMenuSessionSaveAsClick() {
		super.onMenuSessionSaveAsClick();
	}
	
	@FXML
	public void onMenuPreferencesClick() {
		super.onMenuPreferencesClick();
	}
	
	@FXML
	public void onMenuLoginClick() {
		super.onMenuLoginClick();
	}
	
	@FXML
	public void onMenuLogoutClick() {
		super.onMenuLogoutClick();
	}

	@FXML
	@Override
	public void onMenuQuitClick() {
		super.onMenuQuitClick();
	}

	@FXML
	public void onMenuUploadClick() {
		super.onMenuUploadClick();
	}

	@FXML
	public void onMenuUploadEnabled() {
		super.onUploadEnable(uploadEnabledMenuItem.isSelected());
	}
	
	@FXML
	public void onMenuAddCategoryClick() {
		super.onAddCategory();
	}

	@FXML
	public void onMenuAddArticleClick() {
		super.onAddArticle();
	}

	@FXML
	public void onMenuAddLinksClick() {
		super.onMenuAddLinksClick();
	}

	@FXML
	public void onMenuAddContentClick() {
		super.onAddContent();
	}
	
	
	@FXML
	@Override
	public void onMenuStopDownloadingClick() {
		super.onMenuStopDownloadingClick();
	}
	
	@FXML
	@Override
	public void onMenuListsClick() {
		super.onMenuListsClick();
	}
	
	@FXML
	public void onMenuRemoveArticleClick() {
		super.onMenuRemoveArticleClick();
	}

	@FXML
	@Override
	public void onMenuRemoveSelectedArticleClick() {
		super.onMenuRemoveSelectedArticleClick();
	}

	@FXML
	public void onMenuAboutClick() {
		super.onMenuAboutClick();
	}

	@FXML
	public void onMenuManualClick() {
		super.onMenuManualClick();
	}
	
	
	// EVENTS (MENU)
	@Override
	public void onMenuTogglePreviewClick() {
		super.onMenuTogglePreviewClick();
	}
	
	@FXML
	public void onMenuExactToggleClick() {
		super.onMenuToggleExactClick();
	}
	
	
	// EVENTS (CONTROL)
	@FXML
	public void onMenuNextArticleClick() {
		super.onMenuNextArticleClick();
	}

	@FXML
	public void onMenuPreviousArticleClick() {
		super.onMenuPreviousArticleClick();
	}


	@FXML
	public void onUploadToggle()
	 {
		super.onUpload(uploadButton.isSelected());
	 }

	@FXML
	public void onPullClick()
	 {
		super.onPull();
	 }

	@FXML
	public void onAlignClick()
	 {
		super.onAlign();
	 }

	@FXML
	public void onTagClick()
	 {
		super.onTag();
	 }

	
	// HELPERS
	private TabPane getTabPane(TabsBox box)
	 {
		if (box == TabsBox.UPPER) return upperTabPane;
		else if (box == TabsBox.LOWER) return lowerTabPane;
		else return null;
	 }
	
	
}
