package ee.translate.keeleleek.mtapplication.view.swing.dialogs;

import java.awt.Cursor;

import javax.swing.Icon;

import ee.translate.keeleleek.mtapplication.model.media.LogosProxy;
import ee.translate.keeleleek.mtapplication.view.dialogs.AboutDialogMediator;
import ee.translate.keeleleek.mtapplication.view.swing.swingview.SwingAboutViewWindowComponent;
import javafx.fxml.FXML;


public class AboutDialogMediatorSwing extends AboutDialogMediator {
	
	// IMPLEMENTATION:
	@Override
	public SwingAboutViewWindowComponent getViewComponent() {
		return (SwingAboutViewWindowComponent) super.getViewComponent();
	}
	
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
		LogosProxy logosProxi = (LogosProxy) getFacade().retrieveProxy(LogosProxy.NAME);
		
		getViewComponent().minorityTranslateLogoLabel.setIcon((Icon) logosProxi.retrieveApplicationLogo());
		getViewComponent().wikimediaLabel.setIcon((Icon) logosProxi.retrieveWMFLogo());
		getViewComponent().keeleleekLogoLabel.setIcon((Icon) logosProxi.retrieveKeeleleekLogo());
		
		getViewComponent().licenseLabel.setCursor(new Cursor(Cursor.HAND_CURSOR));
		getViewComponent().wikimediaLabel.setCursor(new Cursor(Cursor.HAND_CURSOR));
		getViewComponent().keeleleekLogoLabel.setCursor(new Cursor(Cursor.HAND_CURSOR));
	 }

	@Override
	protected void setVersion(String version, String date) {
		getViewComponent().versionLabel.setText(version + " (" + date + ")");
	}
	
	
	// BUTTONS:
	@FXML
	@Override
	public void onLicenceHyperlinkClick() {
		super.onLicenceHyperlinkClick();
	}

	@FXML
	@Override
	public void onWMFImageViewClick() {
		super.onWMFImageViewClick();
	}

	@FXML
	@Override
	public void onKeeleleekImageViewClick() {
		super.onKeeleleekImageViewClick();
	}
	
}
