package ee.translate.keeleleek.mtapplication.view.javafx.helpers;

import java.util.ArrayList;
import java.util.List;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteChoice;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteChoices;
import ee.translate.keeleleek.mtapplication.view.elements.TranslateTabMediator;
import ee.translate.keeleleek.mtapplication.view.javafx.application.ApplicationMediatorFX;
import ee.translate.keeleleek.mtpluginframework.spellcheck.Misspell;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Popup;

public abstract class EditorLayerFX extends TranslateTabMediator {

	public static KeyCombination AUTOCOMPLETE_KEYS = new KeyCodeCombination(KeyCode.SPACE, KeyCombination.SHORTCUT_DOWN);
	
	public static KeyCombination PASTE_KEYS = new KeyCodeCombination(KeyCode.V, KeyCombination.SHORTCUT_DOWN);
	
	public static final String EDITOR_STYLESHEET_PATH = TranslateTabMediator.class.getResource("/css/editor.css").toExternalForm();

	protected Popup popup = new Popup();
	protected ListView<AutocompleteBox> popupContent = new ListView<>();
	
	protected List<Misspell> misspells = new ArrayList<>();
	protected Tooltip misspellTooltip = null;
	
	protected boolean feedback = false;
	
	
	// INIT
	protected abstract void initEditor();
	
	protected void initPopup()
	 {
		// popup
		popup.getScene().getStylesheets().add(getClass().getResource(ApplicationMediatorFX.findFontSizeCSS(MinorityTranslateModel.preferences().getFontSize())).toExternalForm());
		
		popupContent.setMinWidth(400);
		
		popupContent.addEventFilter(MouseEvent.MOUSE_CLICKED ,new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event)
			 {
				if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
					autocomplete(popupContent.getSelectionModel().getSelectedItem().getChoise());
					event.consume();
					popup.hide();
				}
			 }
		});
		
		popupContent.addEventFilter(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event)
			 {
				switch (event.getCode()) {
				case ENTER:
					autocomplete(popupContent.getSelectionModel().getSelectedItem().getChoise());
				case ESCAPE:
					event.consume();
					popup.hide();	
					break;

				default:
					break;
				}
			 }
		});
		
		popup.getContent().addAll(popupContent);
		popup.setAutoHide(true);
	 }
	
	
	// EDITOR
	public abstract Node getParent();
	
	public abstract Point2D findCaretPosition();

	public abstract void setTextEditEditable(boolean editable);
	
	
	// CONTENT
	public abstract String getText();

	public abstract void setText(String text);
	
	public abstract void replaceText(String text);
	
	public abstract void pasteText(String text);
	
	
	// AUTOCOMPLETE
	public abstract void autocomplete(AutocompleteChoice choice);

	public abstract void showAutocomplete(AutocompleteChoices autocomplete);
	

	// MISSPELL
	public void showMisspellTooltip(Misspell misspell)
	 {
		Point2D pos = findCaretPosition();
		if (pos == null) return;

		// show tooltip
		misspellTooltip = new Tooltip(misspell.getMessage());
		misspellTooltip.show(getParent(), pos.getX(), pos.getY());
	 }
	
	public void closeMisspellTooltip()
	 {
		if (misspellTooltip == null) return;
		
		misspellTooltip.hide();
		misspellTooltip = null;
	 }
	
	public Misspell findMisspell(int row, int col) {
		for (Misspell misspell : misspells) {
			if (misspell.getStartRow() <= row && row <= misspell.getEndRow())
				if (misspell.getStartColumn() <= col && col <= misspell.getEndColumn()) return misspell;
		}
		return null;
	}

	public abstract void markMisspells(List<Misspell> misspells);
	
	
}
