package ee.translate.keeleleek.mtapplication.view.swing;

import javax.swing.JFrame;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.view.dialogs.PreferencesDialogMediator;
import ee.translate.keeleleek.mtapplication.view.dialogs.SimpleDialogMediator;
import ee.translate.keeleleek.mtapplication.view.elements.SnippetsPaneMediator;
import ee.translate.keeleleek.mtapplication.view.elements.TranslateTabMediator;
import ee.translate.keeleleek.mtapplication.view.swing.dialogs.AboutDialogMediatorSwing;
import ee.translate.keeleleek.mtapplication.view.swing.dialogs.ConfirmationDialogMediatorSwing;
import ee.translate.keeleleek.mtapplication.view.swing.dialogs.ListsDialogMediatorSwing;
import ee.translate.keeleleek.mtapplication.view.swing.dialogs.LoginDialogMediatorSwing;
import ee.translate.keeleleek.mtapplication.view.swing.dialogs.ManualDialogMediatorSwing;
import ee.translate.keeleleek.mtapplication.view.swing.dialogs.MessageDialogMediatorSwing;
import ee.translate.keeleleek.mtapplication.view.swing.dialogs.PreferencesDialogMediatorSwing;
import ee.translate.keeleleek.mtapplication.view.swing.dialogs.SimpleDialogMediatorSwing;
import ee.translate.keeleleek.mtapplication.view.swing.elements.SnippetsPaneMediatorSwing;
import ee.translate.keeleleek.mtapplication.view.swing.elements.TranslateTabMediatorSwing;
import ee.translate.keeleleek.mtapplication.view.swing.swingview.SwingAboutViewWindowComponent;
import ee.translate.keeleleek.mtapplication.view.swing.swingview.SwingConfirmationDialogViewComponent;
import ee.translate.keeleleek.mtapplication.view.swing.swingview.SwingListsWindowViewComponent;
import ee.translate.keeleleek.mtapplication.view.swing.swingview.SwingLoginWindowViewComponent;
import ee.translate.keeleleek.mtapplication.view.swing.swingview.SwingManualWindowViewComponent;
import ee.translate.keeleleek.mtapplication.view.swing.swingview.SwingMessageWindowViewComponent;
import ee.translate.keeleleek.mtapplication.view.swing.swingview.SwingPreferencesWindowViewComponent;
import ee.translate.keeleleek.mtapplication.view.swing.swingview.SwingSimpleDialogWindowViewComponent;
import ee.translate.keeleleek.mtapplication.view.swing.swingview.SwingSnippetsPaneViewComponent;
import ee.translate.keeleleek.mtapplication.view.swing.swingview.SwingTranslateTabViewComponent;
import ee.translate.keeleleek.mtapplication.view.swing.swingview.SwingTranslateWindowViewComponent;
import ee.translate.keeleleek.mtapplication.view.swing.windows.TranslateWindowMediatorSwing;
import ee.translate.keeleleek.mtapplication.view.windows.TranslateWindowMediator;

public class MediatorLoaderSwing {

	// WINDOWS:
	public static Mediator loadTranslateWindowMediator() {
		TranslateWindowMediator mediator = new TranslateWindowMediatorSwing();
		SwingTranslateWindowViewComponent viewComponent = new SwingTranslateWindowViewComponent();
		mediator.setViewComponent(viewComponent);
		viewComponent.mediator = mediator;
		return mediator;
	}

	public static Mediator loadLoginWindowMediator(JFrame parent) {
		LoginDialogMediatorSwing mediator = new LoginDialogMediatorSwing();
		SwingLoginWindowViewComponent viewComponent = new SwingLoginWindowViewComponent(parent);
		mediator.setViewComponent(viewComponent);
		viewComponent.mediator = mediator;
		return mediator;
	}

	public static Mediator loadPreferencesWindowMediator(JFrame parent) {
		PreferencesDialogMediator mediator = new PreferencesDialogMediatorSwing();
		SwingPreferencesWindowViewComponent viewComponent = new SwingPreferencesWindowViewComponent(parent);
		mediator.setViewComponent(viewComponent);
		viewComponent.mediator = mediator;
		return mediator;
	}

	public static Mediator loadAddCategoryWindowMediator() {
		SimpleDialogMediatorSwing mediator = new SimpleDialogMediatorSwing(SimpleDialogMediator.NAME_ADD_CATEGORY);
		SwingSimpleDialogWindowViewComponent viewComponent = new SwingSimpleDialogWindowViewComponent();
		mediator.setViewComponent(viewComponent);
		viewComponent.mediator = mediator;
		return mediator;
	}

	public static Mediator loadAddArticleWindowMediator() {
		SimpleDialogMediatorSwing mediator = new SimpleDialogMediatorSwing(SimpleDialogMediator.NAME_ADD_ARTICLE);
		SwingSimpleDialogWindowViewComponent viewComponent = new SwingSimpleDialogWindowViewComponent();
		mediator.setViewComponent(viewComponent);
		viewComponent.mediator = mediator;
		return mediator;
	}

	public static Mediator loadRemoveArticleWindowMediator() {
		SimpleDialogMediatorSwing mediator = new SimpleDialogMediatorSwing(SimpleDialogMediator.NAME_REMOVE_ARTICLE);
		SwingSimpleDialogWindowViewComponent viewComponent = new SwingSimpleDialogWindowViewComponent();
		mediator.setViewComponent(viewComponent);
		viewComponent.mediator = mediator;
		return mediator;
	}

	public static Mediator loadAddLinksWindowMediator() {
		SimpleDialogMediatorSwing mediator = new SimpleDialogMediatorSwing(SimpleDialogMediator.NAME_ADD_LINKS);
		SwingSimpleDialogWindowViewComponent viewComponent = new SwingSimpleDialogWindowViewComponent();
		mediator.setViewComponent(viewComponent);
		viewComponent.mediator = mediator;
		return mediator;
	}

	public static Mediator loadAboutWindowMediator(JFrame parent) {
		AboutDialogMediatorSwing mediator = new AboutDialogMediatorSwing();
		SwingAboutViewWindowComponent viewComponent = new SwingAboutViewWindowComponent(parent);
		mediator.setViewComponent(viewComponent);
		viewComponent.mediator = mediator;
		return mediator;
	}

	public static Mediator loadManualWindowMediator(JFrame parent) {
		ManualDialogMediatorSwing mediator = new ManualDialogMediatorSwing();
		SwingManualWindowViewComponent viewComponent = new SwingManualWindowViewComponent(parent);
		mediator.setViewComponent(viewComponent);
		viewComponent.mediator = mediator;
		return mediator;
	}

	public static Mediator loadListsWindowMediator(JFrame parent) {
		ListsDialogMediatorSwing mediator = new ListsDialogMediatorSwing();
		SwingListsWindowViewComponent viewComponent = new SwingListsWindowViewComponent(parent);
		mediator.setViewComponent(viewComponent);
		viewComponent.mediator = mediator;
		return mediator;
	}


	// PANES:
	public static TranslateTabMediator loadTranslateTabMediator() {
		TranslateTabMediator mediator = new TranslateTabMediatorSwing();
		SwingTranslateTabViewComponent viewComponent = new SwingTranslateTabViewComponent();
		mediator.setViewComponent(viewComponent);
		viewComponent.mediator = mediator;
		return mediator;
	}
	
	public static SnippetsPaneMediator loadSnippetsPaneMediator() {
		SnippetsPaneMediator mediator = new SnippetsPaneMediatorSwing();
		SwingSnippetsPaneViewComponent viewComponent = new SwingSnippetsPaneViewComponent();
		mediator.setViewComponent(viewComponent);
		viewComponent.mediator = mediator;
		return mediator;
	}
	
	
//	public static Mediator loadAddonsPaneMediator() {
//		return loadMediator(AddonsPaneMediator.NAME, "/minoritytranslate/view/AddonsPane.fxml");
//	}
//
//	public static Mediator loadArticlonsPaneMediator() {
//		return loadMediator(ArticlonsPaneMediator.NAME, "/minoritytranslate/view/ArticlonsPane.fxml");
//	}


	public static MessageDialogMediatorSwing loadMessageWindowMediator(JFrame parent) {
		MessageDialogMediatorSwing mediator = new MessageDialogMediatorSwing();
		SwingMessageWindowViewComponent viewComponent = new SwingMessageWindowViewComponent(parent);
		mediator.setViewComponent(viewComponent);
		viewComponent.mediator = mediator;
		return mediator;
	}

	public static ConfirmationDialogMediatorSwing loadConfirmationDialogMediator(JFrame parent) {
		ConfirmationDialogMediatorSwing mediator = new ConfirmationDialogMediatorSwing();
		SwingConfirmationDialogViewComponent viewComponent = new SwingConfirmationDialogViewComponent(parent);
		mediator.setViewComponent(viewComponent);
		viewComponent.mediator = mediator;
		return mediator;
	}

}
