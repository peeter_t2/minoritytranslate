package ee.translate.keeleleek.mtapplication.view.swing.elements;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.media.IconsProxySwing;
import ee.translate.keeleleek.mtapplication.view.elements.ArticlesMediator;


public class ArticlesMediatorSwing extends ArticlesMediator {

	
	private JList<JLabel> articleList;
	private DefaultListModel<JLabel> model = new DefaultListModel<>();
	
	// INIT:
	public ArticlesMediatorSwing(JList<JLabel> articlesList)
	 {
		this.articleList = articlesList;
		this.articleList.setModel(model);
		this.articleList.setCellRenderer(new LabelRenderer());
		
		this.articleList.addListSelectionListener(new ListSelectionListener()
		 {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				selectedIndexChanged(articleList.getSelectedIndex());
			}
		 });
	 }

	
	// INHERIT
	@Override
	protected void setList(String[] items)
	 {
		IconsProxySwing icons = (IconsProxySwing) MinorityTranslateModel.icons();
		
		while (model.size() < items.length) model.add(model.size(), new JLabel());
		while (model.size() > items.length) model.remove(model.size() + 1);
	
		for (int i = 0; i < items.length; i++) {
			
			Icon icon = icons.getArticon(MinorityTranslateModel.content().findArticlon(i));
			
			JLabel label = model.get(i);
			label.setText(items[i]);
			label.setIcon(icon);
			
		}
	 }
	
	@Override
	protected void fetchArticon(int i)
	 {
		if (i < 0 || i >= model.size()) return;
		
		IconsProxySwing icons = (IconsProxySwing) MinorityTranslateModel.icons();
		Icon icon = icons.getArticon(MinorityTranslateModel.content().findArticlon(i));
		
		JLabel label = model.get(i);
		label.setIcon(icon);
		
		articleList.repaint();
	 }
	
	@Override
	protected void setSelectedIndex(int i) {
		articleList.setSelectedIndex(i);
		articleList.ensureIndexIsVisible(articleList.getSelectedIndex());
	}
	
	
	public class LabelRenderer extends JLabel implements ListCellRenderer<JLabel> {
		 
		private static final long serialVersionUID = -1175107501306449589L;

		protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();
		
		@Override
	    public Component getListCellRendererComponent(JList<? extends JLabel> list, JLabel value, int index, boolean isSelected, boolean cellHasFocus)
	     {
			JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
			
			renderer.setIcon(value.getIcon());
			renderer.setText(value.getText());
	        
	        return renderer;
	     }
	     
	}
	
}
