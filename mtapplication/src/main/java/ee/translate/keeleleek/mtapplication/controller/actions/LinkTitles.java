package ee.translate.keeleleek.mtapplication.controller.actions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import net.sourceforge.jwbf.core.actions.Post;
import net.sourceforge.jwbf.core.actions.util.ActionException;
import net.sourceforge.jwbf.core.actions.util.HttpAction;
import net.sourceforge.jwbf.mediawiki.ApiRequestBuilder;
import net.sourceforge.jwbf.mediawiki.MediaWiki;
import net.sourceforge.jwbf.mediawiki.actions.editing.GetApiToken;
import net.sourceforge.jwbf.mediawiki.actions.editing.GetApiToken.Intoken;
import net.sourceforge.jwbf.mediawiki.actions.editing.GetApiToken.TokenResponse;
import net.sourceforge.jwbf.mediawiki.actions.util.MWAction;

public class LinkTitles extends MWAction {

	private HttpAction tokenHTTPAction = null;
	private HttpAction linkHTTPAction = null;
	
	private String fromLangCode; 
	private String fromTitle; 
	private String toLangCode; 
	private String toTitle; 
	
	private GetApiToken apiTokenAction = null;
	
	
	public LinkTitles(String srcLangCode, String srcTitle, String dstLangCode, String dstTitle)
	 {
		this.fromLangCode = srcLangCode;
		this.fromTitle = srcTitle;
		this.toLangCode = dstLangCode;
		this.toTitle = dstTitle;
		
		apiTokenAction = new GetApiToken(Intoken.EDIT, srcTitle);
		tokenHTTPAction = apiTokenAction.popAction();
	 }

	private Post createLinkHTTPAction(TokenResponse tokenResponse)
	 {
		Post action = new ApiRequestBuilder()
			.action("wbsetsitelink")
			.postParam(tokenResponse.token())
			.postParam("site", fromLangCode.replace('-', '_') + "wiki")
			.postParam("title", fromTitle)
			.postParam("linksite", toLangCode.replace('-', '_') + "wiki")
			.postParam("linktitle", toTitle)
			.postParam("format", "xml")
			.buildPost();
		
		return action;
	 }
	
	@Override
	public HttpAction getNextMessage() {
		if (tokenHTTPAction != null) return tokenHTTPAction;
		if (linkHTTPAction != null) return linkHTTPAction;
		return null;
	}

	@Override
	public boolean hasMoreMessages() {
		return tokenHTTPAction != null || linkHTTPAction != null;
	}

	@Override
	public String processReturningText(String xml, HttpAction action)
	 {
		Logger.getLogger(getClass()).info("LinkTitles response: " + xml);
		
		if (xml.contains("error")) {
			Pattern errFinder = Pattern.compile("<p>(.*?)</p>", Pattern.DOTALL | Pattern.MULTILINE);
			Matcher m = errFinder.matcher(xml);
			String lastP = "";
			while (m.find()) {
			  lastP = MediaWiki.urlDecode(m.group(1));
			}

			throw new ActionException("Link failed - " + lastP);
		}
		
		// token
		if (action == tokenHTTPAction) {
			apiTokenAction.processReturningText(xml, action);
			linkHTTPAction = createLinkHTTPAction(apiTokenAction.get());
			tokenHTTPAction = null;
		}
		
		// link
		else if (action == linkHTTPAction) {
			System.out.println("LINK RESULT=" + xml);
			linkHTTPAction = null;
		}
		
		return super.processReturningText(xml, action);
	 }

	
}
