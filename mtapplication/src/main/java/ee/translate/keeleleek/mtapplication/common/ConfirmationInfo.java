package ee.translate.keeleleek.mtapplication.common;

public class ConfirmationInfo {

	private String titleKey;
	private String messageKey;
	private String detailsKey;
	private String yesKey = "confirmation.yes";
	private String noKey = "confirmation.no";
	private String cancelKey = "confirmation.cancel";
	private ConfirmState state = ConfirmState.CANCEL;
	

	public ConfirmationInfo(String titleKey, String messageKey, String detailsKey) {
		this.titleKey = titleKey;
		this.messageKey = messageKey;
		this.detailsKey = detailsKey;
	}

	public ConfirmationInfo(String titleKey, String messageKey, String detailsKey, String yesKey, String noKey, String cancelKey) {
		this.titleKey = titleKey;
		this.messageKey = messageKey;
		this.detailsKey = detailsKey;
		this.yesKey = yesKey;
		this.noKey = noKey;
		this.cancelKey = cancelKey;
	}
	
	public String getTitleKey() {
		return titleKey;
	}
	
	public String getMessageKey() {
		return messageKey;
	}
	
	public String getDetailsKey() {
		return detailsKey;
	}
	
	
	public String getYesKey() {
		return yesKey;
	}
	
	public String getNoKey() {
		return noKey;
	}
	
	public String getCancelKey() {
		return cancelKey;
	}
	

	public ConfirmState getState() {
		return state;
	}
	
	public void setState(ConfirmState state) {
		this.state = state;
	}
	
	
	public enum ConfirmState {
		YES,
		NO,
		CANCEL
	}
	
}
