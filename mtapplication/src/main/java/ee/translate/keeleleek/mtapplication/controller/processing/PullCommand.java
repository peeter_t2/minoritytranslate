package ee.translate.keeleleek.mtapplication.controller.processing;

import java.util.regex.PatternSyntaxException;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.requests.SrcDstRequest;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.Status;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;

public class PullCommand extends SimpleCommand {

	private static Logger LOGGER = LoggerFactory.getLogger(PullCommand.class);
	
	
	@Override
	public void execute(INotification notification)
	 {
		SrcDstRequest request = (SrcDstRequest) notification.getBody();
		
		// check articles
		MinorityArticle srcArticle = MinorityTranslateModel.content().getArticle(request.getSrcRef());
		if (srcArticle == null) {
			LOGGER.error("Failed to pull, because the article with reference " + request.getSrcRef() + " is missing!");
			return;
		}

		MinorityArticle dstArticle = MinorityTranslateModel.content().getArticle(request.getDstRef());
		if (dstArticle == null) {
			LOGGER.error("Failed to pull, because the article with reference " + request.getDstRef() + " is missing!");
			return;
		}
		
		// check status
		if (dstArticle.getStatus() != Status.STANDBY) {
			sendNotification(Notifications.ERROR_MESSAGE, Messages.getString("messages.pull.header.pulling.failed"), Messages.getString("messages.pull.description.target.not.editable"));
			return;
		}
		
		try {
			MinorityTranslateModel.processer().pullProcess(srcArticle, dstArticle);
		} catch (PatternSyntaxException e) {
			sendNotification(Notifications.ERROR_MESSAGE, Messages.getString("messages.pull.header.pulling.failed"), Messages.getString("messages.pull.description.regex.error").replaceFirst("#description", e.getDescription()).replaceFirst("#pattern", e.getPattern()));
		}
	 }
	
}
