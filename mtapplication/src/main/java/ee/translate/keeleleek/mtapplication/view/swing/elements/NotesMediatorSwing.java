package ee.translate.keeleleek.mtapplication.view.swing.elements;

import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import ee.translate.keeleleek.mtapplication.view.elements.NotesMediator;


public class NotesMediatorSwing extends NotesMediator {

	private JTextArea notesEdit;
	
	boolean feedback = false;
	
	
	// INIT
	public NotesMediatorSwing(JTextArea notesEdit) {
		this.notesEdit = notesEdit;
	}
	
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
		notesEdit.getDocument().addDocumentListener(new DocumentListener()
		 {
			@Override
			public void removeUpdate(DocumentEvent e) {
				if (!feedback) onTextEdited();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				if (!feedback) onTextEdited();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				if (!feedback) onTextEdited();
			}
		 });
	 }
	
	
	// INHERIT (CONTENT)
	@Override
	public String getText() {
		return notesEdit.getText();
	}
	
	@Override
	public void setText(String text) {
		feedback = true;
		notesEdit.setText(text);
		feedback = false;
	}
	
	
}
