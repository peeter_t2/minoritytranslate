package ee.translate.keeleleek.mtapplication.view.swing.swingview;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import ee.translate.keeleleek.mtapplication.view.dialogs.PreferencesDialogMediator;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtapplication.view.swing.dialogs.PreferencesDialogMediatorSwing;
import ee.translate.keeleleek.mtapplication.view.swing.dialogs.PreferencesDialogMediatorSwing.PreferencesPage;

public class SwingPreferencesWindowViewComponent extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5729778357844037202L;
	
	public final JPanel programPage = new JPanel();
	public JTextField langsAddField;
	public JComboBox<String> guiLangCombo;
	public JCheckBox templatesCheckbox;
	public JButton okButton;
	public JButton cancelButton;
	public JPanel langsPane;
	public PreferencesDialogMediator mediator;
	public JButton langAddButton;
	public JCheckBox filesCheckbox;
	public JCheckBox introductionCheckbox;
	public JCheckBox referencesCheckbox;
	public JPanel langsPage;
	private JPanel addPane;
	public JPanel profPane;
	private JLabel profLangLabel;
	private JLabel proficiencyLabel;
	private JPanel corpusPage;
	private JPanel allowPane;
	public JCheckBox collectCorpusCheckBox;
	private JTextArea CorpusCollectLabel;
	public JSpinner categDepthField;
	public JTree preferencesTree;
	public JPanel pagesPane;
	private JPanel emptyPage;
	private JPanel collectPage;
	private JPanel replacePage;
	public JTable collectTable;
	public JTable replaceTable;
	private JSplitPane splitPane;
	private JButton collectCreateButton;
	private JButton collectRemoveButton;
	private JButton replaceCreateButton;
	private JPanel processingPage;
	public JLabel processingLink;
	private JLabel lblNewLabel;
	private JTextArea corpusExplanationTextArea;
	private JLabel corpusLink;
	private JTextArea languagesExplanationTextArea;
	private JLabel lblNewLabel_1;
	private JPanel panel_1;
	public JRadioButton adaptationRadiobutton;
	public JRadioButton exactRadiobutton;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Create the dialog.
	 */
	public SwingPreferencesWindowViewComponent(JFrame parent) {
		super(parent);
		setMinimumSize(new Dimension(500, 400));
		
		setBounds(100, 100, 590, 400);
		getContentPane().setLayout(new BorderLayout());
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						mediator.confirm();
					}
				});
				{
					cancelButton = new JButton(Messages.getString("button.cancel")); //$NON-NLS-1$
					cancelButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							mediator.decline();
						}
					});
					cancelButton.setActionCommand("Cancel");
					buttonPane.add(cancelButton);
				}
				okButton.setActionCommand(Messages.getString("button.ok")); //$NON-NLS-1$
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		
		splitPane = new JSplitPane();
		getContentPane().add(splitPane, BorderLayout.CENTER);
		
		preferencesTree = new JTree();
		preferencesTree.setMinimumSize(new Dimension(125, 0));
		preferencesTree.setPreferredSize(new Dimension(100, 72));
		preferencesTree.setRootVisible(false);
		splitPane.setLeftComponent(preferencesTree);
		preferencesTree.setModel(new DefaultTreeModel(
			new DefaultMutableTreeNode("JTree") {
				private static final long serialVersionUID = -1924500152047588030L;
				{
					
				}
			}
		));
		
		pagesPane = new JPanel();
		splitPane.setRightComponent(pagesPane);
		pagesPane.setLayout(new CardLayout(0, 0));
		pagesPane.add(programPage, PreferencesPage.PROGRAM.toString());
		programPage.setVisible(false);
		programPage.setBorder(new EmptyBorder(14, 14, 14, 14));
		GridBagLayout gbl_programPage = new GridBagLayout();
		gbl_programPage.columnWidths = new int[]{135, 0, 0};
		gbl_programPage.rowHeights = new int[]{19, 92, 24, 0};
		gbl_programPage.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_programPage.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		programPage.setLayout(gbl_programPage);
		JLabel categDepthLabel = new JLabel(Messages.getString("preferences.program.category.depth"));
		GridBagConstraints gbc_categDepthLabel = new GridBagConstraints();
		gbc_categDepthLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_categDepthLabel.insets = new Insets(0, 0, 5, 5);
		gbc_categDepthLabel.gridx = 0;
		gbc_categDepthLabel.gridy = 0;
		programPage.add(categDepthLabel, gbc_categDepthLabel);
		
		categDepthField = new JSpinner();
		categDepthField.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				mediator.onCategoryDepthChange((Integer) categDepthField.getValue());
			}
		});
		GridBagConstraints gbc_categDepthField = new GridBagConstraints();
		gbc_categDepthField.fill = GridBagConstraints.HORIZONTAL;
		gbc_categDepthField.insets = new Insets(0, 0, 5, 0);
		gbc_categDepthField.gridx = 1;
		gbc_categDepthField.gridy = 0;
		programPage.add(categDepthField, gbc_categDepthField);
		
		JLabel filtersLabel = new JLabel(Messages.getString("preferences.program.filters")); //$NON-NLS-1$
		GridBagConstraints gbc_filtersLabel = new GridBagConstraints();
		gbc_filtersLabel.anchor = GridBagConstraints.WEST;
		gbc_filtersLabel.insets = new Insets(0, 0, 5, 5);
		gbc_filtersLabel.gridx = 0;
		gbc_filtersLabel.gridy = 1;
		programPage.add(filtersLabel, gbc_filtersLabel);
		
		JPanel panel = new JPanel();
		panel.setBorder(null);
		panel.setLayout(new GridLayout(0, 1, 0, 0));
		
		templatesCheckbox = new JCheckBox(Messages.getString("preferences.program.filters.templates")); //$NON-NLS-1$
		templatesCheckbox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onTemplatesFilterChange(templatesCheckbox.isSelected());
			}
		});
		panel.add(templatesCheckbox);
		
		filesCheckbox = new JCheckBox(Messages.getString("preferences.program.filters.files")); //$NON-NLS-1$
		filesCheckbox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onFilesFilterChange(filesCheckbox.isSelected());
			}
		});
		panel.add(filesCheckbox);
		
		introductionCheckbox = new JCheckBox(Messages.getString("preferences.program.filters.introduction.only")); //$NON-NLS-1$
		introductionCheckbox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onIntroductionFilterChange(introductionCheckbox.isSelected());
			}
		});
		panel.add(introductionCheckbox);
		
		referencesCheckbox = new JCheckBox(Messages.getString("preferences.program.filters.references")); //$NON-NLS-1$
		referencesCheckbox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onReferencesFilterChange(referencesCheckbox.isSelected());
			}
		});
		panel.add(referencesCheckbox);
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel.anchor = GridBagConstraints.NORTH;
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.gridx = 1;
		gbc_panel.gridy = 1;
		programPage.add(panel, gbc_panel);
		
		JLabel interfaceLangLabel = new JLabel(Messages.getString("preferences.program.interface.language")); //$NON-NLS-1$
		GridBagConstraints gbc_interfaceLangLabel = new GridBagConstraints();
		gbc_interfaceLangLabel.anchor = GridBagConstraints.WEST;
		gbc_interfaceLangLabel.insets = new Insets(0, 0, 0, 5);
		gbc_interfaceLangLabel.gridx = 0;
		gbc_interfaceLangLabel.gridy = 2;
		programPage.add(interfaceLangLabel, gbc_interfaceLangLabel);
		
		guiLangCombo = new JComboBox<String>();
		guiLangCombo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((PreferencesDialogMediatorSwing) mediator).onGUILanguageChange(guiLangCombo.getSelectedIndex());
			}
		});
		guiLangCombo.setBorder(null);
		guiLangCombo.setMaximumSize(new Dimension(150, 150));
		GridBagConstraints gbc_guiLangCombo = new GridBagConstraints();
		gbc_guiLangCombo.fill = GridBagConstraints.HORIZONTAL;
		gbc_guiLangCombo.anchor = GridBagConstraints.NORTH;
		gbc_guiLangCombo.gridx = 1;
		gbc_guiLangCombo.gridy = 2;
		programPage.add(guiLangCombo, gbc_guiLangCombo);
		
		langsPage = new JPanel();
		langsPage.setName(Messages.getString("SwingPreferencesWindowViewComponent.langsTab.name")); //$NON-NLS-1$
		pagesPane.add(langsPage, PreferencesPage.LANGUAGES.toString());
		langsPage.setVisible(false);
		langsPage.setBorder(new EmptyBorder(14, 14, 14, 14));
		GridBagLayout gbl_langsPage = new GridBagLayout();
		gbl_langsPage.columnWidths = new int[]{114, 0};
		gbl_langsPage.rowHeights = new int[]{0, 15, 25, 0};
		gbl_langsPage.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_langsPage.rowWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		langsPage.setLayout(gbl_langsPage);
		
		languagesExplanationTextArea = new JTextArea();
		languagesExplanationTextArea.setWrapStyleWord(true);
		languagesExplanationTextArea.setText(Messages.getString("preferences.program.languages.explanation")); //$NON-NLS-1$
		languagesExplanationTextArea.setMaximumSize(new Dimension(300, 2147483647));
		languagesExplanationTextArea.setLineWrap(true);
		languagesExplanationTextArea.setEditable(false);
		languagesExplanationTextArea.setBackground(UIManager.getColor("Button.background"));
		GridBagConstraints gbc_languagesExplanationTextArea = new GridBagConstraints();
		gbc_languagesExplanationTextArea.insets = new Insets(0, 0, 5, 0);
		gbc_languagesExplanationTextArea.fill = GridBagConstraints.BOTH;
		gbc_languagesExplanationTextArea.gridx = 0;
		gbc_languagesExplanationTextArea.gridy = 0;
		langsPage.add(languagesExplanationTextArea, gbc_languagesExplanationTextArea);
		
		langsPane = new JPanel();
		langsPane.setBorder(null);
		GridBagLayout gbl_langsPane = new GridBagLayout();
		gbl_langsPane.columnWidths = new int[]{0, 0, 0, 0};
		gbl_langsPane.rowHeights = new int[]{0, 0};
		gbl_langsPane.columnWeights = new double[]{1.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_langsPane.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		langsPane.setLayout(gbl_langsPane);
		
		JLabel langLabel = new JLabel(Messages.getString("preferences.program.languages.language")); //$NON-NLS-1$
		GridBagConstraints gbc_langLabel = new GridBagConstraints();
		gbc_langLabel.fill = GridBagConstraints.HORIZONTAL;
		gbc_langLabel.insets = new Insets(0, 0, 10, 14);
		gbc_langLabel.gridx = 0;
		gbc_langLabel.gridy = 0;
		langsPane.add(langLabel, gbc_langLabel);
		
		JLabel displayLabel = new JLabel(Messages.getString("preferences.program.languages.display")); //$NON-NLS-1$
		GridBagConstraints gbc_displayLabel = new GridBagConstraints();
		gbc_displayLabel.fill = GridBagConstraints.HORIZONTAL;
		gbc_displayLabel.insets = new Insets(0, 0, 10, 14);
		gbc_displayLabel.gridx = 1;
		gbc_displayLabel.gridy = 0;
		langsPane.add(displayLabel, gbc_displayLabel);
		GridBagConstraints gbc_langsPane = new GridBagConstraints();
		gbc_langsPane.insets = new Insets(0, 0, 5, 0);
		gbc_langsPane.anchor = GridBagConstraints.WEST;
		gbc_langsPane.gridx = 0;
		gbc_langsPane.gridy = 1;
		langsPage.add(langsPane, gbc_langsPane);
		
		addPane = new JPanel();
		addPane.setBorder(null);
		GridBagConstraints gbc_addPane = new GridBagConstraints();
		gbc_addPane.anchor = GridBagConstraints.WEST;
		gbc_addPane.fill = GridBagConstraints.VERTICAL;
		gbc_addPane.gridx = 0;
		gbc_addPane.gridy = 2;
		langsPage.add(addPane, gbc_addPane);
		
		langsAddField = new JTextField();
		addPane.add(langsAddField);
		langsAddField.setText("");
		langsAddField.setColumns(10);
		
		langAddButton = new JButton(Messages.getString("preferences.program.languages.buttons.add"));
		addPane.add(langAddButton);
		
		corpusPage = new JPanel();
		pagesPane.add(corpusPage, PreferencesPage.CORPUS.toString());
		corpusPage.setBorder(new EmptyBorder(14, 14, 14, 14));
		GridBagLayout gbl_profPage = new GridBagLayout();
		gbl_profPage.columnWidths = new int[]{157, 0, 0};
		gbl_profPage.rowHeights = new int[]{0, 0, 0, 0, 15, 0};
		gbl_profPage.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_profPage.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		corpusPage.setLayout(gbl_profPage);
		
		corpusExplanationTextArea = new JTextArea();
		corpusExplanationTextArea.setMaximumSize(new Dimension(300, 2147483647));
		GridBagConstraints gbc_corpusExplanationTextArea = new GridBagConstraints();
		gbc_corpusExplanationTextArea.fill = GridBagConstraints.HORIZONTAL;
		gbc_corpusExplanationTextArea.anchor = GridBagConstraints.WEST;
		gbc_corpusExplanationTextArea.gridwidth = 2;
		gbc_corpusExplanationTextArea.insets = new Insets(0, 0, 5, 0);
		gbc_corpusExplanationTextArea.gridx = 0;
		gbc_corpusExplanationTextArea.gridy = 0;
		corpusPage.add(corpusExplanationTextArea, gbc_corpusExplanationTextArea);
		corpusExplanationTextArea.setWrapStyleWord(true);
		corpusExplanationTextArea.setText(Messages.getString("preferences.program.corpus.explanation")); //$NON-NLS-1$
		corpusExplanationTextArea.setLineWrap(true);
		corpusExplanationTextArea.setEditable(false);
		corpusExplanationTextArea.setBackground(UIManager.getColor("Button.background"));
		
		corpusLink = new JLabel(Messages.getString("preferences.program.corpus.link")); //$NON-NLS-1$
		corpusLink.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				mediator.onRequestCorpusPage();
			}
		});
		corpusLink.setFont(new Font("Dialog", Font.PLAIN, 12));
		corpusLink.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		corpusLink.setForeground(new Color(0, 0, 255));
		GridBagConstraints gbc_corpusLink = new GridBagConstraints();
		gbc_corpusLink.gridwidth = 2;
		gbc_corpusLink.anchor = GridBagConstraints.WEST;
		gbc_corpusLink.insets = new Insets(0, 0, 5, 0);
		gbc_corpusLink.gridx = 0;
		gbc_corpusLink.gridy = 1;
		corpusPage.add(corpusLink, gbc_corpusLink);
		
		allowPane = new JPanel();
		allowPane.setBorder(null);
		GridBagConstraints gbc_allowPane = new GridBagConstraints();
		gbc_allowPane.anchor = GridBagConstraints.WEST;
		gbc_allowPane.gridwidth = 2;
		gbc_allowPane.insets = new Insets(0, 0, 5, 0);
		gbc_allowPane.fill = GridBagConstraints.VERTICAL;
		gbc_allowPane.gridx = 0;
		gbc_allowPane.gridy = 2;
		corpusPage.add(allowPane, gbc_allowPane);
		GridBagLayout gbl_allowPane = new GridBagLayout();
		gbl_allowPane.columnWidths = new int[]{0, 300, 0};
		gbl_allowPane.rowHeights = new int[]{0, 0, 0};
		gbl_allowPane.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_allowPane.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		allowPane.setLayout(gbl_allowPane);
		
		collectCorpusCheckBox = new JCheckBox("");
		collectCorpusCheckBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onCollectCorpusChange(collectCorpusCheckBox.isSelected());
			}
		});
		collectCorpusCheckBox.setIconTextGap(14);
		GridBagConstraints gbc_collectCorpusCheckBox = new GridBagConstraints();
		gbc_collectCorpusCheckBox.insets = new Insets(0, 0, 5, 5);
		gbc_collectCorpusCheckBox.gridx = 0;
		gbc_collectCorpusCheckBox.gridy = 0;
		allowPane.add(collectCorpusCheckBox, gbc_collectCorpusCheckBox);
		
		CorpusCollectLabel = new JTextArea();
		CorpusCollectLabel.setEditable(false);
		CorpusCollectLabel.setBackground(UIManager.getColor("Label.background"));
		CorpusCollectLabel.setWrapStyleWord(true);
		CorpusCollectLabel.setLineWrap(true);
		CorpusCollectLabel.setText(Messages.getString("preferences.program.corpus.collect")); //$NON-NLS-1$
		GridBagConstraints gbc_CorpusCollectLabel = new GridBagConstraints();
		gbc_CorpusCollectLabel.insets = new Insets(0, 0, 5, 0);
		gbc_CorpusCollectLabel.fill = GridBagConstraints.BOTH;
		gbc_CorpusCollectLabel.gridx = 1;
		gbc_CorpusCollectLabel.gridy = 0;
		allowPane.add(CorpusCollectLabel, gbc_CorpusCollectLabel);
		
		lblNewLabel_1 = new JLabel(Messages.getString("preferences.program.corpus.default.translation")); //$NON-NLS-1$
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 3;
		corpusPage.add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		panel_1 = new JPanel();
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel_1.gridx = 1;
		gbc_panel_1.gridy = 3;
		corpusPage.add(panel_1, gbc_panel_1);
		
		
		adaptationRadiobutton = new JRadioButton(Messages.getString("preferences.program.corpus.default.translation.adaptation"));
		adaptationRadiobutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onExactCorpusChange(false);
			}
		});
		buttonGroup.add(adaptationRadiobutton);
		
		exactRadiobutton = new JRadioButton(Messages.getString("toolbar.exact.checkbox"));
		exactRadiobutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onExactCorpusChange(true);
			}
		});
		buttonGroup.add(exactRadiobutton);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.X_AXIS));
		panel_1.add(adaptationRadiobutton);
		panel_1.add(exactRadiobutton);
		
		profPane = new JPanel();
		profPane.setBorder(null);
		GridBagConstraints gbc_profPane = new GridBagConstraints();
		gbc_profPane.insets = new Insets(7, 0, 0, 0);
		gbc_profPane.gridwidth = 2;
		gbc_profPane.anchor = GridBagConstraints.NORTHWEST;
		gbc_profPane.gridx = 0;
		gbc_profPane.gridy = 4;
		corpusPage.add(profPane, gbc_profPane);
		GridBagLayout gbl_profPane = new GridBagLayout();
		gbl_profPane.columnWidths = new int[]{0, 0, 0};
		gbl_profPane.rowHeights = new int[]{0, 0};
		gbl_profPane.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_profPane.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		profPane.setLayout(gbl_profPane);
		
		profLangLabel = new JLabel("Language");
		GridBagConstraints gbc_profLangLabel = new GridBagConstraints();
		gbc_profLangLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_profLangLabel.insets = new Insets(0, 0, 10, 14);
		gbc_profLangLabel.gridx = 0;
		gbc_profLangLabel.gridy = 0;
		profPane.add(profLangLabel, gbc_profLangLabel);
		
		proficiencyLabel = new JLabel("Proficiency");
		GridBagConstraints gbc_proficiencyLabel = new GridBagConstraints();
		gbc_proficiencyLabel.insets = new Insets(0, 0, 10, 14);
		gbc_proficiencyLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_proficiencyLabel.gridx = 1;
		gbc_proficiencyLabel.gridy = 0;
		profPane.add(proficiencyLabel, gbc_proficiencyLabel);
		
		processingPage = new JPanel();
		pagesPane.add(processingPage, PreferencesPage.PROCESSING.toString());
		GridBagLayout gbl_processingPage = new GridBagLayout();
		gbl_processingPage.columnWidths = new int[]{424, 0};
		gbl_processingPage.rowHeights = new int[]{0, 106, 0};
		gbl_processingPage.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gbl_processingPage.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		processingPage.setLayout(gbl_processingPage);
		
		processingLink = new JLabel(Messages.getString("preferences.processing.link")); //$NON-NLS-1$
		processingLink.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				mediator.onRequestProcessingPage();
			}
		});
		
		lblNewLabel = new JLabel(Messages.getString("preferences.processing.explanation")); //$NON-NLS-1$
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		processingPage.add(lblNewLabel, gbc_lblNewLabel);
		processingLink.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		processingLink.setFont(new Font("Dialog", Font.PLAIN, 12));
		GridBagConstraints gbc_processingLink = new GridBagConstraints();
		gbc_processingLink.fill = GridBagConstraints.HORIZONTAL;
		gbc_processingLink.anchor = GridBagConstraints.NORTH;
		gbc_processingLink.weighty = 1.0;
		gbc_processingLink.gridx = 0;
		gbc_processingLink.gridy = 1;
		processingPage.add(processingLink, gbc_processingLink);
		
		collectPage = new JPanel();
		pagesPane.add(collectPage, PreferencesPage.COLLECT.toString());
		GridBagLayout gbl_collectPage = new GridBagLayout();
		gbl_collectPage.columnWidths = new int[]{1, 0, 0};
		gbl_collectPage.rowHeights = new int[]{1, 0, 0};
		gbl_collectPage.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gbl_collectPage.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		collectPage.setLayout(gbl_collectPage);
		
		collectTable = new JTable();
		collectTable.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
			}
		));
		collectTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		GridBagConstraints gbc_table = new GridBagConstraints();
		gbc_table.gridwidth = 2;
		gbc_table.insets = new Insets(0, 0, 5, 5);
		gbc_table.fill = GridBagConstraints.BOTH;
		gbc_table.gridx = 0;
		gbc_table.gridy = 0;
		collectPage.add(collectTable, gbc_table);
		
		collectCreateButton = new JButton(Messages.getString("preferences.processing.create.row")); //$NON-NLS-1$
		collectCreateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = collectTable.getRowCount();
				mediator.createCollect(i);
			}
		});
		GridBagConstraints gbc_collecrCreateButton = new GridBagConstraints();
		gbc_collecrCreateButton.anchor = GridBagConstraints.EAST;
		gbc_collecrCreateButton.insets = new Insets(0, 0, 0, 5);
		gbc_collecrCreateButton.gridx = 0;
		gbc_collecrCreateButton.gridy = 1;
		collectPage.add(collectCreateButton, gbc_collecrCreateButton);
		
		collectRemoveButton = new JButton(Messages.getString("preferences.processing.remove.row")); //$NON-NLS-1$
		collectRemoveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = collectTable.getSelectedRow();
				if (i != -1) mediator.removeCollect(i);
			}
		});
		GridBagConstraints gbc_collectRemoveButton = new GridBagConstraints();
		gbc_collectRemoveButton.anchor = GridBagConstraints.EAST;
		gbc_collectRemoveButton.gridx = 1;
		gbc_collectRemoveButton.gridy = 1;
		collectPage.add(collectRemoveButton, gbc_collectRemoveButton);
		
		replacePage = new JPanel();
		pagesPane.add(replacePage, PreferencesPage.REPLACE.toString());
		GridBagLayout gbl_replacePage = new GridBagLayout();
		gbl_replacePage.columnWidths = new int[]{1, 0, 0};
		gbl_replacePage.rowHeights = new int[]{1, 0, 0};
		gbl_replacePage.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gbl_replacePage.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		replacePage.setLayout(gbl_replacePage);
		
		replaceTable = new JTable();
		replaceTable.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
			}
		));
		GridBagConstraints gbc_replaceTable = new GridBagConstraints();
		gbc_replaceTable.gridwidth = 2;
		gbc_replaceTable.insets = new Insets(0, 0, 5, 5);
		gbc_replaceTable.fill = GridBagConstraints.BOTH;
		gbc_replaceTable.gridx = 0;
		gbc_replaceTable.gridy = 0;
		replacePage.add(replaceTable, gbc_replaceTable);
		
		replaceCreateButton = new JButton(Messages.getString("preferences.processing.create.row")); //$NON-NLS-1$
		replaceCreateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = replaceTable.getRowCount();
				mediator.createReplace(i);
			}
		});
		GridBagConstraints gbc_replaceCreateButton = new GridBagConstraints();
		gbc_replaceCreateButton.anchor = GridBagConstraints.EAST;
		gbc_replaceCreateButton.insets = new Insets(0, 0, 0, 5);
		gbc_replaceCreateButton.gridx = 0;
		gbc_replaceCreateButton.gridy = 1;
		replacePage.add(replaceCreateButton, gbc_replaceCreateButton);
		
		JButton replaceRemoceButton = new JButton(Messages.getString("preferences.processing.remove.row")); //$NON-NLS-1$
		replaceRemoceButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = replaceTable.getSelectedRow();
				if (i != -1) mediator.removeReplace(i);
			}
		});
		GridBagConstraints gbc_replaceRemoceButton = new GridBagConstraints();
		gbc_replaceRemoceButton.anchor = GridBagConstraints.EAST;
		gbc_replaceRemoceButton.gridx = 1;
		gbc_replaceRemoceButton.gridy = 1;
		replacePage.add(replaceRemoceButton, gbc_replaceRemoceButton);
		
		emptyPage = new JPanel();
		pagesPane.add(emptyPage, PreferencesPage.NONE.toString());
		langAddButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mediator.onAddLanguage(langsAddField.getText());
				langsAddField.setText("");
			}
		});
	}
}
