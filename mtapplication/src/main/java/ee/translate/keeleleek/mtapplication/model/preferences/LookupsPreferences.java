package ee.translate.keeleleek.mtapplication.model.preferences;

import java.util.ArrayList;
import java.util.List;

public class LookupsPreferences {

	private ArrayList<Lookup> lookups;

	
	// INIT
	public LookupsPreferences()
	 {
		lookups = new ArrayList<>();
	 }
	

	public LookupsPreferences(ArrayList<Lookup> lookups)
	 {
		this.lookups = new ArrayList<>(lookups);
	 }

	public LookupsPreferences(LookupsPreferences other)
	 {
		lookups = new ArrayList<>(other.lookups);
	 }
	
	public static LookupsPreferences create()
	 {
		ArrayList<Lookup> lookups = new ArrayList<>();

		// Thesaurus
		lookups.add(new Lookup(
				"Thesaurus",
				"http://www.dictionaryapi.com/api/v1/references/thesaurus/xml/<search>",
				"",
				"<entry id=~>~<hw>#</hw>~<fl>#</fl>~<mc>#</mc>~<syn>#</syn>~<rel>#</rel>~</entry>",
				"",
				"<div><h3>#</h3> (#)<br>#<br><b>Synonyms:</b> #<br><strong>Related:</strong> #</div><br>",
				""
			));
		
		String ekiStyle = 
				"<style>\n"
				+ " .tervikart { margin-top: 1em; }\n"
				+ " .c { font-style: italic; }\n"
				+ " .m { font-weight: bold; font-family: Arial; }\n"
				+ " .n { font-style: italic; }\n"
				+ " .s { font-variant: small-caps; }\n"
				+ " .v { font-variant: small-caps; }\n"
				+ " .ld { font-style: italic; }\n"
				+ " .ls { color: #000080; }\n"
				+ " .tg_s_att { font-weight: bold; }\n"
				+ " .tp_s_tnr { font-weight: bold; }\n"
				+ " .gki { font-family: Arial Narrow; font-size: x-small; }\n"
				+ " .m_s_i { vertical-align: super; }\n"
				+ " .m_kno { font-weight: bold; font-family: Arial; }\n"
				+ " .hld_kno  { font-style:italic; }\n"
				+ " .k_kno { font-size: x-small; }\n"
				+ " .d_kno { font-style: italic; }\n"
				+ ".ms, .eb, .fms { font-weight: bold; }\n" +
				"</style>";
		
		// Eesti keele seletav s\u00F5naraamat
		lookups.add(new Lookup(
				"Eesti keele seletav s\u00F5naraamat",
				"http://eki.ee/dict/ekss/index.cgi?Q=<search>&F=M",
				"",
				"<div class=\"tervikart\">#</div>",
				ekiStyle,
				"<div class=\"tervikart\">#</div>",
				""
			));

		// Eesti keele \u00F5igekeelsuss\u00F5naraamat
		lookups.add(new Lookup(
				"Eesti \u00F5igekeelsuss\u00F5naraamat",
				"http://www.eki.ee/dict/qs/index.cgi?Q=<search>&F=M",
				"",
				"<div class=\"tervikart\">#</div>",
				ekiStyle,
				"<div class=\"tervikart\">#</div>",
				""
			));

		// Russian-Estonian dictionary
		lookups.add(new Lookup(
				"Russian-Estonian dictionary",
				"http://www.eki.ee/dict/ves/index.cgi?Q=<search>&F=M",
				"",
				"<div class=\"tervikart\">#</div>",
				ekiStyle,
				"<div class=\"tervikart\">#</div>",
				""
			));
		
		// English-Estonian MT dictionary
		lookups.add(new Lookup(
				"English-Estonian MT dictionary",
				"http://www.eki.ee/dict/ies/index.cgi?Q=<search>&F=M",
				"",
				"<div class=\"tervikart\">#</div>",
				ekiStyle,
				"<div class=\"tervikart\">#</div>",
				""
			));

		// Ladina-Eesti s\u00F5nastik
		lookups.add(new Lookup(
				"Ladina-Eesti s\u00F5nastik",
				"http://www.ut.ee/klassik/dict/dict.cgi?verbum=<search>&lingua=el",
				"",
				"<dl>#</dl>",
				ekiStyle,
				"<div>#</div>",
				""
			));

		// Wiki sitelinks (enwiki)
		lookups.add(new Lookup(
				"Wiki sitelinks (enwiki)",
				"https://www.wikidata.org/w/api.php?action=wbgetentities&sites=enwiki&titles=<search>&props=sitelinks&format=xml",
				"",
				"<sitelink site=\"#\" title=\"#\">~</sitelink>",
				"",
				"<div><b>#</b> #</div>",
				""
			));
				
		return new LookupsPreferences(lookups);
	 }
	

	// LOOKUPS
	public Lookup getLookup(int i)
	 {
		return lookups.get(i);
	 }
	
	public Lookup findLookup(String name)
	 {
		for (Lookup lookup : this.lookups) {
			if (lookup.getName().equals(name)) return lookup;
		}
		return null;
	 }

	public int getLookupCount()
	 {
		return lookups.size();
	 }

	public List<String> getLookupNames()
	 {
		List<String> names = new ArrayList<>();
		for (Lookup lookup : this.lookups) {
			names.add(lookup.getName());
		}
		return names;
	 }
	
	
	// HELPERS
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((lookups == null) ? 0 : lookups.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		LookupsPreferences other = (LookupsPreferences) obj;
		if (lookups == null) {
			if (other.lookups != null) return false;
		} else if (!lookups.equals(other.lookups)) return false;
		return true;
	}

	
}
