package ee.translate.keeleleek.mtapplication.view.suggestions;

public interface Suggestable {

	/**
	 * Makes suggestions. 
	 * 
	 * @param results suggestion results
	 */
	public void suggest(String[] results);
	
	/**
	 * Updates value.
	 * 
	 * @param suggested suggested value
	 */
	void update(String suggested);
	
	/**
	 * Search term.
	 * 
	 */
	String getSearchTerm();
	
}
