package ee.translate.keeleleek.mtapplication.view.swing.dialogs;

import ee.translate.keeleleek.mtapplication.view.dialogs.ManualDialogMediator;
import ee.translate.keeleleek.mtapplication.view.swing.swingview.SwingManualWindowViewComponent;
import javafx.fxml.FXML;
import javafx.scene.web.WebView;


public class ManualDialogMediatorSwing extends ManualDialogMediator {

	@FXML
	private WebView programWebView;
	@FXML
	private WebView wikitextWebView;
	
	protected String programHTML = "";
	protected String wikitextHTML = "";
	
	
	// INITIATION:
	public ManualDialogMediatorSwing() {
	}
	
	
	// IMPLEMENTATION:
	@Override
	public SwingManualWindowViewComponent getViewComponent() {
		return (SwingManualWindowViewComponent) super.getViewComponent();
	}
	
	@Override
	public void onRegister()
	 {
		super.onRegister();
	 }

	@Override
	protected void loadProgramPage(String html) {
		this.programHTML = html;
		getViewComponent().programEditorPane.setText(html);
	}
	
	@Override
	protected void loadWikitextPage(String html) {
		this.wikitextHTML = html;
		getViewComponent().wikitextEditorPane.setText(html);
	}
	
}
