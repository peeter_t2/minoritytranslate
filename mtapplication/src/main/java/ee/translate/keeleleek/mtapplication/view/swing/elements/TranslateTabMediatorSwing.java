package ee.translate.keeleleek.mtapplication.view.swing.elements;

import java.awt.CardLayout;
import java.awt.Component;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import ee.translate.keeleleek.mtapplication.common.requests.FindRequest;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteChoices;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.Status;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.TitleStatus;
import ee.translate.keeleleek.mtapplication.model.media.IconsProxySwing;
import ee.translate.keeleleek.mtapplication.model.preferences.Symbol;
import ee.translate.keeleleek.mtapplication.view.elements.TranslateTabMediator;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtapplication.view.swing.swingview.SwingTranslateTabViewComponent;
import ee.translate.keeleleek.mtpluginframework.spellcheck.Misspell;


public class TranslateTabMediatorSwing extends TranslateTabMediator {
	
	public static class SwingTabObject { public int i; public JTabbedPane pane; }
	

	boolean feedback = false;
	
	
	// INIT
	@Override
	public SwingTabObject getTabObject() {
		return (SwingTabObject) this.tabObject;
	}
	
	@Override
	public SwingTranslateTabViewComponent getViewComponent() {
		return (SwingTranslateTabViewComponent) super.getViewComponent();
	}
	
	@Override
	public void initialise() {
		super.initialise();

		getViewComponent().titleEdit.getDocument().addDocumentListener(new DocumentListener()
		 {
			@Override
			public void removeUpdate(DocumentEvent e) {
				if (!feedback) onTitleEdited();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				if (!feedback) onTitleEdited();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				if (!feedback) onTitleEdited();
			}
		 });

		getViewComponent().textEdit.getDocument().addDocumentListener(new DocumentListener()
		 {
			@Override
			public void removeUpdate(DocumentEvent e) {
				if (!feedback) onTextEdited();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				if (!feedback) onTextEdited();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				if (!feedback) onTextEdited();
			}
		 });

		CardLayout barLayout = (CardLayout)(getViewComponent().bar.getLayout());
		barLayout.show(getViewComponent().bar, mode().toString());
	}
	
	
	// IMPLEMENTATION:
	@Override
	public void onRegister()
	 {
		super.onRegister();
	 }

	
	
	// INHERIT (CONTENT)
	@Override
	public String getTitle() {
		return getViewComponent().titleEdit.getText();
	}
	
	@Override
	public String getText() {
		return getViewComponent().textEdit.getText();
	}
	@Override
	public void setTitleStatus(TitleStatus status) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void setName(String name) {
		int i = getTabObject().i;
		getTabObject().pane.setTitleAt(i, name);
	}
	
	@Override
	public void setTitle(String title) {
		feedback = true;
		getViewComponent().titleEdit.setText(title);
		feedback = false;
	}
	
	@Override
	public void setText(String text) {
		feedback = true;
		getViewComponent().textEdit.setText(text);
		feedback = false;
	}

	@Override
	public void setStatus(Status status)
	 {
		if (status != null) {

			String text = Messages.getString("statusbar." + status.toString().toLowerCase().replace('_', '.'));
			getViewComponent().setToolTipText(text);
			
			IconsProxySwing icons = (IconsProxySwing) MinorityTranslateModel.icons();
			ImageIcon icon = icons.getStatusIcon(status);
			getViewComponent().statusLabel.setIcon(icon);

			getViewComponent().statusLabel.setVisible(true);
			
		} else {
			getViewComponent().statusLabel.setText("");
			getViewComponent().statusLabel.setVisible(false);
		}
		
		boolean disable = status == null;
		getViewComponent().titleEdit.setEnabled(!disable);
		getViewComponent().textEdit.setEnabled(!disable);
//		getViewComponent().filterTemplatesCheckbox.setEnabled(!disable);
//		getViewComponent().filterFilesCheckbox.setEnabled(!disable);
//		getViewComponent().filterIntroductionCheckbox.setEnabled(!disable);
//		getViewComponent().filterReferencesCheckbox.setEnabled(!disable);
		
		boolean editable = status == Status.STANDBY && mode() == TabMode.NORMAL;
		getViewComponent().titleEdit.setEditable(editable && isNnew());
		getViewComponent().textEdit.setEditable(editable);
		
		getViewComponent().exactCheckbox.setEnabled(editable && !disable);
	 }
	
	@Override
	public void setPreview(String html) {
		getViewComponent().previewView.setText(html);
	}
	
	@Override
	public void setView(TabView view)
	 {
		CardLayout editLayout = (CardLayout)(getViewComponent().middleBox.getLayout());
		editLayout.show(getViewComponent().middleBox, view.toString());
		
		switch (view) {
		case EDIT:
			getViewComponent().textEdit.requestFocus();
			getViewComponent().previewButton.setSelected(false);
			break;

		case PREVIEW:
			getViewComponent().previewView.requestFocus();
			getViewComponent().previewButton.setSelected(true);
			break;

		default:
			break;
		}
	 }
	
	
	@Override
	public void setExact(boolean exact) {
		getViewComponent().exactCheckbox.setSelected(exact);
	}

	@Override
	protected void setSymbols(List<Symbol> symbols) {
		// TODO Swing
	}
	

	@Override
	protected void setFilterTemplates(boolean filter) {
		getViewComponent().filterTemplatesCheckbox.setSelected(filter);
	}
	
	@Override
	protected void setFilterFiles(boolean filter) {
		getViewComponent().filterFilesCheckbox.setSelected(filter);
	}
	
	@Override
	protected void setFilterIntroduction(boolean filter) {
		getViewComponent().filterIntroductionCheckbox.setSelected(filter);
	}
	
	@Override
	protected void setFilterReferences(boolean filter) {
		getViewComponent().filterReferencesCheckbox.setSelected(filter);
	}
	
	
	@Override
	public boolean isEditFocused()
	 {
		JFrame window = (JFrame) SwingUtilities.getWindowAncestor(getViewComponent());
		if (window == null) return false;
		Component focused = window.getFocusOwner();
		if (focused == null) return false;
		
		return getViewComponent().textEdit == focused || getViewComponent().previewView == focused;
	 }
	

	@Override
	public void pasteText(String text)
	 {
		if (!getViewComponent().textEdit.isFocusOwner()) return;
//		int c = getViewComponent().textEdit.getCaretPosition();
		getViewComponent().textEdit.replaceSelection(text);
//		getViewComponent().textEdit.setCaretPosition(c + text.length());
	 }
	

	// INHERIT (FORMAT)
	public void format() {
		// TODO Swing
	};
	
	@Override
	public void showAutocomplete(AutocompleteChoices autocomplete) {
		// TODO Swing autocomplete
	}
	
	
	// SPELLCHECK
	@Override
	public void markMisspells(List<Misspell> misspells) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void replaceText(String text) {
		// TODO Auto-generated method stub
	}
	@Override
	public void find(FindRequest request) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public String getSpellerPluginName() {
		return "none";
	}
	
}
