package ee.translate.keeleleek.mtapplication.view.windows;

import java.awt.Color;
import java.util.List;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.requests.ContentRequest;
import ee.translate.keeleleek.mtapplication.common.requests.FindRequest;
import ee.translate.keeleleek.mtapplication.common.requests.PullRequest;
import ee.translate.keeleleek.mtapplication.common.requests.PullResponse;
import ee.translate.keeleleek.mtapplication.common.requests.SrcDstRequest;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteChoices;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.Status;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.WikiType;
import ee.translate.keeleleek.mtapplication.model.content.Reference;
import ee.translate.keeleleek.mtapplication.model.preferences.PersonalisationPreferences;
import ee.translate.keeleleek.mtapplication.view.application.ApplicationMediator;
import ee.translate.keeleleek.mtapplication.view.dialogs.DialogFactory;
import ee.translate.keeleleek.mtapplication.view.editors.AlignEditorMediator;
import ee.translate.keeleleek.mtapplication.view.elements.TranslateTabMediator;
import ee.translate.keeleleek.mtapplication.view.elements.TranslateTabMediator.TabMode;
import ee.translate.keeleleek.mtapplication.view.elements.TranslateTabMediator.TabView;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import net.sourceforge.jwbf.mediawiki.bots.MediaWikiBot;


public abstract class TranslateWindowMediator extends Mediator {

	final public static String NAME = "{F0A8D0ED-D5B8-4F14-9C8B-FCEA612A1D30}";
	
	public static enum Editor { TRANSLATE, ALIGN };
	public static enum TabsBox { UPPER, LOWER };

	
	private String[] upperLangCodes = new String[0];
	private String[] lowerLangCodes = new String[0];
	
	protected AlignEditorMediator alignEditor = null;
	
	protected boolean feedback = false;

	
	// INIT
	public TranslateWindowMediator() {
		super(NAME, null);
	}
	
	@Override
	public void onRegister()
	 {
		upperLangCodes = MinorityTranslateModel.preferences().getDstLangCodes();
		lowerLangCodes = MinorityTranslateModel.preferences().getSrcLangCodes();

		getFacade().registerMediator(createArticlesMediator());
		getFacade().registerMediator(createNotesMediator());

		showSpellCheckerBusy(false);
		showQueuerBusy(false);
		showDownloaderBusy(false);
		showPreviewerBusy(false);
		showUploaderBusy(false);
		showSessionSaveBusy(false);
		showPullingBusy(false);
		showCheckingTitleBusy(false);
	 }

	public abstract Mediator createArticlesMediator();
	public abstract Mediator createNotesMediator();
	

	// NOTIFICATIONS
	@Override
	public String[] listNotificationInterests() {
		return new String[]
		 {

			Notifications.WINDOW_TRANSLATE_VISIBLE,
			Notifications.ENGINE_SELECTED_ARTICLE_CHANGED,
			Notifications.ENGINE_ARTICLE_ADDED,
			Notifications.ENGINE_ARTICLE_STATUS_CHANGED,
			Notifications.ENGINE_ARTICLE_TITLE_STATUS_CHANGED,
			Notifications.ENGINE_ARTICLE_TITLE_CHANGED,
			Notifications.ENGINE_ARTICLE_TEXT_CHANGED,
			Notifications.ENGINE_ARTICLE_DOWNLOAD_INITIALISED,
			Notifications.ENGINE_ARTICLE_PROCESSED,
			Notifications.ENGINE_ARTICLE_PREVIEW_CHANGED,
			Notifications.ENGINE_ARTICLE_FILTERED_PREVIEW_CHANGED,
			Notifications.PREFERENCES_FILTERS_CHANGED,
			Notifications.PREFERENCES_LANGUAGES_CHANGED,
			Notifications.PREFERENCES_SYMBOLS_CHANGED,
			Notifications.SPELL_CHECKER_BUSY,
			Notifications.ENGINE_QUERER_BUSY,
			Notifications.ENGINE_DOWNLOADER_BUSY,
			Notifications.ENGINE_PREVIEWER_BUSY,
			Notifications.ENGINE_UPLOADER_BUSY,
			Notifications.ENGINE_ARTICLE_EXACT_CHANGED,
			Notifications.SELECTED_ARTICLE_CHANGED,
			Notifications.PROCESSING_PASTE,
			Notifications.ARTICLE_LOADED,
			Notifications.ARTICLE_REMOVED,
			Notifications.DOWNLOADER_STARTED,
			Notifications.DOWNLOADER_PROGRESS_CHANGED,
			Notifications.DOWNLOADER_ENDED,
			Notifications.UPLOADER_STARTED,
			Notifications.UPLOADER_PROGRESS_CHANGED,
			Notifications.UPLOADER_ENDED,
			Notifications.READY_CHANGED,
			Notifications.LOGIN_STATUS_CHANGED,
			Notifications.CONNECTION_STATUS_CHANGED,
			Notifications.SESSION_LOADED,
			Notifications.SESSION_SAVING_BUSY,
			Notifications.SESSION_TAG_CHANGED,
			Notifications.READY_BUSY_CHANGED,
			Notifications.ALIGN_EDITOR_OPENED,
			Notifications.ALIGN_EDITOR_CLOSED,
			Notifications.ALIGN_EDITOR_APPLIED,
			Notifications.SHOW_AUTOCOMPLETE,
			Notifications.SPELLCHECK_COMPLETE,
			Notifications.PULL_COMPLETE,
			Notifications.PULLING_BUSY,
			Notifications.ENGINE_TITLE_CHECK_BUSY,
			Notifications.QUITTING_COMMIT,
			Notifications.FIND_FROM_ACTIVE_TAB
		};
	}
	
	@Override
	public void handleNotification(INotification notification)
	 {
		feedback = true;
		
		Reference ref;
		boolean busy;
		String text;
		TranslateTabMediator tab;
		
		switch (notification.getName()) {

		case Notifications.WINDOW_TRANSLATE_VISIBLE:
			setAddonsDivider(MinorityTranslateModel.preferences().getAddonsDivider());
			setTranslateDivider(MinorityTranslateModel.preferences().getTranslateDivider());
			break;
			
		case Notifications.ENGINE_SELECTED_ARTICLE_CHANGED:
			refreshNavigationControls();
			refreshUploadControls();
			refreshProcessingControls();
			refreshAllTabs();
			break;

		case Notifications.ENGINE_ARTICLE_DOWNLOAD_INITIALISED:
		case Notifications.ENGINE_ARTICLE_PROCESSED:
			ref = (Reference) notification.getBody();
			refreshTab(ref);
			break;
		
		case Notifications.PULL_COMPLETE:
			PullResponse response = (PullResponse) notification.getBody();
			tab = findTab(response.getDstRef());
			if (tab != null) tab.replaceText(response.getText());
			break;

		case Notifications.ENGINE_ARTICLE_STATUS_CHANGED:
		case Notifications.ENGINE_ARTICLE_TITLE_STATUS_CHANGED:
			ref = (Reference) notification.getBody();
			refreshTabStatus(ref);
			refreshUploadControls();
			refreshProcessingControls();
			refreshCounts();
			break;

		case Notifications.ENGINE_ARTICLE_PREVIEW_CHANGED:
		case Notifications.ENGINE_ARTICLE_FILTERED_PREVIEW_CHANGED:
			ref = (Reference) notification.getBody();
			refreshTabPreview(ref);
			break;
			
		case Notifications.ENGINE_SELECTABLE_ITEMS_CHANGED:
			refreshNavigationControls();
			break;
			
		case Notifications.ENGINE_ARTICLE_EXACT_CHANGED:
			ref = (Reference) notification.getBody();
			tab = findTab(ref);
			if (tab != null) tab.fetchExact();
			break;
			
		case Notifications.SESSION_LOADED:
			refreshNavigationControls();
			refreshUploadControls();
			refreshProcessingControls();
			refreshAllTabs();
			refreshLogin();
			refreshTabFilters();
			refreshTabSymbols();
			refreshWikis();
			refreshTag();
			refreshCounts();

			showSpellCheckerBusy(false);
			showQueuerBusy(false);
			showDownloaderBusy(false);
			showPreviewerBusy(false);
			showUploaderBusy(false);
			showSessionSaveBusy(false);
			showPullingBusy(false);
			showCheckingTitleBusy(false);
			
			if (MinorityTranslateModel.align().isOpen()) openEditor(Editor.ALIGN);
			else openEditor(Editor.TRANSLATE);
			
			break;

		case Notifications.SESSION_TAG_CHANGED:
			refreshTag();
			break;
			
		case Notifications.SPELL_CHECKER_BUSY:
			busy = (boolean) notification.getBody();
			showSpellCheckerBusy(busy);
			break;
			
		case Notifications.ENGINE_QUERER_BUSY:
			busy = (boolean) notification.getBody();
			showQueuerBusy(busy);
			break;

		case Notifications.ENGINE_DOWNLOADER_BUSY:
			busy = (boolean) notification.getBody();
			showDownloaderBusy(busy);
			break;

		case Notifications.ENGINE_PREVIEWER_BUSY:
			busy = (boolean) notification.getBody();
			showPreviewerBusy(busy);
			break;

		case Notifications.ENGINE_UPLOADER_BUSY:
			busy = (boolean) notification.getBody();
			showUploaderBusy(busy);
			break;

		case Notifications.SESSION_SAVING_BUSY:
			busy = (boolean) notification.getBody();
			showSessionSaveBusy(busy);
			break;

		case Notifications.PULLING_BUSY:
			busy = (boolean) notification.getBody();
			showPullingBusy(busy);
			break;

		case Notifications.ENGINE_TITLE_CHECK_BUSY:
			busy = (boolean) notification.getBody();
			showCheckingTitleBusy(busy);
			break;

		case Notifications.PROCESSING_PASTE:
			text = (String) notification.getBody();
			tab = getSelectedTab(TabsBox.UPPER);
			if (tab != null) tab.pasteText(text);
			break;

		case Notifications.PREFERENCES_LANGUAGES_CHANGED:
			upperLangCodes = MinorityTranslateModel.preferences().getDstLangCodes();
			lowerLangCodes = MinorityTranslateModel.preferences().getSrcLangCodes();
			refreshLogin();
			refreshWikis();
			refreshAllTabs();
			refreshCounts();
			refreshTabSymbols();
			break;
			
		case Notifications.PREFERENCES_SYMBOLS_CHANGED:
			refreshTabSymbols();
			break;

		case Notifications.PREFERENCES_FILTERS_CHANGED:
			ref = (Reference) notification.getBody();
			refreshTabFilters();
			break;
			
		case Notifications.ARTICLE_LOADED:
		case Notifications.ARTICLE_REMOVED:
			break;

		case Notifications.LOGIN_STATUS_CHANGED:
		case Notifications.CONNECTION_STATUS_CHANGED:
			refreshLogin();
			break;
		
		case Notifications.DOWNLOADER_STARTED:
			setProgress(0.0);
			setProgressVisible(true);
			break;
				
		case Notifications.DOWNLOADER_PROGRESS_CHANGED:
			Double progress = (Double) notification.getBody();
			setProgress(progress);
			break;
				
		case Notifications.DOWNLOADER_ENDED:
			setProgressVisible(false);
			break;

		case Notifications.UPLOADER_STARTED:
			setProgress(0.0);
			setProgressVisible(true);
			break;
				
		case Notifications.UPLOADER_PROGRESS_CHANGED:
			progress = (Double) notification.getBody();
			setProgress(progress);
			break;
				
		case Notifications.UPLOADER_ENDED:
			setProgressVisible(false);
			break;
			
		case Notifications.ENGINE_ARTICLE_TITLE_CHANGED:
		case Notifications.ENGINE_ARTICLE_TEXT_CHANGED:
			refreshCounts();
			break;

		case Notifications.ALIGN_EDITOR_OPENED:
			openEditor(Editor.ALIGN);
			break;

		case Notifications.ALIGN_EDITOR_CLOSED:
			openEditor(Editor.TRANSLATE);
			break;

		case Notifications.ALIGN_EDITOR_APPLIED:
			refreshAllTabs();
			break;

		case Notifications.QUITTING_COMMIT:
			PersonalisationPreferences personalisation = MinorityTranslateModel.preferences().personalisation();
			personalisation.setAddonsDivider(getAddonsDivider());
			personalisation.setTranslateDivider(getTranslateDivider());
			sendNotification(Notifications.PREFERENCES_CHANGE_PERSONALISATION, personalisation);
			break;

		case Notifications.SHOW_AUTOCOMPLETE:
			AutocompleteChoices autocomplete = (AutocompleteChoices) notification.getBody();
			switch (getEditor()) {
			case TRANSLATE:
				ref = autocomplete.getRef();
				tab = findTab(ref);
				if (tab != null) tab.showAutocomplete(autocomplete);
				break;
				
			case ALIGN:
				if (alignEditor != null) alignEditor.showAutocomplete(autocomplete);
				break;

			default:
				break;
			}
			
			break;

		case Notifications.SPELLCHECK_COMPLETE:
			tab = findTab((Reference) notification.getBody());
			if (tab != null) tab.fetchSpellcheck();
			break;

		case Notifications.FIND_FROM_ACTIVE_TAB:
			FindRequest request = (FindRequest) notification.getBody();
			tab = getSelectedTab(TabsBox.UPPER);
			if (tab != null) tab.find(request);
			break;

		default:
			break;
		}
		
		feedback = false;
	 }
	
	
	// REFRESH (TABS)
	private void refreshTab(TranslateTabMediator tab, Reference ref)
	 {
		if (tab == null) return;
		
		tab.changeReference(ref);
		tab.fetchContent();
		tab.fetchStatus();
		tab.fetchTitleStatus();
	 }
	
	private void refreshTab(Reference ref)
	 {
		refreshTab(findTab(ref), ref);
	 }

	
	protected void refreshAllTabs(TabsBox box)
	 {
		String[] langCodes = getLangCodes(box);
		
		// number of tabs
		while (getTabCount(box) < langCodes.length) {
			TranslateTabMediator mediator = createTab(box);
			mediator.initialise(box == TabsBox.UPPER ? TabMode.NORMAL : TabMode.FILTERED);
		}
		while (getTabCount(box) > langCodes.length){
			destroyTab(box);
		}
		
		String qid = MinorityTranslateModel.content().getSelectedQid();
		Reference ref = null;
		
		for (int i = 0; i < langCodes.length; i++) {
			
			String langCode = langCodes[i];
			TranslateTabMediator tab = getTab(box, i);
			
			String tabName = langCode;
			String langName = MinorityTranslateModel.wikis().getLangName(langCode);
			if (langName != null) tabName = langName + " (" + tabName + ")";
			tab.setName(tabName);
			tab.setLangCode(langCode);
			
			if (qid != null) ref = new Reference(qid, langCode);
			refreshTab(getTab(box, i), ref);
			
		}
	 }
	
	private void refreshAllTabs()
	 {
		refreshAllTabs(TabsBox.UPPER);
		refreshAllTabs(TabsBox.LOWER);
	 }


	private void refreshTabStatus(TranslateTabMediator tab)
	 {
		if (tab == null) return;

		tab.fetchStatus();
		tab.fetchTitleStatus();
	 }

	private void refreshTabStatus(Reference ref)
	 {
		TranslateTabMediator tab = findTab(ref);
		if (tab != null) refreshTabStatus(tab);
	 }
	
	private void refreshTabPreview(Reference ref)
	 {
		TranslateTabMediator tab = findTab(ref);
		if (tab == null) return;
		
		tab.fetchPreview();
	 }

	private void refreshTabSymbols()
	 {
		int numTabs = getTabCount(TabsBox.UPPER);
		for (int i = 0; i < numTabs; i++) {
			TranslateTabMediator tab = getTab(TabsBox.UPPER, i);
			tab.fetchSymbols();
		}

	 }
	
	private void refreshTabFilters()
	 {
		int numTabs;
		
		numTabs = getTabCount(TabsBox.UPPER);
		for (int i = 0; i < numTabs; i++) {
			TranslateTabMediator tab = getTab(TabsBox.UPPER, i);
			tab.fetchFilters();
			tab.fetchFilteredText();
		}

		numTabs = getTabCount(TabsBox.LOWER);
		for (int i = 0; i < numTabs; i++) {
			TranslateTabMediator tab = getTab(TabsBox.LOWER, i);
			tab.fetchFilters();
			tab.fetchFilteredText();
		}
	 }
	
	
	// REFRESH (CONTROLS)
	private void refreshNavigationControls()
	 {
		boolean hasArticles = MinorityTranslateModel.content().getSelectableCount() > 0;
		setNavigationDisable(!hasArticles);
		
		disableMenuExactToggle(!hasArticles);
	 }
	
	private void refreshUploadControls()
	 {
		// upload button
		boolean isSelected = MinorityTranslateModel.content().getSelectedQid() != null;
		setUploadDisable(!isSelected);
		
		String qid = MinorityTranslateModel.content().getSelectedQid();
		boolean selected = false;
		if (qid != null) selected = MinorityTranslateModel.content().hasStatus(qid, Status.UPLOAD_REQUESTED);
		setUpload(selected);
		
		// menu
		boolean uploadEnabled = MinorityTranslateModel.session().isUploadEnabled();
		setMenuUploadEnabled(uploadEnabled);
	 }
	
	private void refreshProcessingControls()
	 {
		// upload button
		boolean isSelected = MinorityTranslateModel.content().getSelectedQid() != null;
		setPullDisable(!isSelected);
		setAlignDisable(!isSelected);
	 }

	
	// REFRESH (FILTERS)
	private void refreshLogin()
	 {
		// Connection:
		if (!MinorityTranslateModel.connection().hasConnection()) {
			setLoginIndicator(Color.RED, Messages.getString("statusbar.no.connection"));
			return;
		}
		
		// Login:
		MediaWikiBot[] bots = MinorityTranslateModel.bots().fetchToBots();
		for (int i = 0; i < bots.length; i++) {
			
			if (!bots[i].isLoggedIn()) {
				setLoginIndicator(Color.YELLOW, Messages.getString("statusbar.not.logged.in"));
				return;
			}
			
		}
		
		// No problems:
		setLoginIndicator(Color.GREEN, Messages.getString("statusbar.logged.in"));
	 }

	
	// REFRESH (STATUS)
	private void refreshWikis()
	 {
		showWikis(MinorityTranslateModel.preferences().getLangCodes());
	 }

	private void refreshTag()
	 {
		String tag = MinorityTranslateModel.session().getTag();
		if (!tag.isEmpty()) tag = "[" + tag + "]";
		showTag(tag);
	 }

	private void refreshCounts()
	 {
		int doneCount = MinorityTranslateModel.content().countDoneDstArticles();
		int totCount = MinorityTranslateModel.content().countDstArticles();
		showCount(doneCount + "/" + totCount);
	 }
	

	// TAB HELPERS
	private void togglePreview()
	 {
		TranslateTabMediator tab = getFocusedTab();
		if (tab == null) return;
		
		switch (tab.getTabView()) {
		case EDIT:
			tab.onChangeView(TabView.PREVIEW);
			break;
			
		case PREVIEW:
			tab.onChangeView(TabView.EDIT);
			break;

		default:
			break;
		}
	 }
	
	private TranslateTabMediator getFocusedTab()
	 {
		List<TranslateTabMediator> tabs;

		tabs = getTabs(TabsBox.UPPER);
		for (TranslateTabMediator tab : tabs) {
			if (tab.isEditFocused()) return tab;
		}

		tabs = getTabs(TabsBox.LOWER);
		for (TranslateTabMediator tab : tabs) {
			if (tab.isEditFocused()) return tab;
		}
		
		return null;
	 }
	
	
	// REFERENCE
	public Reference getCurrentReference()
	 {
		TranslateTabMediator tab = getSelectedTab(TabsBox.UPPER);
		if (tab == null) return null;
		return tab.getReference();
	 }
	
	
	// HELPERS
	private String[] getLangCodes(TabsBox box)
	 {
		String[] langCodes;
		if (box == TabsBox.LOWER) langCodes = this.lowerLangCodes;
		else if (box == TabsBox.UPPER) langCodes = this.upperLangCodes;
		else langCodes = new String[0];
		return langCodes;
	 }
	
	private TranslateTabMediator findTab(TabsBox box, Reference ref)
	 {
		String qid = MinorityTranslateModel.content().getSelectedQid();
		if (qid == null || !qid.equals(ref.getQid())) return null;
		
		String[] langCodes = getLangCodes(box);
		for (int i = 0; i < langCodes.length; i++) {
			if (langCodes[i].equals(ref.getLangCode())) return getTab(box, i);
		}
		return null;
	 }
	
	private TranslateTabMediator findTab(Reference ref)
	 {
		TranslateTabMediator tab = null;

		tab = findTab(TabsBox.LOWER, ref);
		if (tab != null) return tab;

		tab = findTab(TabsBox.UPPER, ref);
		if (tab != null) return tab;

		return null;
	 }

	private void commitTabs()
	 {
		int numTabs;

		numTabs = getTabCount(TabsBox.UPPER);
		for (int i = 0; i < numTabs; i++) {
			getTab(TabsBox.UPPER, i).commit();
		}

		numTabs = getTabCount(TabsBox.LOWER);
		for (int i = 0; i < numTabs; i++) {
			getTab(TabsBox.LOWER, i).commit();
		}
	 }
	
	
	// INHERIT (EDITORS)
	public abstract void openEditor(Editor editor);
	public abstract Editor getEditor();
	
	
	// INHERIT (SELECTION)
	public abstract String findSelectedSrcLangCode();
	
	
	// INHERIT (TABS)
	protected abstract TranslateTabMediator createTab(TabsBox box);
	protected abstract TranslateTabMediator destroyTab(TabsBox box);
	protected abstract TranslateTabMediator getTab(TabsBox box, int i);
	protected abstract TranslateTabMediator getSelectedTab(TabsBox box);
	protected abstract int getTabCount(TabsBox box);
	protected abstract List<TranslateTabMediator> getTabs(TabsBox box);
	
	
	// INHERIT (MENU)
	protected abstract void setMenuUploadEnabled(boolean enabled);
	protected abstract void disableMenuExactToggle(boolean disable);

	
	// INHERIT (CONTROLS)
	protected abstract void setNavigationDisable(boolean disabled);
	
	protected abstract boolean isUpload();
	protected abstract void setUploadDisable(boolean disabled);
	protected abstract void setUpload(boolean selected);
	
	protected abstract String getPullPluginName();
	protected abstract void setPullDisable(boolean disabled);
	protected abstract void setAlignDisable(boolean disabled);

	// INHERIT (DIVIDERS)
	public abstract int getAddonsDivider();
	public abstract void setAddonsDivider(int position);
	public abstract double getTranslateDivider();
	public abstract void setTranslateDivider(double position);
	
	// INHERIT (STATUSBAR)
	protected abstract void showWikis(String[] langCodes);
	protected abstract void showTag(String tag);
	protected abstract void showCount(String count);
	
	protected abstract void setLoginIndicator(Color colour, String tooltip);

	protected abstract void setProgressVisible(boolean visible);
	protected abstract void setProgress(double value);

	protected abstract void showSpellCheckerBusy(boolean busy);
	protected abstract void showQueuerBusy(boolean busy);
	protected abstract void showDownloaderBusy(boolean busy);
	protected abstract void showPreviewerBusy(boolean busy);
	protected abstract void showUploaderBusy(boolean busy);
	protected abstract void showSessionSaveBusy(boolean busy);
	protected abstract void showPullingBusy(boolean busy);
	protected abstract void showCheckingTitleBusy(boolean busy);
	
	
	// EVENTS (BUTTONS)
	public void onNextClick() {
		sendNotification(Notifications.MENU_SELECT_NEXT_ARTICLE);
	}

	public void onPreviousClick() {
		sendNotification(Notifications.MENU_SELECT_PREVIOUS_ARTICLE);
	}
	
	
	// EVENTS (MENU)
	public void onMenuSessionNewClick() {
		sendNotification(Notifications.SESSION_NEW_REQUEST);
	}

	public void onMenuSessionOpenClick() {
		sendNotification(Notifications.MENU_SESSION_OPEN);
	}

	public void onMenuSessionSaveClick() {
		sendNotification(Notifications.MENU_SESSION_SAVE);
	}

	public void onMenuSessionSaveAsClick() {
		sendNotification(Notifications.MENU_SESSION_SAVE_AS);
	}
	
	public void onMenuPreferencesClick() {
		sendNotification(Notifications.MENU_PREFERENCES_OPEN);
	}

	public void onMenuLoginClick() {
		sendNotification(Notifications.MENU_LOGIN_OPEN);
	}

	public void onMenuLogoutClick() {
		sendNotification(Notifications.LOGOUT);
	}

	public void onMenuQuitClick() {
		sendNotification(Notifications.REQUEST_QUIT);
	}


	public void onUploadEnable(boolean enabled) {
		sendNotification(Notifications.CHANGE_UPLOAD_ENABLED, enabled);
	}
	
	public void onMenuUploadClick() {
		onUpload(true);
	}
	
	public void onAddCategory()
	 {
		if (MinorityTranslateModel.preferences().getLangCodes().length == 0) {
			DialogFactory.dialogs().showError(Messages.getString("messages.adding.header.no.languages"), Messages.getString("messages.adding.content.add.one.language"));
			return;
		}
		sendNotification(Notifications.MENU_ADD_CATEGORY_OPEN);
	 }

	public void onMenuAddLinksClick()
	 {
		if (MinorityTranslateModel.preferences().getLangCodes().length == 0) {
			DialogFactory.dialogs().showError(Messages.getString("messages.adding.header.no.languages"), Messages.getString("messages.adding.content.add.one.language"));
			return;
		}
		sendNotification(Notifications.MENU_ADD_LINKS_OPEN);
	 }

	public void onAddArticle()
	 {
		if (MinorityTranslateModel.preferences().getLangCodes().length == 0) {
			DialogFactory.dialogs().showError(Messages.getString("messages.adding.header.no.languages"), Messages.getString("messages.adding.content.add.one.language"));
			return;
		}
		sendNotification(Notifications.MENU_ADD_ARTICLE_OPEN);
	 }

	public void onAddContent()
	 {
		List<ContentRequest> requests = DialogFactory.dialogs().askAddContent();
		for (ContentRequest request : requests) {
			
			sendNotification(Notifications.ADD_CONTENT, request);
			
		}
	 }
	
	public void onMenuStopDownloadingClick() {
		sendNotification(Notifications.DOWNLOADER_STOP);
	}
	
	public void onMenuListsClick()
	 {
		if (MinorityTranslateModel.preferences().getLangCodes().length == 0) {
			DialogFactory.dialogs().showError(Messages.getString("messages.adding.header.no.languages"), Messages.getString("messages.adding.content.add.one.language"));
			return;
		}
		sendNotification(Notifications.MENU_LISTS_OPEN);
	 }

	public void onMenuRemoveArticleClick()
	 {
		if (MinorityTranslateModel.preferences().getLangCodes().length == 0) {
			DialogFactory.dialogs().showError(Messages.getString("messages.adding.header.no.languages"), Messages.getString("messages.adding.content.add.one.language"));
			return;
		}
		sendNotification(Notifications.MENU_REMOVE_ARTICLE_OPEN);
	 }

	public void onMenuRemoveSelectedArticleClick() {
		sendNotification(Notifications.MENU_REMOVE_SELECTED_ARTICLE);
	}
	
	public void onMenuAboutClick() {
		sendNotification(Notifications.MENU_ABOUT_OPEN);
	}

	public void onMenuManualClick() {
		sendNotification(Notifications.MENU_MANUAL_OPEN);
	}
	
	
	// EVENTS (CONTROLS)
	public void onMenuNextArticleClick() {
		sendNotification(Notifications.MENU_SELECT_NEXT_ARTICLE);
	}

	public void onMenuPreviousArticleClick() {
		sendNotification(Notifications.MENU_SELECT_PREVIOUS_ARTICLE);
	}

	public void onMenuTogglePreviewClick() {
		togglePreview();
	}

	public void onMenuToggleExactClick()
	 {
		TranslateTabMediator tab = getSelectedTab(TabsBox.UPPER);
		if (tab == null) return;
		
		Reference ref = tab.getReference();
		boolean exact = MinorityTranslateModel.content().isExact(ref);
		
		sendNotification(Notifications.ENGINE_CHANGE_ARTICLE_EXACT, ref, (!exact) + "");
	 }
	
	
	public void onUpload(boolean upload)
	 {
		String qid = MinorityTranslateModel.content().getSelectedQid();
		if (qid == null) return;
		
		commitTabs();
		
		if (upload) sendNotification(Notifications.ENGINE_REQUEST_UPLOAD_QID, qid);
		else  sendNotification(Notifications.ENGINE_CANCEL_REQUEST_UPLOAD_QID, qid);
		
		refreshUploadControls();
	 }

	
	public void onPull()
	 {
		TranslateTabMediator srcTab = getSelectedTab(TabsBox.LOWER);
		TranslateTabMediator dstTab = getSelectedTab(TabsBox.UPPER);

		Reference srcRef = null;
		if (srcTab != null) srcRef = srcTab.getReference();
		if (srcRef == null) return;
		
		Reference dstRef = null;
		if (dstTab != null) dstRef = dstTab.getReference();
		if (dstRef == null) return;
		
		String pluginName = getPullPluginName();
		if (pluginName == null) return;
		
		commitTabs();
		
		sendNotification(Notifications.REQUEST_PULL, new PullRequest(pluginName, srcRef, dstRef));
	 }
	
	public void onAlign()
	 {
		TranslateTabMediator srcTab = getSelectedTab(TabsBox.LOWER);
		TranslateTabMediator dstTab = getSelectedTab(TabsBox.UPPER);
		if (srcTab == null || dstTab == null) return;
		
		srcTab.commit();
		dstTab.commit();
		
		Reference srcRef = srcTab.getReference();
		Reference dstRef = dstTab.getReference();
		if (srcRef == null || dstRef == null) return;
		
		sendNotification(Notifications.OPEN_ALIGN_EDITOR, new SrcDstRequest(srcRef, dstRef));
	 }
	
	public void onTag()
	 {
		String tag = DialogFactory.dialogs().askTag();
		if (tag != null) sendNotification(Notifications.SESSION_CHANGE_TAG, tag);
	 }
	
	
	// EVENTS (STATUSBAR)
	public void onWikiClicked(String langCode)
	 {
		String url;
		WikiType wikiType = MinorityTranslateModel.wikis().getWikiType(langCode);
		switch (wikiType) {
		case REGULAR:
			url = "https://" + langCode + ".wikipedia.org";
			break;
		
		case INCUBATOR:
			url = "https://incubator.wikimedia.org";
			break;
			
		default:
			return;
		}
		
		String username = MinorityTranslateModel.session().getUsername();
		
		if (username != null) url+= "/wiki/Special:Contributions/" + username;
		
		ApplicationMediator mediator = (ApplicationMediator) getFacade().retrieveMediator(ApplicationMediator.NAME);
		mediator.showDocument(url);
	 }
	
	
}
