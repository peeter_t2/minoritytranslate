package ee.translate.keeleleek.mtapplication.view.swing.elements;

import javax.swing.event.ChangeEvent;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import ee.translate.keeleleek.mtapplication.view.elements.SnippetsPaneMediator;
import ee.translate.keeleleek.mtapplication.view.swing.swingview.SwingSnippetsPaneViewComponent;
import javafx.fxml.FXML;


public class SnippetsPaneMediatorSwing extends SnippetsPaneMediator {

	
	// INIT
	@Override
	public SwingSnippetsPaneViewComponent getViewComponent() {
		return (SwingSnippetsPaneViewComponent) super.getViewComponent();
	}
	
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
		getViewComponent().snippetEdit.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				if (feedback) return;
				onSnippetChanged(getKeyIndex(), getViewComponent().snippetEdit.getText());
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				if (feedback) return;
				onSnippetChanged(getKeyIndex(), getViewComponent().snippetEdit.getText());
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				if (feedback) return;
				onSnippetChanged(getKeyIndex(), getViewComponent().snippetEdit.getText());
			}
		});
		
		getViewComponent().keyCodeLabel.setText(getCodeLabelText());
		
		getViewComponent().keySelection.addChangeListener(new javax.swing.event.ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				onSelectionChanged();
			}
		});
	 }
	
	// INHERIT
	@Override
	protected int getKeyIndex()
	 {
		return (Integer)(getViewComponent().keySelection.getValue()) - 1;
	 }
	
	@Override
	protected String getSnippet() {
		return getViewComponent().snippetEdit.getText();
	}
	
	@Override
	protected void setSnippet(String snippet) {
		getViewComponent().snippetEdit.setText(snippet);
	}
	
	
	// EVENTS
	@FXML
	private void onSelectionChanged()
	 {
		super.onSelectedSnippetChanged(getKeyIndex());
		
		getViewComponent().keyCodeLabel.setText(getCodeLabelText());
	 }
	
	
	// HELPERS
	private String getCodeLabelText()
	 {
		int n = getKeyIndex() + 1;
		return "Ctrl+" + n;
	 }
	
}
